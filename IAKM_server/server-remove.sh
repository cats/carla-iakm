#!/bin/bash

#docker container stop iakm-server-ai
docker container stop iakm-server-km
docker container stop iakm-server-db
docker container stop iakm-server-mqtt

#docker container rm iakm-server-ai
docker container rm iakm-server-km
docker container rm iakm-server-db
docker container rm iakm-server-mqtt

#docker image rm iakm-server-ai
#docker image rm iakm-server-km
#docker image rm iakm-server-db
#docker image rm iakm-server-mqtt

docker image prune -a
docker network disconnect iakm-public-network iakm-server-graphdb
docker network rm iakm-public-network


# docker image rm localhost:5000/nadar/thorenc-mongo             
# docker image rm thorenc_mongo
# docker image rm localhost:5000/nadar/thorenc-mosquitto         
# docker image rm thorenc_mosquitto 
# docker image rm localhost:5000/nadar/thorenc-knowledge-manager 
# docker image rm thorenc_knowledge_manager
# docker image rm localhost:5000/nadar/thorenc-ai-controller     
# docker image rm thorenc_ai_controller

# docker container stop thorenc-registry && docker container rm -v thorenc-registry
# docker image rm registry:2

# glpat-rmX4Z27r1oYvEvo2RsJD