import requests
import json
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--id', type=str)
parser.add_argument('--ip', type=str)
parser.add_argument('--port', type=int)
args = parser.parse_args()

urlQuery = f"http://{args.ip}:{args.port}/repositories/{args.id}/statements"
query = """
PREFIX :<http://www.ontotext.com/connectors/lucene#>
PREFIX inst:<http://www.ontotext.com/connectors/lucene/instance#>
INSERT DATA {
    inst:lucene-connector :createConnector '''
{
  "fields": [
    {
      "fieldName": "fts",
      "fieldNameTransform": "predicate.localName",
      "propertyChain": [
        "$literal"
      ],
      "indexed": true,
      "stored": true,
      "analyzed": true,
      "multivalued": true,
      "ignoreInvalidValues": false,
      "facet": false
    }
  ],
  "languages": [],
  "types": [
    "$untyped"
  ],
  "readonly": false,
  "detectFields": false,
  "importGraph": false,
  "skipInitialIndexing": false,
  "boostProperties": [],
  "stripMarkup": false
}
''' .
}"""
params = {"update": query, "infer": True, "sameAs": True}
headers = {"Accept": "application/sparql-results+json"}
res = requests.post(urlQuery, params=params, headers=headers)
print(id, res.status_code)

