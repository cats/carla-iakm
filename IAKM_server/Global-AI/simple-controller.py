import json
import threading
from threading import Thread
import ssl
import time
import traceback
from datetime import datetime
import requests
import socketio
import socket
import pickle
import numpy as np
import torch
import sys
from torch import nn
from torch import optim

import syft as sy
from syft.frameworks.torch.fl import utils
from syft.federated.floptimizer import Optims

import net
from net import Net
from dataclasses import dataclass
from flask_socketio import send, emit

data = torch.tensor([[0,0],[0,1],[1,0],[1,1.]])
target = torch.tensor([[0],[0],[1],[1.]])

def train(model):
    opt = optim.SGD(params=model.parameters(),lr=0.1)
    for iter in range(10):
        # 1) erase previous gradients (if they exist)
        opt.zero_grad()
        # 2) make a prediction
        pred = model(data)
        # 3) calculate how much we missed
        loss = ((pred - target)**2).sum()
        # 4) figure out which weights caused us to miss
        loss.backward()
        # 5) change those weights
        opt.step()
        # 6) print our progress
        print("Loss progress: " + str(loss.data))
    print(f"Loss End: {str(loss.data)} ________________________________")
    return model

def merge_simple(models):
    models[0] = train(models[0])
    models[1] = train(models[1])

    model1_pickled = pickle.dumps(models[0]) 
    model2_pickled = pickle.dumps(models[1])

    model1 = pickle.loads(model1_pickled)
    model2 = pickle.loads(model2_pickled)


    try:
        return utils.federated_avg({
            "bob": model1,
            "alice": model2})
    except Exception as e:
        print(f"Ohhhhh! {e.__class__} occurred.", flush=True)
        print(e, flush=True)
        print(e.__dict__, flush=True)
        print(traceback.print_exc())
        print("________________________________________________________")


bobs_model = Net()
alices_model = Net()

models = [bobs_model, alices_model]

merged = merge_simple(models)
print(merged)
print("Done................................")