def on_message(mqttc, obj, msg):
    if msg.topic == "$SYS/broker/log/M/subscribe": # handle subscription
        if "$SYS/broker/log/M/subscribe" in str(msg.payload.decode("utf-8", "ignore")):
            print("--------------------------------------------------------------------------------------", flush=True)
            print("---> FML controller subscribing to BROKER SYS logs to Receive SUB messages", flush=True)
            print("--------------------------------------------------------------------------------------", flush=True)
            return  # nothing to do when handling log subscription
        else:
            payload_array = json.loads(json.dumps(str(msg.payload.decode("utf-8", "ignore")))).split()
            _topic = payload_array[len(payload_array) -1]
            print(_topic)
            if topic_well_formated(_topic): # correct topic format
                agent = Semantic(_topic)
                if agent.method == "sub-model":
                    print("Receiving subscription for agent (" + agent.agent_id + ") with topic: " + _topic, flush=True)
                    print("Loading model from Database...", flush=True)
                    agent.get_binary_model()  # load model from mongoDB
                    if agent.model is None:
                        print("----- END: No model (for use) -----", flush=True)
                        return
                    if agent.model_usage == "use":
                        print("Publishing Model(use only) to: " + _topic, flush=True)
                        mqttc.publish(_topic, agent.model)  # publish saved model
                        return
                    if agent.model_usage == "train":
                        print("Publishing Model(train and use) to: " + _topic, flush=True)
                        mqttc.publish(_topic, agent.model)  # publish saved model
                        
                        
                        print("Subscribing for trained Model with topic:  pub-model/" + agent.get_topic(), flush=True)
                        mqttc.subscribe("pub-model/" + agent.get_topic())  # replace "sub-model" method by "pub-model"
                        # model usage for: training & merge & use
                        filtered_agents = [_agent for _agent in arr_agents 
                                            if agent.model_usage == _agent.model_usage and agent.match_model(_agent)]
                        if len(filtered_agents) == 0:
                            print("------------------ Start timer for merging models -------------------", flush=True)
                            arr_agents.append(agent)
                            threading.Timer(5, merge_save_publish, args=(agent,)).start()
                        else:
                            root_agent = filtered_agents[0]
                            root_agent.matches.append(agent)
                    if agent.model_usage == "available":
                        print("-----> Agent: " + agent.agent_id + " is available !", flush=True)
                        arr_availables = [_agent for _agent in arr_agents if _agent.model_usage == "available"]
                        if len(arr_availables) == 0:
                            arr_agents.append(agent)
                        else:
                            root_available = arr_availables[0]
                            root_available.matches.append(agent)

    elif msg.topic.startswith('pub-model'): # update trained models of agents
        print("Receiving Trained Model by API --> Type: " + str(type(msg.payload)) + " on Topic: " + msg.topic, flush=True)
        model = msg.payload
        current_root_agent = [_agent for _agent in arr_agents if ("pub-model/" + _agent.get_topic()) == msg.topic]
        if len(current_root_agent) > 0:
            print("---> Update trained model for 'Parent' agent ")
            current_root_agent[0].model = model
        else:
            for agent in arr_agents:
                if len(agent.matches)>0:
                    current_agent = [_agent for _agent in agent.matches if ("pub-model/" + _agent.get_topic()) == msg.topic]
                    if len(current_agent) > 0:
                        print("---> Update trained model for 'child' agent ")
                        current_agent[0].model = model
        

    elif msg.topic.startswith('req-available'):
        arr_availables = [_agent for _agent in arr_agents if _agent.model_usage == "available"]
        # add more filter here as semantic.....................................................
        if len(arr_availables) == 0:
            print("There is no 'available' workers", flush=True)
            return
        else:
            print("Send model to the ROOT available workers", flush=True)
            publishModel_subscribeToMerged(arr_availables[0])
            if len(arr_availables[0].matches) > 0:
                print("Start Counting Down for Merging models of available workers", flush=True)
                threading.Timer(5, merge_save_publish, args=(arr_availables[0],)).start()
                for agent in arr_availables[0].matches:
                    print("Send model to the CHILD available workers", flush=True)
                    publishModel_subscribeToMerged(agent)
            else:
                print("WARNING: cannot merge one single model !!!")               
"""