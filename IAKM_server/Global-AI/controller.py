import os
import threading
from threading import Thread
import ssl
import time
import traceback
from datetime import datetime
import socketio
import socket
import pickle
import numpy as np
import torch
import sys
from torch import nn
from torch import optim
import syft as sy
from syft.frameworks.torch.fl import utils
from syft.federated.floptimizer import Optims
import net
from net import Net
from dataclasses import dataclass
from flask_socketio import send, emit
import init_model

#KM_HOST = "eurecom-knowledge-manager"
KM_HOST = os.environ['IP_KM']
KM_PORT = 8183

client_FL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_FL.connect((KM_HOST,KM_PORT))
print(f"Connected to KM server @IP: {KM_HOST} ", end='', flush=True)
conn = None
SEMANTICS = []

def topicToDict(topic: str):
    dict_header ={}
    dict_context ={}
    arr_topic = topic.split('/')
    try:
        for prop in arr_topic:
            try:
                arr_prop = prop.split('=')
                if arr_prop[0] in ["method","model_usage","agent_type","trainability","agent_id","edge_id"]:
                    dict_header[arr_prop[0]] = arr_prop[1]
                else:
                    dict_context[arr_prop[0]] = arr_prop[1]
            except:pass
    except: pass
    return dict_header, dict_context

class AgentData:
    def __init__(self, topic, model=None):
        self.topic = topic
        self.model = model
        headers, context = topicToDict(topic)
        self.method = headers["method"]
        self.model_usage = headers["model_usage"]
        self.agent_type = headers["agent_type"]
        self.trainability = headers["trainability"]
        self.agent_id = headers["agent_id"]
        self.edge_id = headers["edge_id"]
        self.context = context

class Semantic:
    models = []
    topics = []
    merged_model = None
    def __init__(self, topic):
        self.IDs = []
        self.models = []
        self.topics = []
        self.merged_model = None
        _, context = topicToDict(topic)
        self.context =context
    
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def isMatching(self, agentData)-> bool:
        if agentData.context != self.context: return False
        return True

def mergeThenSendToKM(semantic):
    global SEMANTICS
    models = {}
    for i in range(len(semantic.models)):
        models[semantic.IDs[i]] = pickle.loads(semantic.models[i])
    print(f"-----  the number of models to merge is :{str(len(semantic.models))} -----", flush=True)
    if len(models) == 0:
        print("No trained Models in the dictionary !!!", flush=True)
        return
    try:
        merged_model = utils.federated_avg(models)
    except Exception as e:
        print(traceback.print_exc())

    semantic.merged_model = pickle.dumps(merged_model)
    pickled_semantic_data = pickle.dumps(semantic)
    client_FL.sendall(pickled_semantic_data)
    print("--------- Models have been Merged  -------------", flush=True)
    print("------- Models have been Sent to KM ------------", flush=True)
    SEMANTICS.remove(semantic)
    del semantic
    models = {}

init_model.add_initial_model() # add an AI model to DB for a given context

while True:
    data = client_FL.recv(40960)
    if not data: break
    agentData = pickle.loads(data)
    ID = agentData.agent_id
    topic = agentData.topic
    model = agentData.model
    if model:
        print(">>>>>>>> Controller received a model ",flush=True)
        SemanticsFiltered = [semantic for semantic in SEMANTICS if semantic.isMatching(agentData)]

        if len(SemanticsFiltered) == 0:
            new_semantic = Semantic(topic)
            new_semantic.models.append(model)
            new_semantic.topics.append(topic)
            new_semantic.IDs.append(ID)
            SEMANTICS.append(new_semantic)
            print("          >>>>> Start NEW thread to merge trained models",flush=True)
            threading.Timer(5,mergeThenSendToKM ,args=(new_semantic,)).start()
        else:
            print("          >>>>> Adding trained models to Thread",flush=True)
            exist_semantic = SemanticsFiltered[0]
            exist_semantic.models.append(model)
            exist_semantic.topics.append(topic)
            exist_semantic.IDs.append(ID)
