#!/bin/sh

# Better to not run the worker server without ID
if [ -z "${WORKER_ID}" ]; then
  echo "[-] You need to set the WORKER_ID environment variable"
  exit
fi

# if [ "$Add_model" = "yes" ]; then
#  exec python add_model.py
# fi

exec python controller.py --id $WORKER_ID --port 8777