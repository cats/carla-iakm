#!/bin/bash
COMPOSE_FILE="docker-compose-network.yml"
grep -v '^#' server.env
export $(grep -v '^#' server.env | xargs)

# Create network-------------------------------------------------------------------------------------------------------
docker network create  --driver=bridge --subnet=${SUBNET}  iakm-network
#
##PULL and BUILD server network services ------------------------------------------------------------------------------
docker-compose -f $COMPOSE_FILE --env-file server.env pull iakm_graphdb # Pulling GraphDB
docker-compose -f $COMPOSE_FILE --env-file server.env pull iakm_db   # Pulling Mongo image
docker-compose -f $COMPOSE_FILE --env-file server.env pull iakm_mqtt # Pulling Mosquitto image
docker-compose -f $COMPOSE_FILE --env-file server.env build iakm_km  # Building KM container

###Run server network services ----------------------------------------------------------------------------------------
docker-compose -d -f $COMPOSE_FILE --env-file server.env  up

#Update GraphDB -------------------------------------------------------------------------------------------------------
sleep 5
curl -X POST http://127.0.0.1:${GRAPHDB_PORT}/rest/repositories -H 'Content-Type: multipart/form-data' -F "config=@graphdb-IAKM-config.ttl"
curl -X PUT --header 'Content-Type: application/x-trig' --header 'Accept: application/json' -d @ontology.trigs "http://127.0.0.1:${GRAPHDB_PORT}/repositories/iakm-graphdb/statements"
python add-lucene-connector.py --ip '127.0.0.1' --port ${GRAPHDB_PORT}  --id iakm-graphdb

docker-compose -f $COMPOSE_FILE logs --follow

