import requests
import json
endpoint = f"repositories/docker-IAKM-graphdb"
params = {
    "query": """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX : <http://www.example.org/IAKM.owl#>
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                SELECT (strafter(str(?leaf),"#") as ?node) ?x FROM <http://www.ontotext.com/explicit>
                WHERE {
                    ?leaf rdfs:subClassOf+ :Road .  
                }"""
}
headers = {"Accept": "application/sparql-results+json"}
res = requests.get("http://localhost:7201/repositories/docker-IAKM-graphdb", params=params, headers=headers)
result = json.loads(res.content.decode())
leaves = []
for r in result["results"]["bindings"]:
    leaves.append(r["node"]["value"])

print(leaves)

