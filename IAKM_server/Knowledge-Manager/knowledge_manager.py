import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import socketio
import json
import socket
import os
import ssl
from _thread import *
from threading import Thread
import time
from datetime import datetime
import pickle
import pymongo, gridfs
from gridfs import GridFS
from bson import ObjectId
import sys
import traceback
from flask import Flask, render_template, request
from flask_socketio import SocketIO
from flask_socketio import send, emit
from dataclasses import dataclass
import logging
from db import dbGateway

MQTT_KEEPALIVE_INTERVAL = 60
MONGO_MAX_SIZE = 16000000 # mongo supports BSON document sizes up to 16793598 bytes.
MQTT_HOST,MQTT_PORT =  os.environ['MQTT_IP'], int(os.environ['MQTT_PORT'])     
MQTT_USERNAME, MQTT_PASSWORD = "nadar", "Ali@1234"
KM_HOST, KM_PORT = os.environ['IP_KM'], 8183
DB_HOST ,DB_PORT = os.environ['DB_IP'], int(os.environ['DB_PORT'])
DB_TYPE = "MongoDB"

#HOST, PORT = socket.gethostbyname(socket.gethostname()), 8183

server_FL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_FL.bind((KM_HOST,KM_PORT)) 
server_FL.listen(5)
size = 0
conn = None
Customers = []
Trainers  = []

#method=pub-model/model_usage=push/agent_type=vehicle/trainability=yes/agent_id=oulu-agent/edge_id=eurecom-5gmec/
#context_map/entity=junction/context_model/type=decision/input=3/feature=1"

def topicToDict(topic: str):
    dict_header ={}
    dict_context ={}
    arr_topic = topic.split('/')
    current_context = None
    try:
        for prop in arr_topic:
            try:
                if '=' in prop:
                    arr_prop = prop.split('=')
                    if arr_prop[0] in ["method", "model_usage", "agent_type", "trainability", "agent_id", "edge_id"]:
                        dict_header[arr_prop[0]] = arr_prop[1]
                    else:
                        dict_context[arr_prop[0]] = arr_prop[1]
                else:
                    current_context = arr_prop
            except:pass
    except: pass
    return dict_header, dict_context

def topicToContext(topic: str): # new 12/10/2023
    dict = {}
    context_map, context_model = {}, {}
    current_context = None
    try:
        arr_topic = topic.split('/')
        for prop in arr_topic:
            try:
                if '=' in prop:
                    arr_prop = prop.split('=')
                    if arr_prop[0] in ["method", "model_usage", "agent_type", "trainability", "agent_id", "edge_id"]:
                        dict[arr_prop[0]] = arr_prop[1]
                    else:
                        current_context[arr_prop[0]] = arr_prop[1]
                else:
                    if prop == "context_map": current_context = context_map
                    if prop == "context_model": current_context = context_model
            except:pass
    except:pass
    dict["context"] = {}
    dict["context"]["context_map"] = context_map
    dict["context"]["context_model"] = context_model
    return dict


#region "Classes & Functions"
class Semantic:
    models = []
    topics = []
    merged_model = None
    def __init__(self, topic):
        self.IDs = []
        self.models = []
        self.topics = []
        self.merged_model = None
        _, context = topicToDict(topic)
        self.context =context
    
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def isMatching(self, agentData)-> bool:
        if agentData.context != self.context: return False
        return True

class AgentData:
    def __init__(self, topic, model=None):
        self.topic = topic
        self.model = model
        dict = topicToContext(topic)
        self.method = dict["method"]
        self.model_usage = dict["model_usage"]
        self.agent_type = dict["agent_type"]
        self.trainability = dict["trainability"]
        self.agent_id = dict["agent_id"]
        self.edge_id = dict["edge_id"]
        self.context = dict["context"]

@dataclass
class AgentModel:
    topic: str
    topic_json: dict
    method: str
    model_usage: str
    agent_type: str
    trainability: str
    agent_id: str
    edge_id: str
    context = {}
    model_info = {}
    model: bytes = None
    data: object = None
    matches = []
    similar_available_agents = []
    def __init__(self, topic , payload:bytes = None):
        try:
            self.topic = topic
            if payload:
                self.data = pickle.loads(payload)
                self.model = self.data.model
            self.matches = []
            self.similar_available_agents = []
            dict = topicToContext(topic)
            self.topic_json = dict
            self.method = dict["method"]
            self.model_usage = dict["model_usage"]
            self.agent_type = dict["agent_type"]
            self.trainability = dict["trainability"]
            self.agent_id = dict["agent_id"]
            self.edge_id = dict["edge_id"]
            self.context = dict["context"]
        except:
            traceback.print_exc()

    def find_model(self):
        mongo_client = pymongo.MongoClient(f'mongodb://{DB_HOST}:{DB_PORT}/')
        mydb = mongo_client['data']
        mycollection = mydb['models']
        myquery = {**self.context}
        mydata = mycollection.find(myquery)
        print(f"QUERY: {myquery}")
        mydata = list(mydata)
        if len(mydata) > 0:
            data = mydata[0]
            is_big = bool(data['is_big'])
            if is_big:
                fs = gridfs.GridFS(mydb)
                big_id = data['big_id']
                print(f"BIG-ID: {big_id}")
                obj_model = fs.get(ObjectId(big_id)).read() 
                self.model = obj_model
            else:
                self.model = data['model']    
    def find_model_new(self):
        try:
            search_term = " AND ".join(str(e) for e in list(self.context.values())) 
            print(f"Search Term: {search_term}", flush=True)
            self.model_info, self.model = dbGateway.getModel_lucene(search_term)
            print("Model Info:" + json.dumps(self.model_info, indent=1) , flush=True)    
        except Exception:
            print(traceback.format_exc(), flush=True)
    def find_model_new_new(self):
        try:
            #self.model = dbGateway.getGraphModel_simple(self.context)
            self.data = dbGateway.getGraphModel_simple(self.context)
            if self.data:
                self.model = self.data.model
                model_size = sys.getsizeof(self.model)
                print(f"(KM) Model Size: {model_size} bytes", flush=True)
        except Exception:
            print(traceback.format_exc(), flush=True)

    def save_model(self):
        mongo_client = pymongo.MongoClient(f'mongodb://{DB_HOST}:{DB_PORT}/')
        mydb = mongo_client['data']
        mycollection = mydb['models']
        model_size = sys.getsizeof(self.model)
        if model_size > MONGO_MAX_SIZE:
            fs = gridfs.GridFS(mydb)
            big_id = fs.put(self.model)
            insert_data = {"is_big":1, "big_id":big_id, **self.context, "created_time": datetime.now()}
            insert = mycollection.insert_one(insert_data)
            _id = insert.inserted_id
            print(f"      ✓✓✓✓✓ Pushed 'BIG' Model has been saved successfully in Database - id: {_id}", flush=True )
        else:
            insert_data = {"is_big":0, "model":self.model, **self.context, "created_time": datetime.now()}
            insert = mycollection.insert_one(insert_data)
            _id = insert.inserted_id
            print(f"      ✓✓✓✓✓ Pushed 'SMALL' Model has been saved successfully in Database - id: {_id}", flush=True )   
    def save_model_new(self):
        try:
            model_db_meta = {"model_db_host": DB_HOST,"model_db_port": DB_PORT}
            response = dbGateway.setGraphModel(self.context ,self.data, model_db_meta)
            return response
        except Exception:
            print(sys.exc_info(), flush=True)
            return None

    def getSimilarAgents(self):
        mongo_client = pymongo.MongoClient(f'mongodb://{DB_HOST}:{DB_PORT}/')
        mycollection = mongo_client['data']['agents']
        myquery = {**self.context}
        data = mycollection.find(myquery)
        data = list(data)
        if len(data) > 0:
            for _agent in data:
                self.similar_available_agents.append(_agent)
    
def getTopicAfterChange(key: str, new_value: str, topic: str)-> str:
    l  = []
    arr_topic = topic.split('/')
    try:
        for prop in arr_topic:
                arr_prop = prop.split('=')
                if arr_prop[0] == key: l.append(f"{key}={new_value}")
                else:                  l.append(prop)
    except:pass 
    new_topic = '/'.join(l)
    return new_topic

def save_merged_model(semantic: Semantic):
    mongo_client = pymongo.MongoClient(f'mongodb://{DB_HOST}:{DB_PORT}/')
    mydb = mongo_client['data']
    mycollection = mydb['models']
    insert_data = {"model":semantic.merged_model, "merging_worker":len(semantic.IDs), "merging_time":datetime.now(), **semantic.context}
    insert = mycollection.insert_one(insert_data)
    _id, _ack = insert.inserted_id, insert.acknowledged
    print(f"      ✓✓✓✓✓ MERGED Model has been saved successfully in Database - id: {_id}", flush=True )

def topicWellFormat(topic):
    length_topic = len(topic.split('/'))
    if length_topic < 6:return False
    else:               return True

def save_available_agent(agent):
    mongo_client = pymongo.MongoClient(f'mongodb://{DB_HOST}:{DB_PORT}/')
    mydb = mongo_client['data']
    mycollection = mydb['agents']

    myquery = {"topic": agent.topic,"_id": agent.agent_id,**agent.context}
    newvalues = {"$set": {**myquery , "created_time": datetime.now()}}
    mycollection.update_one(myquery, newvalues, upsert = True)
    print('available agent has been saved successfully in Database !', flush=True )

#endregion

def task_FL_connection():
    global conn
    while True:
        conn, addr = server_FL.accept()
        pickled_semantic_data = conn.recv(40960)
        if pickled_semantic_data:
            master_trainer = None
            print(">>>>> KM received Merged Pickled model from FL controller", flush=True)
            semantic_data = pickle.loads(pickled_semantic_data)
           
            CustomerFilter = [customer for customer in Customers if semantic_data.isMatching(customer)]
            for customer in CustomerFilter:
                mqttc.publish(customer.topic, semantic_data.merged_model)
                print(f"      ✓✓✓✓✓ MERGED model is published to the Customer: {str(customer.agent_id)}" , flush = True)
                Customers.remove(customer)
            print(f"Lenght of Customers is: {len(Customers)}")
            
            for i in range(len(semantic_data.IDs)):
                if "model_usage=train" in semantic_data.topics[i]:
                    agent_id = semantic_data.IDs[i]
                    filter = [x for x in Trainers if x.agent_id == agent_id]
                    if len(filter) > 0: master_trainer = filter[0]
                new_topic = getTopicAfterChange("method", "pub-merge", semantic_data.topics[i])
                mqttc.publish(new_topic, semantic_data.merged_model)
                print(f"      ✓✓✓✓✓ MERGED model is published to the Agent: {str(semantic_data.IDs[i])}" , flush = True)
            
            # Save merged model in DB/RDF
            if master_trainer:
                response = dbGateway.setModel(semantic_data.context,
                                              semantic_data.merged_model,
                                              master_trainer.model_info,
                                              isMerged=True)
                print(response, flush=True)

        time.sleep(0.1)
Thread(target=task_FL_connection, args=()).start()

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT Broker!", flush=True)
    mqttc.subscribe("$SYS/broker/log/M/subscribe", 0)
    mqttc.subscribe("method=sub-available/#", 0)
    mqttc.subscribe("method=pub-model/#", 0)

def on_message(mqttc, obj, msg ):
    global Customers, Trainers, size
    if msg.topic == "$SYS/broker/log/M/subscribe": # handle subscription
            if "$SYS/broker/log/M/subscribe" in str(msg.payload.decode("utf-8", "ignore")):
                print("---------------------------------------------------------------------------", flush=True)
                print("---> SERVER-KM subscribing to BROKER SYS logs to listen to AGENTS subs <---", flush=True)
                print("---------------------------------------------------------------------------", flush=True)
            else: # Some-Agent subscribe for Some-Service
                payload_array = json.loads(json.dumps(str(msg.payload.decode("utf-8", "ignore")))).split()
                _topic = payload_array[len(payload_array) -1]
                print(_topic, flush=True)
                if topicWellFormat(_topic):
                    agent = AgentModel(_topic)

                    if agent.method == "sub-model":
                        print(f"Agent ({agent.agent_id}) is subscribing with topic: {_topic}" , flush=True)
                        if agent.model_usage == "use":
                            print("the agent " + agent.agent_id +" want a model to use.")
                            print("Loading model from Database...", flush=True)
                            agent.find_model_new_new()
                            if agent.model is None:
                                print("----- END: No model in Edge DB -----", flush=True)
                            else :
                                print("Publishing Model(use only) to: " + _topic, flush=True)
                                mqttc.publish(_topic, pickle.dumps(agent.data))

                        if agent.model_usage == "train" and agent.trainability == "yes":
                            print(f"Agent ({agent.agent_id}) needs model [Trainability: {agent.trainability}]")
                            agent.find_model_new()
                            if agent.model is None:
                                print("----- END: No model in Edge DB -----", flush=True)
                            else:
                                print("---- Searching trainers in Database ---", flush=True)
                                agent.getSimilarAgents()
                                if len(agent.similar_available_agents) ==0:
                                    print("There is -NO- Similar available agent in Database", flush=True)
                                    mqttc.publish(_topic, agent.model) 
                                else:
                                    Trainers.append(agent)
                                    print(f"There is -{len(agent.similar_available_agents)}- Similar trainer in Database", flush=True)
                                    print("Publishing Initial Model to Original Trainer with topic: "+ agent.topic, flush=True)
                                    mqttc.publish(agent.topic, agent.model) 
                                    new_topic = getTopicAfterChange("method","pub-model", agent.topic)
                                    print(f"Subscribing to Trained Model with topic: {new_topic}" , flush=True)
                                    mqttc.subscribe(new_topic)  
                                    # Publish DB model to all other available agents && Subscribe to Trained model
                                    for availableAgent in agent.similar_available_agents:
                                        print("Publishing Initial Model(train and use) to the agent with topic: "+ availableAgent["topic"], flush=True)
                                        mqttc.publish(availableAgent["topic"], agent.model) 
                                        new_topic =  availableAgent["topic"].replace("sub-model","pub-model")
                                        print(f"Subscribing to Trained Model with topic: {new_topic}" , flush=True)
                                        mqttc.subscribe(new_topic)  # replace "sub-model" method by "pub-model"
                         
                        if agent.model_usage == "train" and agent.trainability != "yes":
                            print(f"AI-Customer ({agent.agent_id}) needs Merged model [Trainability: {agent.trainability}]")
                            agent.find_model_new()
                            if agent.model is None:
                                print("----- END: No model in Edge DB -----", flush=True)
                            else:
                                print("---- Searching trainers in Database ---", flush=True)
                                agent.getSimilarAgents()
                                if len(agent.similar_available_agents) ==0:
                                    print("There is -NO- Similar available agent in Database", flush=True)
                                else:
                                    print(f"There is -{len(agent.similar_available_agents)}- Similar trainers in Database", flush=True)
                                    Customers.append(agent)
                                    # Publish DB model to all other available agents && Subscribe to Trained model
                                    for availableAgent in agent.similar_available_agents:
                                        print("Publishing Initial Model(train and use) to the agent with topic: "+ availableAgent["topic"], flush=True)
                                        mqttc.publish(availableAgent["topic"], agent.model) 
                                        new_topic =  availableAgent["topic"].replace("sub-model","pub-model")
                                        print(f"Subscribing to Trained Model with topic: {new_topic}" , flush=True)
                                        mqttc.subscribe(new_topic)  # replace "sub-model" method by "pub-model"
                    
                    if agent.method == "sub-available":
                        print(f"-----> Agent: ({agent.agent_id}) is available !", flush=True)
                        save_available_agent(agent)

    elif msg.topic.startswith('method=pub-model/model_usage=push'): # Receiving model from Agent
        topic = msg.topic
        payload = msg.payload
        print(f"---> Size  {sys.getsizeof(payload)} bytes", flush=True)
        agentModel = AgentModel(topic, payload)
        size_temp = sys.getsizeof(agentModel.model)
        size += size_temp
        print(f"Push Model received:", flush=True)
        print(f"---> Topic: {topic}", flush=True)
        print(f"---> Size : {size_temp} bytes (total: {size} bytes)", flush=True)
        res_save = agentModel.save_model_new()
        print(json.dumps(res_save, indent=1), flush=True)
        print(f"   --> Model Saved in DB <--- ", flush=True)
    
    elif msg.topic.startswith('method=pub-model'): # Receiving trained models from Agents
        topic = msg.topic
        model = msg.payload
        agentData = AgentData(topic,model)
        print(f">>> Trained Model received: Topic ({topic})", flush=True)
        pickled_agentData = pickle.dumps(agentData)
        conn.sendall(pickled_agentData)
        print(f"    >>> Send Trained Model to FL controller", flush=True)
  



#region "Start KM Microservice"
try:
    mqttc = mqtt.Client("client-logger")
    #mqttc.tls_set(ca_certs="certs/rootCA.crt", certfile="certs/client.crt", keyfile="certs/client.key")
    #mqttc.tls_set(ca_certs="certs/certs/rootCA.crt")
    #mqttc.tls_insecure_set(True)     
    logger = logging.getLogger(__name__)
    mqttc.enable_logger(logger)
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    #mqttc.username_pw_set(MQTT_USERNAME, MQTT_PASSWORD)
    mqttc.connect(host=MQTT_HOST, port=MQTT_PORT, keepalive=MQTT_KEEPALIVE_INTERVAL) 
    #dbGateway.index_create()
    mqttc.loop_forever()
except Exception:
    print(traceback.format_exc())
#endregion 



'''Note
{
  "@id" : "http://www.example.org/IAKM.owl#ra_3_moulins",
  "@type" : [ "http://www.w3.org/2002/07/owl#NamedIndividual", "http://www.example.org/IAKM.owl#Roundabout" ],
  "http://www.example.org/IAKM.owl#nbEntry" : [ {
    "@type" : "http://www.w3.org/2001/XMLSchema#integer",
    "@value" : "3"
  } ],
  "http://www.example.org/IAKM.owl#nbExit" : [ {
    "@type" : "http://www.w3.org/2001/XMLSchema#integer",
    "@value" : "3"
  } ],
  "http://www.example.org/IAKM.owl#nbLane" : [ {
    "@type" : "http://www.w3.org/2001/XMLSchema#integer",
    "@value" : "3"
  } ],
  "http://www.example.org/IAKM.owl#width" : [ {
    "@type" : "http://www.w3.org/2001/XMLSchema#integer",
    "@value" : "33"
  } ]
}
'''


# curl -X GET --header 'Accept: text/plain' 'http://iakm-server-graphdb:7200/repositories/docker-IAKM-graphdb/size'
# curl -X GET --header 'Accept: text/plain' 'http://192.168.10.135:7200/repositories/docker-IAKM-graphdb/size'
