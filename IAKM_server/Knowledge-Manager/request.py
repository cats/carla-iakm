import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import json
import threading
import ssl
import time
from datetime import datetime
import requests
import pickle
import numpy as np
import pymongo
import torch
from torch import nn
from torch import optim
import syft as sy
from syft.frameworks.torch.fl import utils
from syft.federated.floptimizer import Optims
import net
from net import Net
from dataclasses import dataclass
mos_HOST = "192.168.104.45"
mos_PORT = 8884
mos_USERNAME = "nadar"
mos_PASSWORD = "Ali@1234"
MQTT_KEEPALIVE_INTERVAL = 60
import logging
#logging.basicConfig(level=logging.DEBUG)
is_connected = False


def on_connect(client, userdata, flags, rc):
    global is_connected
    is_connected = True
    print("Connected to MQTT Broker", flush=True)
    mqttc.publish("req-available/roundabout/4/5", "Start")
    print("Publish: req-available/2/roundabout/4/5", flush=True)

def on_message(mqttc, obj, msg):
    print("Message Received:" + msg, flush=True)


# Initiate MQTT Client
mqttc = mqtt.Client("client-request")
mqttc.tls_set(ca_certs="certs/ca.crt", certfile="certs/client.crt", keyfile="certs/client.key")
mqttc.tls_insecure_set(True)
logger = logging.getLogger(__name__)
mqttc.enable_logger(logger)
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.username_pw_set(mos_USERNAME, mos_PASSWORD)
#while not is_connected:
#    try:
mqttc.connect(host=mos_HOST, port=mos_PORT, keepalive=MQTT_KEEPALIVE_INTERVAL)
#except ConnectionRefusedError:
#print("ERROR: connection refused" )
#time.sleep(3)
mqttc.loop_forever()