
import datetime
import json

context = {
 "entity": "Junction",
 "type": "decision",
  "inputs": 3,
  "outputs": 1
}

info = {
    "model_db_type": "MongoDB",
    "model_db_host": "192.168.10.134",
    "model_db_port": 27017,
    "model_db_id": "651d6dcb7430db15e2ec5820" }

def ttt(context, db_info):
    query = """
    PREFIX : <http://www.example.org/uiakm.owl#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    INSERT DATA{ %s   %s }"""
    
    if "entity" in context:
        uid = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
        instance_class = context['entity']
        del context["entity"]
        if "name" in context:
            ctx_instance_name  =  context['name'] 
            del context["name"]
        else: 
            ctx_instance_name = f"{instance_class}_{str(uid)}" 
        ##########################################################################
        def format_item(item):
            k,v = item[0], item[1]
            if isinstance(v, str): return f":{k} '{v}';"
            else:                  return f":{k}  {v};"
        
        trplts_map = (f":{ctx_instance_name} a owl:NamedIndividual . "
                      f":{ctx_instance_name} a :{instance_class}; ")
        l = list(map(format_item, context.items()))
        l[-1]=l[-1].replace(";", " . ")
        trplts_map = trplts_map + " ".join(l)
        ##########################################################################
        def format_model(ctx):
            return f":{ctx[0]} '{ctx[1]}'; "
        
        ctx_model = f"model_{ctx_instance_name}"
        trplts_model = (f":{ctx_model}         a              owl:NamedIndividual . "
                        f":{ctx_model}         a              :Models . "
                        f":{ctx_instance_name} :hasModel      :{ctx_model} . "
                        f":{ctx_model}         :hasMapContext :{ctx_instance_name} ; ")             
        l = list(map(format_model, db_info.items()))
        l[-1]=l[-1].replace(";", " .")
        trplts_model = trplts_model + " ".join(l)

        query = query % (trplts_map, trplts_model)
        return query
    
#print(ttt(context, info))


import requests
query = ""
query = query + " PREFIX : <http://www.example.org/IAKM.owl#> "
query = query + " PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  "
query = query + " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "                       
query = query + " PREFIX owl: <http://www.w3.org/2002/07/owl#>   "             
query = query + " INSERT DATA{ :junction_111 a owl:NamedIndividual . :junction_111 a :Junction; :bd '1985' . :model_1985         a              owl:NamedIndividual .              :model_1985         a              :Models . :111 :hasModel      :model_1985 .              :model_1985         :hasMapContext :111 ;                                 :model_db_type 'MongoDB';                                  :model_db_host '192.168.10.134';                                :model_db_port '27017';                           :model_db_id '651d81a6e5dcb86c1417d7fa' .  }"

urlQuery="http://127.0.0.1:7201/repositories/docker-IAKM-graphdb/statements"
params = { "update":query, "infer":True,"sameAs":True }

print(query)
print("----------------------------------------------")
headers = {"Accept": "application/sparql-results+json"}
res = requests.post(urlQuery, params=params, headers=headers)
print(res.content.decode(), flush=True)
print(res.status_code, flush=True)
    


