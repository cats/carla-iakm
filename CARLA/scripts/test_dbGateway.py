import sys
import json
import pickle
from db import dbGateway
from io import BytesIO
import pandas as pd
import joblib

'''
context_getModel = {
    "entity": "round circle",
    "attributes": [
        {
            "name": "number of Entries",
            "value": 5
        },
        {
            "name": "number of exits",
            "value": 5
        },
        {
            "name": "direction",
            "value": "right"
        },
        {
            "name": "width",
            "value": 78.0
        },
        {
            "name": "lanes",
            "value": 2
        }]
}

context_setModel = {
    "entity": "roundabout",
    "attributes": [
        {
            "name": "number of Entries",
            "value": 5
        },
        {
            "name": "number of exits",
            "value": 5
        },
        {
            "name": "direction",
            "value": "right",
            # "weight": 3
        },
        {
            "name": "width",
            "value": 77.0,
            "approx": 3
        },
        {
            "name": "lanes",
            "value": 2
        }]
}

'''

context_getModel = {
    "entity": "roundabout",
    "attributes": [
        { "name": "inputs",  "value": 3},
        { "name": "outputs", "value": 1
        }]
}
context_setModel = {
    "entity": "junction",
    "attributes": [
        { "name": "inputs",  "value": 3},
        { "name": "outputs", "value": 1
        }]
}

#region "Request SET model"
# model = None
# with open(f"model_T_RFC_50.sav", 'rb') as readFile:
#     data_read = readFile.read()
#     model = pickle.dumps(data_read)
#     print(f"✓✓✓ RFC Junction Model is pickled; Size : {sys.getsizeof(model)} bytes", flush=True)
# DB_PORT=27017
# DB_IP="127.0.0.1"
# model_db_meta = { "model_db_type": "MongoDB", "model_db_host": DB_IP,"model_db_port": DB_PORT}
# response_set = dbGateway.setGraphModel(map_context=context_setModel, model=model, db_info=model_db_meta )
# print("--------------------  RESULT  ------------------------")
# print(response_set)
#endregion



#region "Request GET model"
x = pd.DataFrame({"ego_time": [3],"other_time": [6],"distance_ra": [0.7]})
response_model = dbGateway.getGraphModel_simple(map_context=context_getModel)
print(sys.getsizeof(response_model))
bytes_container = BytesIO()



joblib.dump(response_model, bytes_container)
bytes_container.seek(0)
b = bytes_container.read()
sklearn_model = pickle.loads(b)

message = sklearn_model.predict(x)[0]
print(message)
#endregion

















# https://lookup.dbpedia.org/api/search?format=JSON&QueryCategory=&QueryString=round%20junction

# corpus_embeddings = embedder.encode(leaves, convert_to_tensor=True)

# # Find the closest 5 sentences of the corpus for each query sentence based on cosine similarity
# top_k = min(5, len(leaves))
# query_embedding = embedder.encode(query, convert_to_tensor=True)

# # We use cosine-similarity and torch.topk to find the highest 5 scores
# cos_scores = util.cos_sim(query_embedding, corpus_embeddings)[0]
# top_results = torch.topk(cos_scores, k=top_k)

# print("Query:", query)
# print(f"Top similar using '{model}':")
# print("---------------------------------------")
# for score, idx in zip(top_results[0], top_results[1]):
#     print(leaves[idx], "(Score: {:.2f})".format(score))

