#region import
import glob
from datetime import datetime
import traceback
import threading
import subprocess
import pygame
import os
import sys
import pandas as pd
import torch
#torch.set_default_dtype(torch.float64)
from torch.autograd import Variable
import time
import pickle
import numpy as np
####from sklearn.linear_model import LogisticRegression
####from sklearn.model_selection import train_test_split
from shapely.geometry.polygon import Polygon
from shapely.geometry.polygon import Point as P
import random
import math
import argparse
import logging
import subprocess
import random
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    print("Failed to locate CARLA")
    pass
import carla
sys.path.append('../carla/')
Delta_Time = 0.1
p = os.path.abspath('.')
sys.path.insert(1, p)
from tools import *
from utils import *
from utils_iakm import *
from pid_test_update_ic_fcn_final_merge_2 import adv_thr_ctrl
from numpy import linalg as LA
array_crosstrack_error = []
import requests
from pathlib import Path
from dotenv import load_dotenv
#endregion

#region Variables
vehicles, new_points, Arcs, Zones,list_vehicleSpeeds = [],[], [], [], []
list_wps, ego_id, ego_actor, veh_ego, timestamp = None,None,None,None,None
k_e =  18.9
k_v =  2.2
MAX_EDGE = 80
nextInterval = 0.1
tm_port = 8000
total_veh_count = 10
safety_distance = 8
TARGET_SPEED = 6.95
SF = 1.1             # Safety Factor
BRAKE = 0.95
speed_limit = 30
SPT = 3 # steer calculation per Throttle
actors = []
#model_name = 'town03.pickle'
#model_name = 'Carla_0.pickle'
#model = pickle.load(open(model_name, 'rb'))

#endregion

#region "Map Positions"
exit_top = Exit(0, "TOP", carla.Location(x=TOP_exit_X, y=TOP_exit_Y, z=0.5))
exit_right = Exit(1, "RIGHT",carla.Location(x=RIGHT_exit_X, y=RIGHT_exit_Y, z=0.5))
exit_bottom = Exit(2, "BOTTOM",carla.Location(x=BOTTOM_exit_X, y=BOTTOM_exit_Y, z=0.5))
exit_left = Exit(3, "LEFT",carla.Location(x=LEFT_exit_X, y=LEFT_exit_Y, z=0.5))
Exits = [exit_top, exit_right, exit_bottom, exit_left]

entry_top = Entry(0, "TOP", carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5))
entry_right = Entry(1, "RIGHT",carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5))
entry_bottom = Entry(2, "BOTTOM",carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5))
entry_left = Entry(3, "LEFT",carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5))
Entries = [entry_top, entry_right, entry_bottom, entry_left]

RIGHT_SPOT = carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5)
TOP_SPOT = carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5)
LEFT_SPOT = carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5)
BOTTOM_SPOT = carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5)

box_right_entry = carla.Location(x=5.467874, y=34.947102, z=1)
box_top_entry = carla.Location(x=33.563801, y=-7.999302, z=1)
box_left_entry = carla.Location(x=-7.077684, y=-34.086342, z=1)
box_bottom_entry = carla.Location(x=-35.431053, y=1.172551, z=1)

box_right_exit = carla.Location(x=9.737114, y=23.112617, z=1)
box_top_exit = carla.Location(x=22.559887, y=-9.433374, z=1)
box_left_exit = carla.Location(x=-10.412357, y=-22.614059, z=1)
box_bottom_exit = carla.Location(x=-24.933035, y=5.580378, z=1)
#endregion

# region Functions
def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude
#Calc minimum distance from a point and a line segment (i.e. consecutive vertices in a polyline).
def DistancePointLine(px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)

    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine

    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)

    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)

    return DistancePointLine
def get_nearest_wp(location, wps):
    old_d= float("inf")
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            wp = wps[i]
            index = i
    # if old_d > 2:
    #     return None, None
    #else:
    return index, wp
def get_nearest_index(t, wps, index_back,bumper):
    global flag
    direction = t.get_forward_vector()
    min_d= float("inf")
    index = 0
    my_x,my_y = bumper.x, bumper.y

    for i in range(index_back-5,index_back+6):
        if i < 0:continue
        elif i > len(wps)-2 : continue
        else:
            wp1, wp2 = wps[i], wps[i + 1]
            wp1_x, wp1_y = wp1.transform.location.x, wp1.transform.location.y
            wp2_x, wp2_y = wp2.transform.location.x, wp2.transform.location.y
            d = DistancePointLine(my_x, my_y, wp1_x, wp1_y, wp2_x, wp2_y)
            if d < min_d:
                min_d = d
                index = i
    return index
def getVehicleSpot_wp_index(list_wp, spot):
    distance = float("inf")
    index = 0
    counter = 0
    for i, wp in enumerate(list_wp):
        d = getDistance(wp.transform.location, spot)
        if d < distance:
            distance = d
            index = i

        if d > distance: counter +=1
        if counter > 3: break
    return index
def get_nearest_Vehicle(location):
    old_d= float('inf')
    for i in range(len(vehicles)):
        d = getDistance(location, vehicles[i].centroid)
        if d < old_d:
            old_d = d
    return old_d
def get_next_index(veh, wps, index):
    wp_location = wps[index].transform.location
    angle = find_angle(veh.direction,wp_location,veh.centroid)
    if abs(angle) < 90:
        return index
    else:
        return index+1
def getNearestExit(location) -> Exit:
    d = 1000000
    res_Exit = None
    for _exit in Exits:
        new_d = getDistance(_exit.location, location)
        if new_d < d:
            d = new_d
            res_Exit = _exit
    return res_Exit
def get_safePoint_coor(location, distance):
    r= (R_SMALL + (LANE_WIDTH * 1.5 ))
    alpha = math.atan2(location.y-CENTER.y, location.x -CENTER.x)
    theta = distance/r
    angle = theta+alpha
    x = X_CENTER + r * math.cos(angle)
    y = Y_CENTER + r * math.sin(angle)
    return carla.Location(x=x, y=y, z=0.5)
def vehicle_in_roundabout(location):
    dx = abs(location.x - CENTER.x)
    dy = abs(location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True
def bumper_in_roundabout(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    bumper_location = t.location + (actor.bounding_box.extent.x * fwd_vector)
    dx = abs(bumper_location.x - CENTER.x)
    dy = abs(bumper_location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True
def getBumper(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    return t.location + (actor.bounding_box.extent.x * fwd_vector)
def getRear(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    return t.location - (actor.bounding_box.extent.x * fwd_vector)
def getSpeed(actor):
    speed_x, speed_y = actor.get_velocity().x, actor.get_velocity().y
    speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
    speed_km = 3.6 * speed
    return [speed, speed_km]
def get_model_thr():
    model_thr = []
    data = pd.read_csv(FILE_THROTTLE_MODEL)
    speed = data['speed']
    distance = data['distance']
    time = data['time']
    for i in range(len(speed)):model_thr.append([speed[i],distance[i],time[i]])
    return model_thr

Z=1
poly_OUT = Polygon([(-70,-1,Z),(-70,2.5,Z),(-40,2.5,Z),(-40,-1,Z)])
poly_AUTOPILOT = Polygon([(-50,-1,Z),(-150,2.5,Z),(-40,2.5,Z),(-40,-1,Z)])
def draw_polygones(debug):
        pts = poly_OUT.boundary.coords
        for i in range(len(pts)-1):
            p1 = carla.Location(pts[i][0], pts[i][1], pts[i][2])
            p2 = carla.Location(pts[i+1][0], pts[i+1][1], pts[i][2])
            debug.draw_line(p1, p2, thickness=0.3, color=COLOR_CARLA_RED, life_time=1000)
def get_polygones(debug):
        circle_1 = []
        countd = 1
        radius_1 = R_SMALL + (2 * LANE_WIDTH)
        for i in range(360):
            angle = countd * (math.pi / 180)
            x1,y1 = radius_1 * math.sin(angle), radius_1 * math.cos(angle)
            p1 = carla.Location(x1 + X_CENTER, y1 + Y_CENTER)
            circle_1.append(p1)
            countd += 1

        # for i in range(len(circle_1)-1):
        #     if (i >5 and i< 35) or ( i > 96 and i < 122) or ( i> 185 and i< 213) or ( i> 273 and i< 292 ):
        #         p1, p2 = circle_1[i], circle_1[i+1]
        #         debug.draw_line(p1, p2,thickness=0.3, color=COLOR_CARLA_RED, life_time=10000)
# endregion

FILE_MODEL = 'model_T_RFC_50.sav'
FILE_THROTTLE_MODEL = 'model_thr_50_tesla_big.csv'
BOX_DIST = 10
LEN_EGO_WPS = 800
CCC = carla.Location(-50, 50)
w_label_location = carla.Location(-28, 6)
w_count_location = carla.Location(-30.3, 6.5)
control_type_location = carla.Location(-35, 6.5)


class Vehicle(object):
    #region "Vehicle variables"
    _id: int
    actor: carla.Vehicle = None
    transform: carla.Transform
    centroid: carla.Location
    bumper: carla.Location
    rear: carla.Location
    direction: carla.Location
    previous_location: carla.Location
    spot: carla.Location
    model_thr: list
    heading: float
    next_wp_index: int
    dist_exit: float
    dist_exit_normalized: float
    dist_ra:float
    dist_ra_normalized: float
    prev_next_exit: Exit = None
    prev_next_entry: Entry = None
    next_exit: Exit = None
    next_entry: Entry = None
    nearest_exit: Exit = None
    nearest_entry: Entry = None
    in_control_box:bool = False
    traveled_distance: float
    centroid_in_ra: bool
    bumper_in_ra: bool
    rear_in_ra: bool
    bumper_to_ra:float
    rear_to_ra:float
    length: float
    lateral: float
    steer_angle: float
    control: str
    wp_steer: float = 0
    wp_throttle: float = 0
    steer: float
    throttle: float
    brake: float
    brake_index: int =0
    speed: float
    speed_km: float
    list_brake:[]
    dataset = []
    max_steering_angle: float
    max_brake_torque: float
    max_steering_angle_rad: float
    side:str
    target_zone_index: int
    target_zone_location: carla.Location
    slice: int = None
    status: str = "not-entered"
    arc: Arc = None
    zone: Arc = None
    arc_index_current: int = -1
    throttle_index_current: int = 0
    throttle_list = []
    zone_index_current: int = -1
    arc_index_new: int = -1
    zone_index_new: int = -1
    wp: carla.Waypoint = None
    wp_index: int = None
    startTimestep: int = 0
    distance_decision_prediction: float = 0
    isExiting: bool = False
    spot_wp_index: int
    time_to_spot: float
    list_wps =[]
    yaw: float
    index_back: int
    index_front: int
    toward_RA : bool
    prev_in_ra:bool
    heading:float
    ahead = None
    isFollowing: False
    model_control = None
    #endregion
    def __init__(self,side, actor, ego_wps=None):
        self._id = actor.id
        self.actor = actor
        self.wp_index = 0
        self.spot_wp_index = -1
        self.time_toSpot = -1
        self.prev_in_ra = False
        self.side = side
        if self.side == "right":
            self.target_zone_index = 2
            self.target_zone_location = carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5)
        if self.side == "top":
            self.target_zone_index =1
            self.target_zone_location = carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5)
        if self.side == "left":
            self.target_zone_index =0
            self.target_zone_location = carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5)
        if self.side == "bottom":
            self.target_zone_index =3
            self.target_zone_location = carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5)
        self.spot = None #EGO_SPOT
        self.time_waiting = None
        self.model_thr = get_model_thr()
        self.transform = actor.get_transform()
        self.previous_location = self.transform.location
        self.centroid = actor.get_location()
        fwd_vector = self.transform.get_forward_vector()
        self.bumper = self.centroid + (actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (actor.bounding_box.extent.x * fwd_vector)
        self.direction = self.bumper + (10 * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.length = getDistance(self.bumper, self.rear)
        self.traveled_distance = 0
        self.steps_total = 0
        self.next_wp_index = 1
        self.list_wps = []
        self.index_back = 0
        self.index_front = 0
        self.heading = 0
        self.dist_exit = None
        self.dist_ra_normalized = None
        self.dist_exit_normalized = None
        self.dist_ra = None
        self.dist_spot = 0
        self.throttle = 0.5
        self.brake = 0
        self.centroid_in_ra = False
        self.bumper_in_ra = False
        self.rear_in_ra = False
        self.in_control_box = False
        self.closest_veh: None
        self.throttle_index_current = 0
        self.throttle_list = []
        self.list_brake = []
        physics_control = actor.get_physics_control()
        wheels = physics_control.wheels
        self.max_steering_angle = wheels[0].max_steer_angle
        self.max_steering_angle_rad = math.radians(self.max_steering_angle)
        # self.context = {
        #     "context_map": {"entity": "roundabout"},
        #     "context_model": {"ml_type": "decision", "input": "3", "feature": "1"}
        # }
        #self.iakm_request = IAKMRequest(self.context)
        #self.model_control = self.iakm_request.get_model()
        self.model_control = pickle.load(open("model.sav", 'rb'))

    def update_Arc(self, debug):
        arc = None
        index = -1
        for i in range(len(Arcs)):
            if Arcs[i].contains(self.bumper, debug):
                arc = Arcs[i]
                index = i
                break
        self.arc = arc
        self.prev_next_exit = self.next_exit
        self.next_exit = self.arc.front if self.arc is not None else None
        self.arc_index_new = index
    def update_Zone(self, debug):
        zone = None
        index = -1
        for i in range(len(Zones)):
            if Zones[i].contains(self.bumper, debug):
                zone = Zones[i]
                index = i
                break
        self.zone = zone
        if self.zone is not None:
            self.target_zone_location = self.zone.front.location
        self.prev_next_entry = self.next_entry
        self.next_entry = self.zone.front if self.zone is not None else None
        self.zone_index_new = index
    def update(self,world, debug):
        #region "Global Update"
        global main_exit
        self.transform = self.actor.get_transform()
        fwd_vector = self.transform.get_forward_vector()
        self.steer = self.actor.get_control().steer
        self.steer_angle = self.steer * self.max_steering_angle
        self.centroid = self.transform.location + carla.Location(z=1)
        self.bumper = self.centroid + (self.actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (self.actor.bounding_box.extent.x * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.centroid_in_ra = vehicle_in_roundabout(self.centroid)
        self.bumper_in_ra = vehicle_in_roundabout(self.bumper)
        self.rear_in_ra = vehicle_in_roundabout(self.rear)
        self.bumper_to_ra = getDistance(self.bumper,CENTER) - R_BIG
        self.rear_to_ra = getDistance(self.rear,CENTER) - R_BIG
        self.lateral = round((getDistance(self.centroid, CENTER) - R_SMALL) / (LANE_COUNT * LANE_WIDTH), 3)
        tangent = get_tangent(CENTER, self.bumper)
        self.direction = self.bumper + (10 * fwd_vector)
        angle_tangent_direction = find_angle(tangent, self.direction, self.bumper)
        self.heading = -(round(self.steer_angle + angle_tangent_direction, 2))
        self.toward_RA = True if self.heading > 0 else False
        speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
        self.speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
        self.speed_km = round(3.6 * self.speed, 2)
        distance_to_center = getDistance(self.bumper, CENTER)
        self.dist_ra = distance_to_center - R_BIG
        self.status = "entered" if self.bumper_in_ra and self.status == "not-entered" else self.status
        self.in_control_box = True if (self.toward_RA and (0 < self.dist_ra < 10)) else False
        self.dist_ra_normalized = self.dist_ra / BOX_DIST
        self.update_Arc(debug)
        self.update_Zone(debug)
        self.wp_index = get_nearest_index(self.transform, self.list_wps, self.wp_index, self.bumper)
        self.closest_veh = None
        message = "initial"
        #endregion

        #debug.draw_string(control_type_location, "AI", False, COLOR_CARLA_BLACK, 0.05)
        if self.spot_wp_index > self.index_front:
            self.dist_spot = (self.spot_wp_index - self.index_front) * nextInterval
            self.time_to_spot = self.get_time_to_spot()
            #debug.draw_string(self.centroid, str(round(self.time_to_spot, 2)), False, COLOR_CARLA_YELLOW, 0.05)

        if self.in_control_box:
            if self.speed_km < 1:
                if not self.time_waiting:
                    self.time_waiting = time.time()
                else:
                    w = str(round((time.time()-self.time_waiting),1))
                    # debug.draw_string(w_label_location, "waiting", False, COLOR_CARLA_BLACK, 0.05)
                    # debug.draw_string(w_count_location, w, False, COLOR_CARLA_BLACK, 0.05)
            message = "t50"
            self.closest_veh, closest_angle = None , 135 # 90
            for other in actors_ra:
                a = find_angle_degree(CENTER, getBumper(other), self.spot)
                if a < closest_angle:
                    self.closest_veh = other
                    closest_angle = a

            if self.closest_veh:
                closest_location = self.closest_veh.get_location()
                closest_bumper, closest_rear   = getBumper(self.closest_veh) , getRear(self.closest_veh)
                closest_speed    = math.sqrt(self.closest_veh.get_velocity().x ** 2 + self.closest_veh.get_velocity().y ** 2)
                debug.draw_line(self.bumper, closest_rear, thickness=0.05, color=COLOR_CARLA_RED, life_time=0.1)
                r = getDistance(closest_location, CENTER)
                data_distance_other_spot = find_arc_length(closest_angle, r)
                other_time_spot = round((data_distance_other_spot/closest_speed), 2)
                time_diff = other_time_spot-self.time_to_spot
                if self.model_control != None:
                    X = pd.DataFrame({
                        "time_diff": [time_diff],
                        "ego_speed": [self.speed_km],
                        "distance_ra": [self.dist_ra_normalized]
                    })
                    message = self.model_control.predict(X)[0]
                    #print(f"{round(time_diff,2)}, {round(self.speed_km,2)}, {round(self.dist_ra_normalized,2)} --> {message}")
                    # debug.draw_string(self.rear, message, False, COLOR_CARLA_GREEN, 0.05)
                    # debug.draw_string(closest_bumper, str(round(other_time_spot, 2)), False, COLOR_CARLA_YELLOW, 0.1)
                else:
                    self.throttle, self.brake = 0, 0.5
            else:
                self.throttle, self.brake = 0.5, 0

        if self.bumper_in_ra:
            message = "static"
            #self.actor.set_autopilot(True)

        #region "Calculate Steer"
        try:
            center_axle_x, center_axle_y = self.bumper.x, self.bumper.y
            self.index_back = get_nearest_index(self.transform, self.list_wps, self.index_back, self.bumper)
            self.index_front = self.index_back + 1
            back_x, back_y = self.list_wps[self.index_back].transform.location.x, self.list_wps[
                self.index_back].transform.location.y
            front_x, front_y = self.list_wps[self.index_front].transform.location.x, self.list_wps[
                self.index_front].transform.location.y
            yaw_path = np.arctan2(front_y - back_y, front_x - back_x)
            yaw_diff = yaw_path - math.radians(self.yaw)
            if yaw_diff > np.pi:   yaw_diff -= 2 * np.pi
            if yaw_diff < - np.pi: yaw_diff += 2 * np.pi
            crosstrack_error = DistancePointLine(center_axle_x, center_axle_y, back_x, back_y, front_x, front_y)
            yaw_cross_track = np.arctan2(center_axle_y - front_y, center_axle_x - front_x)
            yaw_path2ct = yaw_path - yaw_cross_track
            if yaw_path2ct > np.pi:   yaw_path2ct -= 2 * np.pi
            if yaw_path2ct < - np.pi: yaw_path2ct += 2 * np.pi
            if yaw_path2ct > 0:
                crosstrack_error = abs(crosstrack_error)
            else:
                crosstrack_error = - abs(crosstrack_error)
            yaw_diff_crosstrack = np.arctan(k_e * crosstrack_error / (k_v + self.speed))
            steer_expect = yaw_diff + yaw_diff_crosstrack
            if steer_expect > np.pi:   steer_expect -= 2 * np.pi
            if steer_expect < - np.pi: steer_expect += 2 * np.pi
            steer_expect = min(self.max_steering_angle_rad, steer_expect)
            steer_expect = max(-self.max_steering_angle_rad, steer_expect)
            steer_normalized = steer_expect / self.max_steering_angle_rad
            self.steer = steer_normalized
        except:
            traceback.print_exc()
            print("...........................")
            print(self.index_back)

        #endregion

        #debug.draw_string(self.rear, f"{message} s: {round(self.speed_km, 2)}", False, COLOR_CARLA_YELLOW, 0.05)
        self.setControl(message)

    def setControl(self, message):
        if message == "static":
            self.throttle = 0.3 if self.speed_km < 20 else (0.24 if self.speed_km <25 else 0.22)
            self.brake =  0
        elif message == "initial":
            coef = (self.speed_km / 100) if self.speed_km > 25 else 0
            self.throttle, self.brake = (0.5 - coef), 0
        else:
            if 't' in message:
                self.brake = 0
                self.throttle = int(message.replace('t', '')) / 100
                if self.throttle == 0.6: self.throttle == 0.5
            if 'b' in message:
                self.throttle = 0
                self.brake = int(message.replace('b', '')) / 100

        control = self.actor.get_control()
        control.steer = self.steer
        control.throttle = self.throttle
        control.brake = self.brake
        self.actor.apply_control(control)
    def get_time_to_spot(self):
        diff = 100
        index1 = -1
        for i in range(len(self.model_thr)): # [speed, distance, time]
            delta = abs(self.model_thr[i][0] - self.speed)
            if delta < diff:
                index1 = i
                diff = delta

        initial_distance = self.model_thr[index1][1]
        initial_time = self.model_thr[index1][2]
        final_distance = initial_distance + self.dist_spot
        diff = 100
        index2=-1
        for j in range(index1+1,len(self.model_thr)):
            delta_distance = abs(self.model_thr[j][1] - final_distance)
            delta_time = abs(self.model_thr[j][2] - initial_time)
            if delta_distance < diff:
                index2 = j
                diff = delta_distance

        required_time = self.model_thr[index2][2] - initial_time
        return required_time
    def get_newtransform(self, world, actors):
        new_transforms = []
        for t in get_spawnpoints(world, replacing=True):
            closest_veh = 1000
            for actor in actors:
                new_distance = getDistance(actor.get_transform().location, t.location)
                if new_distance < closest_veh:
                    closest_veh = new_distance
            if closest_veh > 6:
                new_transforms.append(tuple([closest_veh, t]))
        lenght = len(new_transforms)

        if lenght > 0:
            new_transforms.sort(key=lambda x: x[0], reverse=True)
            return new_transforms[0][1]
        else:
            return None
        list_vehicleSpeeds.append(vehSpeeds)
        del self


def start_sequence(wp_other, world, blueprints):
    global actors, traffic_manager
    other_bp = blueprints.find('vehicle.tesla.model3')
    other_bp.set_attribute('color', '255,255,0')
    for i in range(5):
        dwell = random.choice(list(range(7,27)))
        wp_other_new = None
        previous = wp_other.previous(dwell)
        while not wp_other_new:
            # if len(previous) == 1:
            wp_other_new = previous[0]
            # else:
            #     d_min = 1000
            #     for prev in previous:
            #         d = getDistance(CCC, prev.transform.location)
            #         if d < d_min:
            #             d_min = d
            #             wp_other_new = prev
        actor_other = world.spawn_actor(other_bp, random.choice(spawn_points))
        actor_other.set_transform(wp_other_new.transform)
        actor_other.set_autopilot(True)
        actors.append(actor_other)
        wp_other = wp_other_new
        print("AUtopiloted....")





def spawn(side, world, blueprints):
    global  actors
    spawn_points = get_spawnpoints(world)
    random.shuffle(spawn_points)
    ego_t = get_bottom_spawnpoint(world)
    ego_t = carla.Transform(ego_t.location + carla.Location(x=random.choice(list(range(-5,5)))), ego_t.rotation)
    ego_bp = blueprints.find('vehicle.tesla.model3')
    ego_bp.set_attribute('color', '255,0,0')
    actor_ego = world.spawn_actor(ego_bp, ego_t)
    wp_index, wp = get_nearest_wp(ego_t.location, wps)
    initial_wp = wp
    actors.append(actor_ego)
    try:
        actor_ego.set_autopilot(False, traffic_manager.get_port())
        veh_ego = Vehicle(side, actor_ego)

        veh_ego.list_wps = []
        while len(veh_ego.list_wps) < LEN_EGO_WPS:
            location = wp.transform.location
            if veh_ego.spot is None:
                d = getDistance(location, CENTER)
                if d < R_BIG-1.5:
                    veh_ego.spot = carla.Location(location.x, location.y)
                    veh_ego.spot_wp_index = len(veh_ego.list_wps)-1
                    #world.debug.draw_string(veh_ego.spot, "*", False, carla.Color(0, 0, 0), 100)
            myWaypoint = MyWaypoint(wp)
            veh_ego.list_wps.append(myWaypoint)

            nexts = wp.next(nextInterval)
            if len(nexts) == 2:  wp = nexts[random.randint(0, 1)]
            elif len(nexts) > 0: wp = nexts[0]
            else: wp = None
            #world.debug.draw_string(location, ".", False, carla.Color(0, 0, 0), 15)
    except Exception:
        traceback.print_exc()
        pass
    #start_sequence(initial_wp, world, blueprints)
    #spawn_auto(initial_wp, world, blueprints, spawn_points)

    return veh_ego

wps, vehicles = [],[]
traffic_manager = None
actors, actors_ra = [] , []
spawn_points = []
other_sps = []
def main():
    global timestamp, Arcs, Zones, actors, actors_ra
    global wps, vehicles, spawn_points, other_sps
    global  traffic_manager
    loop = True
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    global ego_id, ego_actor,veh_ego
    try:
        # region Global-Settings
        client = carla.Client('127.0.0.1', 2000)
        client.set_timeout(10)
        traffic_manager = client.get_trafficmanager(8020)
        traffic_manager.set_synchronous_mode(False)
        world = client.get_world()  # world = client.load_world("Town03")
        map = world.get_map()
        debug = world.debug
        get_polygones(debug)
        #draw_polygones(debug)
        wps = map.generate_waypoints(0.5)
        other_sps = [x for x in wps
                     if 82 < x.transform.location.x < 85
                     and -110 < x.transform.location.y < -90]
        # spectator = world.get_spectator()
        # spectator.set_transform(carla.Transform(carla.Location(x=-10, y=0, z=75),carla.Rotation(pitch=-90)))
        actors_ra =  [x for x in world.get_actors().filter('vehicle.*') if bumper_in_roundabout(x)]
        blueprints = world.get_blueprint_library()
        arc_top_right = Arc("Top_Right", exit_top, exit_right, "right")
        arc_right_bottom = Arc("Right_Bottom", exit_right, exit_bottom, "bottom")
        arc_bottom_left = Arc("Bottom_Left", exit_bottom, exit_left, "left")
        arc_left_top = Arc("Left_Top", exit_left, exit_top, "top")
        zone_top_right = Arc("Top_Right",  entry_right,entry_top, "right")
        zone_right_bottom = Arc("Right_Bottom", entry_bottom, entry_right, "bottom")
        zone_bottom_left = Arc("Bottom_Left", entry_left, entry_bottom, "left")
        zone_left_top = Arc("Left_Top",  entry_top,entry_left, "top")
        Arcs = [arc_top_right, arc_right_bottom, arc_bottom_left, arc_left_top]
        Zones = [zone_top_right, zone_right_bottom, zone_bottom_left, zone_left_top]
        spawn_points = get_spawnpoints(world)
        random.shuffle(spawn_points)
        #endregion

        ego_bottom = spawn("bottom", world, blueprints)
        clock = pygame.time.Clock()
        vehicles.append(ego_bottom)

        while loop:
            for veh in vehicles:
                try:
                    veh.update(world, debug)
                    if veh.centroid.y > 22: sys.exit()
                except Exception:
                        traceback.print_exc()
            # for actor in actors:
            #     traffic_manager.auto_lane_change(actor,True)
            #     traffic_manager.force_lane_change(actor,False)
            #     traffic_manager.set_route(actor,['Right'])
            #     traffic_manager.ignore_lights_percentage(actor, 100)


            clock.tick_busy_loop(int(SPT / Delta_Time))

    finally:
        for actor in actors: actor.destroy()
        print("Finally Ended...")

if __name__ == '__main__':
    main()