import sys
import json
import pickle
from db import dbGateway
from io import BytesIO
import pandas as pd
import numpy as np
import joblib
from utils_iakm import IAKMRequest

FILE_MODEL = f"model.sav"
#x = pd.DataFrame({"ego_time": [3],"other_time": [6],"distance_ra": [0.7]})
#x = pd.DataFrame({"ego_time": [3],"other_time": [6],"distance_ra": [0.7]})
# X = np.array([[2.401, 1.13, 12.6, 0.245]])
# X = np.array([[0.5300000000000002, 1.15, 32.6, 0.016000000000000014]])





df = pd.DataFrame(np.array([[1.78, 1.63, 15.2, 0.41],
[1.79, 1.6, 14.93, 0.4 ]]),
                  columns=["time_diff", "ego_speed", "distance_ra"])
X = df[["time_diff", "ego_speed", "distance_ra"]]
model = pickle.load(open(FILE_MODEL, 'rb'))
message = model.predict(X)
print(f"m1: {message}")

'''
DB_PORT=27017
DB_IP="127.0.0.1"
model_db_meta = { "model_db_type": "MongoDB", "model_db_host": DB_IP,"model_db_port": DB_PORT}
##############################################################################################
bytes_container = BytesIO()
joblib.dump(model, bytes_container)
bytes_container.seek(0)
model_db = bytes_container.read()
info = dbGateway.save_model_mongo(model=model_db, db_info=model_db_meta )
print(info)
##############################################################################################
model_db = dbGateway.load_model_mongo(model_id=info['model_db_id'], db_info=model_db_meta)
model = joblib.load(BytesIO(model_db))
message = model.predict(x)[0]
print(message)
'''


# context_map = {
#     "entity": "junction",
#     "attributes": [{ "name": "inputs",  "value": 3},
#                    { "name": "outputs", "value": 1}]
#         }
context_set = {
  "context_map": {
   "entity": "junction"
  },
  "context_model": {
   "type": "decision",
   "input": "3",
   "feature": "1"
  }
 }

context_get = {
  "context_map": {
   "entity": "roundabout"
  },
  "context_model": {
   "type": "decision",
   "input": "3",
   "feature": "1"
  }
 }
#
# #req = IAKMRequest(context_set, model)
# req = IAKMRequest(context_get, None)
# req.send()
# print(req.response)
# model = req.model
# #model = joblib.load(BytesIO(req.model))
# message = model.predict(x)[0]
# print(message)