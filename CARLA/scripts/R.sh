#!/usr/bin/env bash
max=100
for (( i=1; i <= $max; ++i ))
do
if [[ $(( i % 2 )) == 0 ]]
then
  python3.7 ego_R.py
else
  python3.7 ego_R_basic.py
fi

done


