#region import
import glob
from datetime import datetime
import traceback
#from threading import Thread
import threading
import subprocess
import pygame
import os
import sys
import pandas as pd
import time
import json
import csv
import time
import pickle
import numpy as np
####from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import random
import math
import argparse
import logging
import subprocess

from networkx.utils import nodes_or_number
from termcolor import colored
from dataclasses import dataclass
from shapely.geometry.polygon import Polygon
from shapely.geometry.polygon import Point as P
import matplotlib.pyplot as plt
#import torch
#import logisticRegression
#from logisticRegression import LogisticRegression
import random
try:sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (sys.version_info.major, sys.version_info.minor,'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:print("Failed to locate CARLA")
import carla
sys.path.append('../carla/')
from agents.navigation import global_route_planner
from agents.navigation import global_route_planner_dao
p = os.path.abspath('.')
sys.path.insert(1, p)
from tools import *
from utils import *
from utils_iakm import *
#endregion

#region Variables
vehicles, polygones =  [], []
list_wps = None
k_e, k_v=  18.9, 2.2
nextInterval = 0.1
tm_port = 8000
Delta_Time, SPT = 0.1, 3 # steer calculation per Throttle
actors = []
z = 8.1
BOX_CONTROL_DIST = 10
BOX_COMING_DIST = 50
BORDER_p1, BORDER_p2 = carla.Location(91.5,-75), carla.Location(91.5,-71.8)
poly_CONTROL = Polygon([(91.5+BOX_CONTROL_DIST,-75.1,z),(91.5+BOX_CONTROL_DIST,-78.3,z),(91.4,-78.5,z),(91.4,-75.3,z)])
poly_COMING = Polygon([(84.5,-67.8,z),(81.2,-67.8,z),(80.5,-67.8 + BOX_COMING_DIST,z),(83.5,-67.8 + BOX_COMING_DIST,z)])
poly_TARGET = Polygon([(86.6,-120,z+1),(83,-120,z+1),(81.6,-81.1,z+1),(85.6,-81,z+1)])
poly_SQUARE = Polygon([(91.1,-81,z),(91.1,-75.3,z),(81.5,-75.3,z),(81.5,-81.1,z)])
THROTTLE = 0.6
FILE_DATASET = 'dataset.csv'
FILE_THROTTLE_MODEL = 'model_thr_50_tesla_big.csv'
OTHER_BOTTOM_SPOT = carla.Location(-22.61, 8.05)  # carla.Location(-22.22 ,9.06)
EGO_BOTTOM_SPOT   = carla.Location(-21.96, 9.78)
EGO_ENTRY = carla.Location(91.33, -73.49)
SPECTATOR = carla.Location(79.49, -68.58, 50)
ROLE = "trainer" # "ai"  "trainer"
FILE_MODEL = 'model_T_RFC_50.sav'
AUTO_PILOT = False
#endregion

def draw_polygones(debug):
    #polys = [poly_CONTROL, poly_COMING,poly_TARGET, poly_SQUARE]
    polys = [poly_CONTROL]
    for p in polys:
        pts = p.boundary.coords
        for i in range(len(pts)-1):
            p1 = carla.Location(pts[i][0], pts[i][1], pts[i][2])
            p2 = carla.Location(pts[i+1][0], pts[i+1][1], pts[i][2])
            debug.draw_line(p1, p2, thickness=0.2, color=COLOR_CARLA_RED, life_time=25)

# region Functions
def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude
#Calc minimum distance from a point and a line segment (i.e. consecutive vertices in a polyline).
def DistancePointLine(px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)

    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine

    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)

    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)

    return DistancePointLine
def get_nearest_wp(location, wps):
    old_d= float("inf")
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            wp = wps[i]
            index = i
    return index, wp
def get_nearest_index(t, wps, index_back,bumper):
    direction = t.get_forward_vector()
    min_d= float("inf")
    index = 0
    my_x,my_y = bumper.x, bumper.y

    for i in range(index_back-5,index_back+6):
        if i < 0:continue
        elif i > len(wps)-2 : continue
        else:
            wp1, wp2 = wps[i], wps[i + 1]
            wp1_x, wp1_y = wp1.transform.location.x, wp1.transform.location.y
            wp2_x, wp2_y = wp2.transform.location.x, wp2.transform.location.y
            d = DistancePointLine(my_x, my_y, wp1_x, wp1_y, wp2_x, wp2_y)
            if d < min_d:
                min_d = d
                index = i
    return index

def vehicle_in_square(location):
    dx = abs(location.x - CENTER.x)
    dy = abs(location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True
def bumper_in_suqare(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    bumper_location = t.location + (actor.bounding_box.extent.x * fwd_vector)
    dx = abs(bumper_location.x - CENTER.x)
    dy = abs(bumper_location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True
def getBumper(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    return t.location + (actor.bounding_box.extent.x * fwd_vector)
def getRear(actor):
    t = actor.get_transform()
    fwd_vector = t.get_forward_vector()
    return t.location - (actor.bounding_box.extent.x * fwd_vector)
def getSpeed(actor):
    speed_x, speed_y = actor.get_velocity().x, actor.get_velocity().y
    speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
    speed_km = 3.6 * speed
    return [speed, speed_km]
def get_model_thr():
    model_thr = []
    data = pd.read_csv(FILE_THROTTLE_MODEL)
    speed = data['speed']
    distance = data['distance']
    time = data['time']
    for i in range(len(speed)):model_thr.append([speed[i],distance[i],time[i]])
    return model_thr

def distance_point_to_line(p, l1, l2):
    numerator = abs((l1.x - l2.x) * (l2.y - p.y) - (l1.y - l2.y) * (l2.x - p.x))
    denominator = math.sqrt((l1.x - l2.x) ** 2 + (l1.y - l2.y) ** 2)
    distance = numerator / denominator
    return distance

# endregion

class Vehicle(object):
    #region "Vehicle variables"
    _id: int
    actor: carla.Vehicle = None
    transform: carla.Transform
    role:str
    spot: carla.Location
    spot_wp_index: int
    centroid: carla.Location
    bumper: carla.Location
    rear: carla.Location
    direction: carla.Location
    previous_location: carla.Location
    model_thr: list
    next_wp_index: int
    centroid_in_ra: bool
    bumper_in_sq: bool
    rear_in_ra: bool
    bumper_to_ra:float
    rear_to_ra:float
    steer_angle: float
    wp_throttle: float = 0
    steer: float
    throttle: float
    brake: float
    brake_index: int =0
    speed: float
    speed_km: float
    dataset = []
    max_steering_angle: float
    max_steering_angle_rad: float
    throttle_index_current: int = 0
    throttle_list = []
    wp: carla.Waypoint = None
    wp_index: int = None
    list_wps =[]
    yaw: float
    index_back: int
    index_front: int
    wp_border_ra: carla.Waypoint = None
    context: dict
    #endregion
    def __init__(self, actor):
        self._id = actor.id
        self.actor = actor
        self.wp_index = 0
        self.spot = None
        self.spot_wp_index = -1
        self.time_toSpot = -1
        self.model_thr = get_model_thr()
        self.transform = self.actor.get_transform()
        self.previous_location = self.transform.location
        self.centroid = actor.get_location()
        _fwd_vector = self.transform.get_forward_vector()
        _yaw = self.transform.rotation.yaw
        self.bumper = self.centroid + (actor.bounding_box.extent.x * _fwd_vector)
        self.rear = self.centroid - (actor.bounding_box.extent.x * _fwd_vector)
        self.direction = self.bumper + (10 * _fwd_vector)
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.next_wp_index = 1
        self.list_wps = []
        self.index_back = 0
        self.index_front = 0
        self.dist_sq = None
        self.dist_sq_normalized = None
        self.dist_spot = 0
        self.wp_border_ra = None
        self.throttle = THROTTLE
        self.brake = 0
        self.centroid_in_ra = False
        self.bumper_in_sq = False
        self.rear_in_ra = False
        self.in_control_box = False
        self.in_square = False
        self.in_poly_target = False
        self.closest_veh: None
        self.throttle_index_current = 0
        self.throttle_list = []
        physics_control = actor.get_physics_control()
        wheels = physics_control.wheels
        self.max_steering_angle = wheels[0].max_steer_angle
        self.max_steering_angle_rad = math.radians(self.max_steering_angle)
        # self.context = {
        #     "context_map": {"entity": "junction"},
        #     "context_model": {"ml_type": "decision","input": "3","feature": "1"}
        # }
        # self.iakm_request = IAKMRequest(self.context)
        # #self.model_control = RandomForestClassifier(random_state=42)
        # self.model_control = self.iakm_request.get_model()
        self.model_control = pickle.load(open("model.sav", 'rb'))

    def update(self,world, debug):
        #region "properties"
        self.transform = self.actor.get_transform()
        fwd_vector = self.transform.get_forward_vector()
        self.steer = self.actor.get_control().steer
        self.steer_angle = self.steer * self.max_steering_angle
        self.centroid = self.transform.location + carla.Location(z=1)
        self.bumper = self.centroid + (self.actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (self.actor.bounding_box.extent.x * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.centroid_in_ra = vehicle_in_square(self.centroid)
        self.bumper_in_sq = vehicle_in_square(self.bumper)
        self.rear_in_ra = vehicle_in_square(self.rear)
        tangent = get_tangent(CENTER, self.bumper)
        self.direction = self.bumper + (10 * fwd_vector)
        speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
        self.speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
        self.speed_km = round(3.6 * self.speed, 2)
        self.in_control_box = True if P(self.bumper.x, self.bumper.y).within(poly_CONTROL) else False
        self.in_square = True if P(self.bumper.x, self.bumper.y).within(poly_SQUARE) else False
        self.in_poly_target = True if P(self.bumper.x, self.bumper.y).within(poly_TARGET) else False
        self.wp_index = get_nearest_index(self.transform, self.list_wps, self.wp_index, self.bumper)
        self.bumper_in_sq = True
        self.closest_veh = None
        message = "initial"
        #endregion

        if self.spot_wp_index > self.index_front:
            self.dist_spot = (self.spot_wp_index - self.index_front)  * nextInterval
            self.time_to_spot = self.get_time_to_spot()
            # debug.draw_string(self.centroid, "t: "+str(round(self.time_to_spot,2)), False, COLOR_CARLA_YELLOW, 0.05)
            # debug.draw_string(self.bumper, "d: "+str(round(self.dist_spot,2)), False, COLOR_CARLA_YELLOW, 0.05)

        if self.in_control_box:
            message = "t50"
            self.dist_sq = distance_point_to_line(self.bumper, BORDER_p1, BORDER_p2)
            self.dist_sq_normalized = self.dist_sq / BOX_CONTROL_DIST
            self.closest_veh, distance_other_spot = self.get_closest_vehicle_polygone(world, poly_COMING)
            
            if self.closest_veh:
                closest_bumper, closest_rear   = getBumper(self.closest_veh) , getRear(self.closest_veh)
                closest_speed    = math.sqrt(self.closest_veh.get_velocity().x ** 2 + self.closest_veh.get_velocity().y ** 2)
                debug.draw_line(self.bumper, closest_rear, thickness=0.05, color=COLOR_CARLA_RED, life_time=0.15)
                other_time_spot = round((distance_other_spot/closest_speed), 2)

                if ROLE =='ai':
                    if self.model_control != None:
                        x = pd.DataFrame({
                            "ego_time": [self.time_to_spot],
                            "other_time": [other_time_spot],
                            "ego_speed": [self.speed_km],
                            "distance_ra": [self.dist_sq_normalized]
                        })
                        message = self.model_control.predict(x)[0]
                    else:
                        self.throttle, self.brake = 0, 0.5
                if ROLE == 'trainer':
                    timme_diff = other_time_spot - self.time_to_spot
                    message = "b100"
                    if timme_diff >= 1:
                        message = "t50"
                    elif timme_diff < 1:
                        if self.speed_km < 1:
                            if   self.dist_sq_normalized >= 0.3:  message = "t30"
                            else:                                 message = "b10"
                        elif self.speed_km < 6:
                            if   self.dist_sq_normalized >= 0.8:  message = "t10"
                            elif self.dist_sq_normalized >= 0.6:  message = "t00"
                            elif self.dist_sq_normalized >  0.4:  message = "b10"
                            elif self.dist_sq_normalized <= 0.4:  message = "b20"
                        else:
                            if   self.dist_sq_normalized >= 0.8: message = "b10"
                            elif self.dist_sq_normalized >= 0.6: message = "b20"
                            elif self.dist_sq_normalized >  0.4: message = "b30"
                            elif self.dist_sq_normalized <= 0.4: message = "b50"
                    self.dataset.append([timme_diff,
                                         self.speed_km,
                                         self.dist_sq_normalized,
                                         message])
                #debug.draw_string(closest_bumper, str(round(other_time_spot, 2)), False, COLOR_CARLA_YELLOW, 0.1)
            else:
                self.throttle, self.brake = 0.5, 0
        if self.in_square:
            message = "static"
        if self.in_poly_target:
            message = "fast"

        #region "Calculate Steer"
        center_axle_x, center_axle_y = self.bumper.x, self.bumper.y
        self.index_back = get_nearest_index(self.transform, self.list_wps, self.index_back, self.bumper)
        self.index_front = self.index_back + 1
        #print(self.index_back, self.index_front)
        back_x, back_y = self.list_wps[self.index_back].transform.location.x, self.list_wps[
            self.index_back].transform.location.y
        front_x, front_y = self.list_wps[self.index_front].transform.location.x, self.list_wps[
            self.index_front].transform.location.y
        yaw_path = np.arctan2(front_y - back_y, front_x - back_x)
        yaw_diff = yaw_path - math.radians(self.yaw)
        if yaw_diff > np.pi:   yaw_diff -= 2 * np.pi
        if yaw_diff < - np.pi: yaw_diff += 2 * np.pi
        crosstrack_error = DistancePointLine(center_axle_x, center_axle_y, back_x, back_y, front_x, front_y)
        yaw_cross_track = np.arctan2(center_axle_y - front_y, center_axle_x - front_x)
        yaw_path2ct = yaw_path - yaw_cross_track
        if yaw_path2ct > np.pi:   yaw_path2ct -= 2 * np.pi
        if yaw_path2ct < - np.pi: yaw_path2ct += 2 * np.pi
        if yaw_path2ct > 0:
            crosstrack_error = abs(crosstrack_error)
        else:
            crosstrack_error = - abs(crosstrack_error)
        yaw_diff_crosstrack = np.arctan(k_e * crosstrack_error / (k_v + self.speed))
        steer_expect = yaw_diff + yaw_diff_crosstrack
        if steer_expect > np.pi:   steer_expect -= 2 * np.pi
        if steer_expect < - np.pi: steer_expect += 2 * np.pi
        steer_expect = min(self.max_steering_angle_rad, steer_expect)
        steer_expect = max(-self.max_steering_angle_rad, steer_expect)
        steer_normalized = steer_expect / self.max_steering_angle_rad
        self.steer = steer_normalized
        #endregion

        #debug.draw_string(self.rear,  f"{message} s: {round(self.speed_km,2)}" , False, COLOR_CARLA_YELLOW, 0.05)
        self.setControl(message)
    def setControl(self, message):
        if message == 'static':
            self.throttle, self.brake = 0.5 ,0
        elif message == "initial":
            coef = (self.speed_km/100) if self.speed_km > 25 else 0
            self.throttle, self.brake = (0.5-coef), 0
        elif message == 'fast':
            self.throttle, self.brake = 0.75 ,0
        else:
            if 't' in message:
                self.brake = 0
                self.throttle = int(message.replace('t',''))/100
            if 'b' in message:
                self.throttle = 0
                self.brake = int(message.replace('b',''))/100


        control = self.actor.get_control()
        control.steer = self.steer
        control.throttle = self.throttle
        control.brake = self.brake
        self.actor.apply_control(control)

    def saveDataset(self):
        if len(self.dataset) == 0: return
        dataset_np = np.array(self.dataset)
        Biodata_small = {'time_diff': dataset_np[:, 0],
                         "ego_speed": dataset_np[:, 1],
                         'distance_ra': dataset_np[:, 2],
                         'message': dataset_np[:, 3].tolist() }
        try:
            df = pd.DataFrame(Biodata_small)
            df.to_csv(FILE_DATASET, mode='a', index=False, header=False)
            self.dataset = []
        except Exception:
            traceback.print_exc()
    def train_model(self):
        if len(self.dataset) == 0:
            return
        if self.model_control is None:
            print("Model is None: Not trained")
            return
        self.iakm_request.samples = len(self.dataset)
        df = pd.DataFrame(np.array(self.dataset),
                          columns=["time_diff", "ego_speed", "distance_ra", "message"])
        print(df)
        X, y = df[["time_diff", "ego_speed", "distance_ra"]], df["message"]
        self.model_control.fit(X, y)  # .ravel()
        print("Trained !")
        self.dataset = []
        pickle.dump(self.model_control, open("model.sav", 'wb'))
    def train_model_from_file(self):
        pima = pd.read_csv("dataset.csv")
        if len(pima) == 0: return
        feature_cols = ['time_diff','ego_speed','distance_ra']
        X,y  = pima[feature_cols], pima.message  # Features
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=None)  # 70% training and 30% test
        self.model_control.fit(X_train, y_train)
        y_pred = self.model_control.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)
        print(f"Accuracy-RF: {accuracy}")
        pickle.dump(self.model_control, open("model.sav", 'wb'))
    def get_time_to_spot(self):
        diff,index1 = 1000, -1
        for i in range(len(self.model_thr)): # [speed, distance, time]
            delta = abs(self.model_thr[i][0] - self.speed)
            if delta < diff:
                index1 = i
                diff = delta
        initial_distance = self.model_thr[index1][1]
        initial_time = self.model_thr[index1][2]
        final_distance = initial_distance + self.dist_spot
        diff,index2 = 1000, -1
        for j in range(index1+1,len(self.model_thr)):
            delta_distance = abs(self.model_thr[j][1] - final_distance)
            if delta_distance < diff:
                index2 = j
                diff = delta_distance
        required_time = self.model_thr[index2][2] - initial_time
        return required_time
    def get_closest_vehicle_polygone(self, world, poly):
        veh, dist = None, 1000.0
        for other in [x for x in world.get_actors().filter('vehicle.*')
                      if P(x.get_location().x, x.get_location().y).within(poly)]:
            d = getDistance(getBumper(other), self.spot)
            if d < dist: veh, dist = other, d
        return veh, dist
    def distance_to_square(self):
        try:
            route_to_spot = planner.trace_route(self.bumper, self.wp_border_ra.transform.location)
            return (len(route_to_spot)-1) * nextInterval
        except:
            traceback.print_exc()

    def setPath(self,world, wp = None):
        if wp == None: _, wp = get_nearest_wp(self.centroid, wps)
        self.list_wps = []
        LEN = 5000
        d_max = 100
        while len(self.list_wps) < LEN:
            if self.spot is None:
                p = P(wp.transform.location.x, wp.transform.location.y)
                if p.within(poly_TARGET):
                    self.spot = carla.Location(wp.transform.location.x, wp.transform.location.y,10)
                    self.spot_wp_index = len(self.list_wps)-1
                    world.debug.draw_string(self.spot, "*", False, COLOR_CARLA_RED, 25)
            d= getDistance(wp.transform.location, EGO_ENTRY)
            if d < d_max:
                self.wp_border_ra = wp
                d_max = d
            myWaypoint = MyWaypoint(wp)
            self.list_wps.append(myWaypoint)
            nexts = wp.next(nextInterval)
            wp = nexts[0]
            #world.debug.draw_string(wp.transform.location, ".", False, COLOR_CARLA_YELLOW, 50)



def spawn( world, blueprints):
    spawn_points = get_spawnpoints(world)
    random.shuffle(spawn_points)
    ego_bp = blueprints.find('vehicle.tesla.model3')
    ego_bp.set_attribute('color', '255,0,0')
    actor_ego = world.spawn_actor(ego_bp, random.choice(spawn_points))
    wp_ego = random.choice(ego_sps)
    actor_ego.set_transform(wp_ego.transform)
    actor_ego.set_autopilot(AUTO_PILOT,  traffic_manager.get_port())
    veh_ego = Vehicle(actor_ego)
    veh_ego.setPath(world, wp=wp_ego)
    veh_ego.actor.set_transform(veh_ego.list_wps[0].transform)
    return veh_ego

wps, vehicles, waiting_vehicles, ego_sps = [],[],[],[]
planner = None
loop = True
traffic_manager = None
def main():
    global actors, loop, ego_sps, wps, vehicles, waiting_vehicles , planner, traffic_manager
    #logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    try:
        #region Global-Settings
        client = carla.Client('127.0.0.1', 2000)
        client.set_timeout(10)
        traffic_manager = client.get_trafficmanager(8010)
        traffic_manager.set_synchronous_mode(False)
        world = client.get_world() #world = client.load_world("Town03")
        map = world.get_map()
        wps = map.generate_waypoints(0.5)
        ego_sps = [p for p in wps
                   if 91.5 + (1.2 * BOX_CONTROL_DIST) < p.transform.location.x < 91.5 + ( 4 * BOX_CONTROL_DIST)
                   and -79 < p.transform.location.y < -75.5]
        debug = world.debug
        draw_polygones(debug)
        dao = global_route_planner_dao.GlobalRoutePlannerDAO(map, nextInterval)
        planner = global_route_planner.GlobalRoutePlanner(dao)
        planner.setup()
        # traffic_manager = client.get_trafficmanager(tm_port)
        # traffic_manager.set_synchronous_mode(True)
        #traffic_manager.global_distance_to_leading_vehicle(5)
        blueprints = world.get_blueprint_library()
        #endregion

        ego = spawn(world, blueprints)
        clock = pygame.time.Clock()
        actors = world.get_actors().filter('vehicle.tesla.model3')
        vehicles.append(ego)
        while loop:
            for veh in vehicles:
                try:
                    veh.update(world, debug)
                    if veh.centroid.y < -90:
                        # if ROLE == "trainer":
                        #     veh.saveDataset()
                        #     veh.train_model_from_file()
                        #     #veh.train_model()
                        #     veh.iakm_request.push_model()
                        sys.exit()
                except Exception:
                        traceback.print_exc()
            clock.tick_busy_loop(int(SPT / Delta_Time))
            if len(vehicles) == 0: loop = False
    finally:
        all = world.get_actors().filter('vehicle.tesla.model3')
        ids = [x._id for x in vehicles]
        for actor in [x for x in all if x.id in ids ]: actor.destroy()
        print("---------------------------------------------------------------------")
        print("----------------------- End of Simulation ---------------------------")
        print("---------------------------------------------------------------------")

if __name__ == '__main__':
    main()