import ast
import re
import nltk
from jsonschema import validate
import jsonschema
# nltk.download('wordnet')
# nltk.download('stopwords')
# nltk.download('nltk')
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
STOPWORDS_ADDITIONAL = ["equal", "number"]
SPLITTER_PATTERN = '[._;,]'
lemmatizer = WordNetLemmatizer()
PREFIX_entity = "__entity__"



def getAttribute(sentence):
    list_stops = stopwords.words('english')
    for item in STOPWORDS_ADDITIONAL: list_stops.append(item)
    stopFilter = set(list_stops)
    word_tokens = word_tokenize(sentence)
    word_tokens = [w.lower() for w in word_tokens]
    word_tokens = [w for w in word_tokens if not w in stopFilter]
    word_tokens = [lemmatizer.lemmatize(w) for w in word_tokens ]
    new_sentence = "_".join([str(item) for item in word_tokens])
    return new_sentence

def formulateContext(context):
    new_context = {"entity":None, "attributes":[]}
    new_context["entity"] = lemmatizer.lemmatize(context["entity"]).capitalize()
    for attribute in context["attributes"]:
        new_attribute = {}
        for key,value in attribute.items():
            if key == "name":
                new_value = getAttribute(re.split(SPLITTER_PATTERN, value)[0])
                new_attribute["name"] = new_value
            else:
                new_attribute[key] = value
        new_context["attributes"].append(new_attribute)
    return new_context

def serializeTopic(topic:str):
  object = {
    "type":[],
    "properties":{}
  }
  elememts = topic.split('/')
  elememts = [x for x in elememts if len(x.strip()) > 3]
  for element in elememts:
    key = element.split('=')[0]
    value = element.split('=')[1]
    if key == PREFIX_entity:
      object["type"].append(value)
    else:
      try:
        data = ast.literal_eval(value)
      except:
        data = value
      object["properties"][key] = data
  return object

def loadTopic(object:dict):
  list_type = object["type"]
  properties = object["properties"]
  list_result = []
  for type in list_type:
    list_result.append(f"{PREFIX_entity}={type}")
  for key, value in properties.items():
    list_result.append(f"{key}={value}")
  return '/'.join(list_result)

from jsonschema import validate
import jsonschema

schema_set = {
    "$schema": "https://json-schema.org/draft/2019-09/schema",
    "type": "object",
    "properties": {
        "entity": {"type": "string"},
        "attributes": {
            "type": "array",
            "items":{
                "type":"object",
                "required": [ "name", "value" ],
                "additionalProperties": False,
                "properties": {
                    "name":  {"type": "string"},
                    "value": {"type": ["integer", "number", "string"]},
                    "approx":{"type": "number"},
                    "weight":{"type": "number"},
                },
                "if":{
                    "properties": {
                        "value": {
                            "type": ["string"]
                        }
                    }
                },
                "then":{
                    "not" : {
                        "required": ["approx"]
                    }
                }
            }
        }
    }
}

schema_get = {
    "$schema": "https://json-schema.org/draft/2019-09/schema",
    "type": "object",
    "properties": {
        "entity": {"type": "string"},
        "attributes": {
            "type": "array",
            "items":{
                "type":"object",
                "required": [ "name", "value" ],
                "additionalProperties": False,
                "properties": {
                    "name":  {"type": "string"},
                    "value": {"type": ["integer", "number", "string"]}
                }
            }
        }
    }
}



class InvalidJsonSchemaException(Exception):
    def __init__(self):
        self.message = "JSON format is not Valid!"
        super().__init__(self.message)

def validateJson(obj, method = "set"):
    res = False
    schema = schema_set if method == "set" else schema_get
    try:
        validate(instance=obj,schema=schema)
        res= True
    except:
        raise InvalidJsonSchemaException()
    return res


# topic_mqtt = "__entity__=Intersection/__entity__=Round/str=abc/nbLane=2/nbEntry=4/nbExit=4/radius=34.5"
# a = serializeTopic(topic_mqtt)
# print(a)
# b = loadTopic(a)
# print(b)

# PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
# PREFIX owl: <http://www.w3.org/2002/07/owl#>
# PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
# PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
# PREFIX iakm: <http://www.example.org/IAKM.owl#>
#             SELECT ?model_id ?mapEntity
#             {
#                 ?model rdf:type iakm:Models;
#                        iakm:hasMapContext ?mapEntity;
#                        iakm:model_id ?model_id .
# ?mapEntity a iakm:Intersection .
# ?mapEntity a iakm:Round .

# ?mapEntity iakm:nbLane ?nbLane .
#  FILTER(?nbLane = 2)
# ?mapEntity iakm:radius ?radius .
#  FILTER(?radius = 34.5)







