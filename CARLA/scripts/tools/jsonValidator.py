from jsonschema import validate
import jsonschema

schema_set = {
    "$schema": "https://json-schema.org/draft/2019-09/schema",
    "type": "object",
    "properties": {
        "entity": {"type": "string"},
        "attributes": {
            "type": "array",
            "items":{
                "type":"object",
                "required": [ "name", "value" ],
                "additionalProperties": False,
                "properties": {
                    "name":  {"type": "string"},
                    "value": {"type": ["integer", "number", "string"]},
                    "approx":{"type": "number"},
                    "weight":{"type": "number"},
                },
                "if":{
                    "properties": { 
                        "value": { 
                            "type": ["string"] 
                        }
                    }
                },
                "then":{
                    "not" : {
                        "required": ["approx"]
                    }
                }  
            }
        }
    }        
}

schema_get = {
    "$schema": "https://json-schema.org/draft/2019-09/schema",
    "type": "object",
    "properties": {
        "entity": {"type": "string"},
        "attributes": {
            "type": "array",
            "items":{
                "type":"object",
                "required": [ "name", "value" ],
                "additionalProperties": False,
                "properties": {
                    "name":  {"type": "string"},
                    "value": {"type": ["integer", "number", "string"]}
                }
            }
        }
    }        
}

class InvalidJsonSchemaException(Exception):
    def __init__(self):
        self.message = "JSON format is not Valid!"
        super().__init__(self.message)

def validateJson(obj, method = "set"):
    res = False
    schema = schema_set if method == "set" else schema_get
    try:
        validate(instance=obj,schema=schema)
        res= True
    except:
        raise InvalidJsonSchemaException()
    return res

