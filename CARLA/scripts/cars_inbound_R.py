#region import
import glob
from datetime import datetime
import traceback
#from threading import Thread
import threading
import subprocess
import pygame
import os
import sys
import time
import json
import csv
import time
import pickle
import numpy as np
####from sklearn.linear_model import LogisticRegression
####from sklearn.model_selection import train_test_split
import random
import math
import argparse
import logging
import subprocess
from termcolor import colored
from dataclasses import dataclass
# from shapely.geometry import Point
# from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt
#import torch
#import logisticRegression
#from logisticRegression import LogisticRegression
import random
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
from time import gmtime, strftime
import carla
import circle_fit as cf
char = "*"
Delta_Time = 0.1
p = os.path.abspath('..')
sys.path.insert(1, p)
from tools import *
from pid_test_update_ic_fcn_final_merge_2 import adv_thr_ctrl
from numpy import linalg as LA
array_crosstrack_error = []
import requests
from pathlib import Path
from dotenv import load_dotenv
load_dotenv()
from utils import *
#endregion
argparser = argparse.ArgumentParser(description=__doc__)
argparser.add_argument('--host',metavar='H', default='127.0.0.1', help='IP of the host server (default: 127.0.0.1)')
argparser.add_argument('--port',metavar='P', default=2000, type=int, help='TCP port to listen to (default: 2000)')
argparser.add_argument('--map',metavar='M', default='Town03', help='MAP to load simulator (default: Town03)')
argparser.add_argument('--threshold',metavar='T', default=0.5, help = "Exit probability threshod")
args = argparser.parse_args()
#region Variables
vehicles, new_points, polygones,list_vehicleSpeeds = [], [], [], []
list_wps, ego_id, ego_actor, veh_ego, timestamp = None,None,None,None,None
k_e =  18.9
k_v =  2.2
MAX_EDGE = 80
NEXT_INTERVAL = 0.0999
LEN_WP = 1360
TM_PORT = 8000
total_veh_count = 10
safety_distance = 8
TARGET_SPEED = 6.95
BRAKE = 0.95
speed_limit = 30
spawn_back_at = 35
distance_attention = 15
distance_attention_margin = 2
speed_threshold = 8
steps_before_forcing = 1000
timeStep = 0
waitingTime = 150
initial_transform = None
total_wait = 0
success_wait = 0
failed_wait = 0
distance_threshold = 0
max_steering_angle_rad = 70 * math.pi/180
#endregion
class Point:
    x: int
    y:  int
    def __init__(self,x,y):
        self.x = x
        self.y = y

# region Functions

def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude
#Calc minimum distance from a point and a line segment (i.e. consecutive vertices in a polyline).
def DistancePointLine(px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)

    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine

    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)

    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)

    return DistancePointLine

def get_nearest_wp(location, wps):
    old_d= float("inf")
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            wp = wps[i]
            index = i
    # if old_d > 2:
    #     return None, None
    #else:
    return index, wp

def get_nearest_index(wps, index_back, bumper):
    index,min_d= 0, float("inf")
    my_x,my_y = bumper.x, bumper.y
    old_range = range(index_back-5,index_back+6)
    new_range = [(i%LEN_WP) if (i >= LEN_WP) else i for i in old_range]
    for i in new_range:
        if i < 0:continue
        elif i > len(wps)-2 : continue
        else:
            wp1, wp2 = wps[i], wps[i + 1]
            wp1_x, wp1_y = wp1.transform.location.x, wp1.transform.location.y
            wp2_x, wp2_y = wp2.transform.location.x, wp2.transform.location.y
            d = DistancePointLine(my_x, my_y, wp1_x, wp1_y, wp2_x, wp2_y)
            if d < min_d:
                min_d = d
                index = i
    return index

def getVehicleSpot_wp_index(list_wp, spot_location):
    distance = float("inf")
    index = 0
    counter = 0
    for i, wp in enumerate(list_wp):
        d = getDistance(wp.transform.location, spot_location)
        if d < distance:
            distance = d
            index = i

        if d > distance: counter +=1
        if counter > 3: break
    return index

def get_nearest_Vehicle(location):
    old_d= float('inf')
    for i in range(len(vehicles)):
        d = getDistance(location, vehicles[i].centroid)
        if d < old_d:
            old_d = d
    return old_d

def get_next_index(veh, wps, index):
    wp_location = wps[index].transform.location
    angle = find_angle(veh.direction,wp_location,veh.centroid)
    if abs(angle) < 90:
        return index
    else:
        return index+1

# endregion

class MyWaypoint(dict):
    radius: float
    v_max_turn : float
    circle_center = Point
    wp: carla.Waypoint
    transform: carla.Transform
    def __init__(self, wp):
        dict.__init__(self, wp=wp)
        self.wp = wp
        self.transform = wp.transform
        self.radius = float('inf')
        self.v_max_turn = 40    # km/h

def vehicle_in_roundabout(location):
    dx = abs(location.x - CENTER.x)
    dy = abs(location.y - CENTER.y)
    if (dx ** 2 + dy ** 2) > (R_BIG) ** 2:
        return False
    return True


class Vehicle(object):
    #region "Vehicle variables"
    _id: int
    actor: carla.Vehicle
    transform: carla.Transform
    initial_transform: carla.Transform
    centroid: carla.Location
    bumper: carla.Location
    rear: carla.Location
    direction: carla.Location
    previous_location: carla.Location
    heading: float
    next_wp_index: int
    dist_exit: float
    dist_exit_normalized: float
    decision_dist: float
    decision_dist_norm: float
    decision_prob: float
    decision_speed: float
    dist_ra:float
    traveled_distance: float
    centroid_in_ra: bool
    bumper_in_ra: bool
    rear_in_ra: bool
    bumper_to_ra:float
    rear_to_ra:float
    length: float
    prob_exit: float
    lateral: float
    steer_angle: float
    control: str
    wp_steer: float = 0
    wp_throttle: float = 0
    steer: float
    throttle: float
    brake: float
    brake_index: int =0
    speed: float
    speed_km: float
    list_speed_km:[]
    list_brake:[]
    max_steering_angle: float
    lane: int = None
    side:str
    lane_change: bool
    array_prediction = []
    array_dist_exit = []
    array_prob_exit = []
    array_distance = []
    array_probability = []
    wp: carla.Waypoint = None
    startTimestep: int = 0
    distance_decision_prediction: float = 0
    isExiting: bool = False
    spot_wp_index: int
    distance_toSpot: float
    time_toSpot: float
    spot_location : carla.Location
    list_wps =[]
    yield_to_veh: int
    yield_list = []
    yaw: float
    index_back: int
    index_front: int
    toward_RA : bool
    prev_in_ra:bool
    heading:float
    distance_by_angle = False
    calling_matlab = 0
    idx_box_entry = 0
    idx_box_exit = 0
    #endregion
    def __init__(self,side, actor):
        self._id = actor.id
        self.actor = actor
        self.calling_matlab = 0
        self.spot_wp_index = -1
        self.distance_toSpot = -1
        self.time_toSpot = -1
        self.prev_in_ra = False
        self.transform = actor.get_transform()
        self.previous_location = self.transform.location
        self.centroid = actor.get_location()
        fwd_vector = self.transform.get_forward_vector()
        self.bumper = self.centroid + (actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (actor.bounding_box.extent.x * fwd_vector)
        self.direction = self.bumper + (10 * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.length = getDistance(self.bumper, self.rear)
        self.traveled_distance = 0
        self.steps_total = 0
        self.next_wp_index = 1
        self.list_wps = []
        self.index_back = 0
        self.index_front = 0
        self.heading = 0
        self.dist_exit = None
        self.dist_exit_normalized = None
        self.dist_ra = None
        self.yield_to_veh = None
        self.yield_list = []
        self.decision_dist = None
        self.decision_dist_norm = None
        self.decision_prob = None
        self.decision_speed = None
        self.throttle = 0.3
        self.brake = 0
        self.centroid_in_ra = False
        self.bumper_in_ra = False
        self.rear_in_ra = False
        self.in_box = False
        self.lane_change = random.choice([False])
        self.prob_exit = 0
        self.coming_vehicle: None
        self.array_prediction = []
        self.list_speed_km = []
        self.list_brake = []
        self.array_dist_exit = []
        self.array_prob_exit = []
        physics_control = actor.get_physics_control()
        wheels = physics_control.wheels
        self.max_steering_angle = wheels[0].max_steer_angle
        self.front_vehicle = None

    def update(self,world, debug):
        global main_exit ,distance_threshold
        self.transform = self.actor.get_transform()
        fwd_vector = self.transform.get_forward_vector()
        #self.throttle =self. actor.get_control().throttle
        #self.brake = self.actor.get_control().brake
        self.steer = self.actor.get_control().steer
        self.steer_angle = self.steer * self.max_steering_angle
        self.centroid = self.transform.location + carla.Location(z=1)
        self.bumper = self.centroid + (self.actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (self.actor.bounding_box.extent.x * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.centroid_in_ra = vehicle_in_roundabout(self.centroid)
        self.bumper_in_ra = vehicle_in_roundabout(self.bumper)

        self.rear_in_ra = vehicle_in_roundabout(self.rear)
        self.bumper_to_ra = getDistance(self.bumper,CENTER) - R_BIG
        self.rear_to_ra = getDistance(self.rear,CENTER) - R_BIG
        self.lateral = round((getDistance(self.centroid, CENTER) - R_SMALL) / (LANE_COUNT * LANE_WIDTH), 3)
        tangent = get_tangent(CENTER, self.bumper)
        self.direction = self.bumper + (10 * fwd_vector)
        angle_tangent_direction = find_angle(tangent, self.direction, self.bumper)
        self.heading = -(round(self.steer_angle + angle_tangent_direction, 2))
        self.toward_RA = True if self.heading > 0 else False
        speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
        self.speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
        self.speed_km = round(3.6 * self.speed, 2)
        distance_to_center = getDistance(self.bumper, CENTER)
        self.dist_ra = distance_to_center - R_BIG
        #debug.draw_string(self.rear, str(int(self.speed_km)), False, carla.Color(255, 255, 0), 0.1)

        ########################################################################################
        ########################################################################################
        ########################################################################################
        self.front_vehicle = None
        self.coming_vehicle = None

        self.yield_to_veh = None
        try:
            #region "Calculate Steer"
            center_axle_x, center_axle_y = self.bumper.x, self.bumper.y
            self.index_back = get_nearest_index(self.list_wps, self.index_back, self.bumper)
            self.index_front = 0 if self.index_back == (len(self.list_wps)-1) else (self.index_back+1)
            back_x, back_y = self.list_wps[self.index_back].transform.location.x, self.list_wps[self.index_back].transform.location.y
            front_x, front_y = self.list_wps[self.index_front].transform.location.x, self.list_wps[self.index_front].transform.location.y
            yaw_path = np.arctan2(front_y - back_y, front_x - back_x)
            yaw_diff = yaw_path - math.radians(self.yaw)
            if yaw_diff > np.pi:   yaw_diff -= 2 * np.pi
            if yaw_diff < - np.pi: yaw_diff += 2 * np.pi
            speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
            v = math.sqrt(speed_x ** 2 + speed_y ** 2)
            crosstrack_error = DistancePointLine(center_axle_x, center_axle_y, back_x, back_y, front_x, front_y)
            yaw_cross_track = np.arctan2(center_axle_y - front_y, center_axle_x - front_x)
            yaw_path2ct = yaw_path - yaw_cross_track
            if yaw_path2ct > np.pi:   yaw_path2ct -= 2 * np.pi
            if yaw_path2ct < - np.pi: yaw_path2ct += 2 * np.pi
            if yaw_path2ct > 0:
                crosstrack_error = abs(crosstrack_error)
            else:
                crosstrack_error = - abs(crosstrack_error)
            yaw_diff_crosstrack = np.arctan(k_e * crosstrack_error / (k_v + v))
            steer_expect = yaw_diff + yaw_diff_crosstrack
            if steer_expect > np.pi:   steer_expect -= 2 * np.pi
            if steer_expect < - np.pi: steer_expect += 2 * np.pi
            steer_expect = min(max_steering_angle_rad, steer_expect)
            steer_expect = max(-max_steering_angle_rad, steer_expect)
            steer_normalized = steer_expect / max_steering_angle_rad
            #endregion

            self.steer = steer_normalized
            self.setControl()
        except:
            traceback.print_exc()


    def plot(self):
        plt.plot(self.array_distance, self.array_probability, c="#FF8888", label="exited")
        plt.axvline(x=self.decision_dist_norm, color='blue')
        plt.xlabel("Distance")
        plt.ylabel("Probability")
        plt.legend()
        plt.show()
    def calculateBrake(self):
        ratio_distance = 1- (self.dist_ra/distance_attention)
        ratio_speed = self.speed_km/speed_limit
        brake = 0.2 * ratio_distance + 0.2 * ratio_speed + 0.6 * self.brake
        return brake
    def setControl(self):
        control = self.actor.get_control()
        control.steer = self.steer
        control.throttle = self.throttle
        control.brake = self.brake
        #if control.brake > 0.1:print(f"{self._id} is brake: {round(control.brake,2)} and Throttle: {round(control.throttle,2)}")
        self.actor.apply_control(control)

def get_polygones(debug):
        circles = []
        circle_1 = []
        circle_2 = []
        countd = 1
        radius_1 = R_SMALL + (2 * LANE_WIDTH)
        radius_2 = R_SMALL + (2 * LANE_WIDTH) +10
        for i in range(360):
            angle = countd * (math.pi / 180)
            x1,y1 = radius_1 * math.sin(angle), radius_1 * math.cos(angle)
            x2,y2 = radius_2 * math.sin(angle), radius_2 * math.cos(angle)
            p1 = carla.Location(x1 + X_CENTER, y1 + Y_CENTER)
            p2 = carla.Location(x2 + X_CENTER, y2 + Y_CENTER)
            circle_1.append(p1)
            circle_2.append(p2)
            countd += 1
        circles.append(circle_1)
        circles.append(circle_2)

        for i in range(len(circle_1)-1):
            if (i >5 and i< 35) or ( i > 96 and i < 122) or ( i> 185 and i< 213) or ( i> 273 and i< 292 ):
                p1, p2 = circle_1[i], circle_1[i+1]
                #debug.draw_line(p1, p2,thickness=0.3, color=COLOR_CARLA_RED, life_time=10000)

def spawn(world, blueprints, debug, transform):
    try:
        ego_bp = blueprints.find('vehicle.tesla.model3')
        ego_bp.set_attribute('color', '0, 0, 0')
        actor_ego = world.spawn_actor(ego_bp, transform)
        actor_ego.set_autopilot(False, traffic_manager.get_port())
        veh_ego = Vehicle("right", actor_ego)
        wp_index, wp = get_nearest_wp(transform.location, wps)
        veh_ego.list_wps = []
        while len(veh_ego.list_wps) < LEN_WP :
            myWaypoint = MyWaypoint(wp)
            veh_ego.list_wps.append(myWaypoint)
            location = wp.transform.location
            if max(abs(location.x), abs(location.y)) > MAX_EDGE: break
            nexts = wp.next(NEXT_INTERVAL)
            if len(nexts) == 2:
                index_selected = -1
                if getDistance(nexts[0].next(5)[0].transform.location, CENTER) > R_BIG: index_selected = 1
                if getDistance(nexts[1].next(5)[0].transform.location, CENTER) > R_BIG: index_selected = 0
                wp = nexts[index_selected]

            elif len(nexts) > 0: wp = nexts[0]
            else: wp = None
    except Exception:
        traceback.print_exc()
        pass

    return veh_ego, actor_ego

def start_vehicles(world, blueprints, debug):
    global  vehicles
    sleep = [4.4,12]
    tranforms = get_inbound_spawnpoint(world)
    for i in range(6):
        #for t in tranforms:
        veh_ego, actor_ego = spawn(world, blueprints, debug, tranforms[0])
        vehicles.append(veh_ego)
        time.sleep(4.4)

### new function
def find_angle_degree(CENTER, START, END):
    delta_start_x = START.x - CENTER.x
    delta_start_y = START.y - CENTER.y
    angle_point_degrees = math.degrees(math.atan2(delta_start_y, delta_start_x))

    delta_spot_x = END.x - CENTER.x
    delta_spot_y = END.y - CENTER.y
    delta_spot_degrees = math.degrees(math.atan2(delta_spot_y, delta_spot_x))

    angle_start_end = (360 + (angle_point_degrees - delta_spot_degrees)) % 360
    return angle_start_end

def find_arc_length(angle_degree, R):
    angle_radian = math.radians(angle_degree)
    arc_length = angle_radian * R
    return arc_length

array_drawn_others, wps, vehicles = [],[],[]
TOP_entry_X, TOP_entry_Y =20.306316, -10.241057
RIGHT_entry_X, RIGHT_entry_Y = 14.902605, 17.888777
BOTTOM_entry_X, BOTTOM_entry_Y = -19.968517, 13.354440
LEFT_entry_X, LEFT_entry_Y = -15.191403, -17.738312
RIGHT_SPOT = carla.Location(x=RIGHT_entry_X, y=RIGHT_entry_Y, z=0.5)
TOP_SPOT = carla.Location(x=TOP_entry_X, y=TOP_entry_Y, z=0.5)
LEFT_SPOT = carla.Location(x=LEFT_entry_X, y=LEFT_entry_Y, z=0.5)
BOTTOM_SPOT = carla.Location(x=BOTTOM_entry_X, y=BOTTOM_entry_Y, z=0.5)
traffic_manager  = None
def main():
    global timestamp, initial_transform, timeStep
    global wps, vehicles
    global ego_id, ego_actor,veh_ego, traffic_manager
    loop = True
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

    taw_brake = 1 / (math.log(20))  # 95% response at 1 sec
    BRAKE_LIST = [round(-0.375 * math.exp(-i * Delta_Time / taw_brake) + 0.375, 4) for i in
                  range(round(50 / Delta_Time))]
    BRAKE_LIST.pop(0)

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    client = carla.Client('127.0.0.1', 2000)
    client.set_timeout(10)
    traffic_manager = client.get_trafficmanager(8050)

    try:
        #region Global-Settings
        world = client.get_world()
        traffic_manager.set_synchronous_mode(False)
        map = world.get_map()
        debug = world.debug
        get_polygones(debug)
        in_path_spot_x, in_path_spot_y =  -23.24,5.98
        out_path_spot_x, out_path_spot_y = -24.19,6.36
        spot_x = (in_path_spot_x + out_path_spot_x)/2
        spot_y = (in_path_spot_y + out_path_spot_y)/2
        # CARLA_BOTTOM_SPOT = carla.Location (spot_x, spot_y)
        wps = map.generate_waypoints(0.5)
        # OTHER_SPOT = carla.Location(-22.61, 8.05)  # carla.Location(-22.22 ,9.06)
        # EGO_SPOT = carla.Location(-21.96, 9.78)
        # OTHER_RIGHT_SPOT = carla.Location(11.46,20.42)  # carla.Location(-22.22 ,9.06)
        # EGO_RIGHT_SPOT = carla.Location(12.02, 20.35)
        # EGO_RIGHT_SPOT = carla.Location(12.02, 20.35)
        # debug.draw_string(OTHER_SPOT, ".", False, carla.Color(0, 255, 0), 500)
        # debug.draw_string(EGO_SPOT, "*", False, carla.Color(0, 255, 0), 500)
        # debug.draw_string(OTHER_RIGHT_SPOT, ".", False, carla.Color(0, 255, 0), 500)
        # debug.draw_string(EGO_RIGHT_SPOT, "*", False, carla.Color(0, 255, 0), 500)
        # for wp in wps:
        #     location = wp.transform.location
        #     #     d = getDistance(CENTER, location)
        #     #     if R_SMALL + LANE_WIDTH < d < R_BIG:
        #     debug.draw_string(location,f"{round(location.x,2)},{round(location.y,2)}", False, carla.Color(0, 0, 0), 500)
        # spectator = world.get_spectator()
        # spectator.set_transform(carla.Transform(carla.Location(x=0, y=0, z=80),carla.Rotation(pitch=-90)))

        blueprints = world.get_blueprint_library()
        spawn_points = get_spawnpoints(world)
        random.shuffle(spawn_points)
        #endregion

        spot_x ,spot_y = BOTTOM_entry_X, BOTTOM_entry_Y
        spot_x, spot_y = TOP_entry_X, TOP_entry_Y
        spot_x ,spot_y = RIGHT_entry_X, RIGHT_entry_Y
        spot_x ,spot_y = LEFT_entry_X, LEFT_entry_Y
        #debug.draw_string(LEFT_SPOT, "*", False, carla.Color(0, 255, 0), 100)


        counter = 0
        threading.Timer(0, start_vehicles, (world, blueprints, debug)).start()
        while loop:
            counter += 1
            for veh in vehicles:
                try:
                    veh.update(world, debug)
                    # R = getDistance(CENTER, veh.bumper)
                    # angle_degree = find_angle_degree(CENTER, veh.bumper, LEFT_SPOT)
                    # arc_length = find_arc_length(angle_degree, R)
                except Exception:
                        traceback.print_exc()
            world.tick()
            #world.wait_for_tick()
    finally:
        all = world.get_actors().filter('vehicle.tesla.model3')
        ids = [x._id for x in vehicles]
        for actor in [x for x in all if x.id in ids ]: actor.destroy()
        print("---------------------------------------------------------------------")
        print("----------------------- End of Simulation ---------------------------")
        print("---------------------------------------------------------------------")

if __name__ == '__main__':
    main()
