#region import
import glob
from datetime import datetime
import traceback
#from threading import Thread
import threading
import subprocess
import pygame
import os
import sys
#from solver import *
import time
import json
import csv

import time
import pickle
import numpy as np
####from sklearn.linear_model import LogisticRegression
####from sklearn.model_selection import train_test_split
import math
import argparse
import logging
import subprocess
from termcolor import colored
from dataclasses import dataclass
# from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.geometry.polygon import Point as P
import matplotlib.pyplot as plt
#import torch
#import logisticRegression
#from logisticRegression import LogisticRegression
import random
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
from time import gmtime, strftime
import carla
import circle_fit as cf
char = "*"
Delta_Time = 0.1
p = os.path.abspath('..')
sys.path.insert(1, p)
from tools import *
from pid_test_update_ic_fcn_final_merge_2 import adv_thr_ctrl
from numpy import linalg as LA
array_crosstrack_error = []
import requests
from pathlib import Path
from dotenv import load_dotenv
from utils import *
#endregion

#region Variables
vehicles, new_points = [], []
list_wps, ego_id, ego_actor, veh_ego, timestamp = None,None,None,None,None
k_e =  18.9
k_v =  2.2
MAX_EDGE = 80
NEXT_INTERVAL = 0.5
LEN_WP = 760
TM_PORT = 8000
safety_distance = 8
TARGET_SPEED = 6.95
BRAKE = 0.95
speed_limit = 30
spawn_back_at = 35
distance_attention = 15
distance_attention_margin = 2
speed_threshold = 8
steps_before_forcing = 1000
timeStep = 0
waitingTime = 150
initial_transform = None
total_wait = 0
success_wait = 0
failed_wait = 0
distance_threshold = 0
max_steering_angle_rad = 70 * math.pi/180
#endregion

class Point:
    x: int
    y:  int
    def __init__(self,x,y):
        self.x = x
        self.y = y

# region Functions
### new function
def find_angle_degree(CENTER, START, END):
    delta_start_x = START.x - CENTER.x
    delta_start_y = START.y - CENTER.y
    angle_point_degrees = math.degrees(math.atan2(delta_start_y, delta_start_x))

    delta_spot_x = END.x - CENTER.x
    delta_spot_y = END.y - CENTER.y
    delta_spot_degrees = math.degrees(math.atan2(delta_spot_y, delta_spot_x))

    angle_start_end = (360 + (angle_point_degrees - delta_spot_degrees)) % 360
    return angle_start_end

def find_arc_length(angle_degree, R):
    angle_radian = math.radians(angle_degree)
    arc_length = angle_radian * R
    return arc_length
def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude

def DistancePointLine(px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)

    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine

    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)

    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)

    return DistancePointLine

def get_nearest_wp(location, wps):
    old_d= float("inf")
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            wp = wps[i]
            index = i
    # if old_d > 2:
    #     return None, None
    #else:
    return index, wp

def get_nearest_index(wps, index_back, bumper):
    index,min_d= 0, float("inf")
    my_x,my_y = bumper.x, bumper.y
    old_range = range(index_back-5,index_back+6)
    new_range = [(i%LEN_WP) if (i >= LEN_WP) else i for i in old_range]
    for i in new_range:
        if i < 0:continue
        elif i > len(wps)-2 : continue
        else:
            wp1, wp2 = wps[i], wps[i + 1]
            wp1_x, wp1_y = wp1.transform.location.x, wp1.transform.location.y
            wp2_x, wp2_y = wp2.transform.location.x, wp2.transform.location.y
            d = DistancePointLine(my_x, my_y, wp1_x, wp1_y, wp2_x, wp2_y)
            if d < min_d:
                min_d = d
                index = i
    return index

def getVehicleSpot_wp_index(list_wp, spot_location):
    distance = float("inf")
    index = 0
    counter = 0
    for i, wp in enumerate(list_wp):
        d = getDistance(wp.transform.location, spot_location)
        if d < distance:
            distance = d
            index = i

        if d > distance: counter +=1
        if counter > 3: break
    return index

def get_nearest_Vehicle(location):
    old_d= float('inf')
    for i in range(len(vehicles)):
        d = getDistance(location, vehicles[i].centroid)
        if d < old_d:
            old_d = d
    return old_d

def get_next_index(veh, wps, index):
    wp_location = wps[index].transform.location
    angle = find_angle(veh.direction,wp_location,veh.centroid)
    if abs(angle) < 90:
        return index
    else:
        return index+1

# endregion

class MyWaypoint(dict):
    radius: float
    v_max_turn : float
    circle_center = Point
    wp: carla.Waypoint
    transform: carla.Transform
    def __init__(self, wp):
        dict.__init__(self, wp=wp)
        self.wp = wp
        self.transform = wp.transform
        self.radius = float('inf')
        self.v_max_turn = 40    # km/h

z = 8
poly_OUT = Polygon([(100,-71,z),(100,-74,z),(138,-73,z),(138,-70,z)])
poly_IN = Polygon([(81,-68,z),(78,-68,z),(77,-25,z),(80,-25,z)])
poly_COMING = Polygon([(83,-120,z+1),(80,-120,z+1),(78.6,-84,z+1),(81.4,-84,z+1)])
EGO_SPOT = carla.Location(79.49, -68.58)
SPECTATOR = carla.Location(79.49, -68.58, 50)
OTHER_SPOT = carla.Location(79.39, -68.68)
CCC = carla.Location(125, -75.5)
CENTER_RA = carla.Location(0, 0)
AUTO_PILOT = False
NB_VEH = 12

class Vehicle(object):
    #region "Vehicle variables"
    _id: int
    actor: carla.Vehicle
    transform: carla.Transform
    initial_transform: carla.Transform
    centroid: carla.Location
    bumper: carla.Location
    rear: carla.Location
    direction: carla.Location
    previous_location: carla.Location
    heading: float
    role: str
    next_wp_index: int
    traveled_distance: float
    length: float
    lateral: float
    steer_angle: float
    control: str
    wp_steer: float = 0
    wp_throttle: float = 0
    steer: float
    throttle: float
    brake: float
    brake_index: int =0
    speed: float
    speed_km: float
    list_speed_km:[]
    list_brake:[]
    max_steering_angle: float
    array_distance = []
    wp: carla.Waypoint = None
    startTimestep: int = 0
    spot_wp_index: int
    distance_toSpot: float
    time_toSpot: float
    spot_location : carla.Location
    list_wps =[]
    yield_to_veh: int
    yield_list = []
    yaw: float
    index_back: int
    index_front: int
    heading:float
    distance_by_angle = False
    idx_box_entry = 0
    idx_box_exit = 0
    #endregion
    def __init__(self, actor, role):
        self._id = actor.id
        self.actor = actor
        self.role = role
        self.spot_wp_index = -1
        self.distance_toSpot = -1
        self.time_toSpot = -1
        self.in_poly_out = False
        self.transform = actor.get_transform()
        self.previous_location = self.transform.location
        self.centroid = actor.get_location()
        fwd_vector = self.transform.get_forward_vector()
        self.bumper = self.centroid + (actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (actor.bounding_box.extent.x * fwd_vector)
        self.direction = self.bumper + (10 * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.length = getDistance(self.bumper, self.rear)
        self.traveled_distance = 0
        self.steps_total = 0
        self.next_wp_index = 1
        self.list_wps = []
        self.index_back = 0
        self.index_front = 0
        self.heading = 0
        self.yield_to_veh = None
        self.yield_list = []
        self.throttle = 0.35        #0.35
        self.brake = 0
        self.in_box = False
        self.coming_vehicle: None
        self.array_prediction = []
        self.list_speed_km = []
        self.list_brake = []
        physics_control = actor.get_physics_control()
        wheels = physics_control.wheels
        self.max_steering_angle = wheels[0].max_steer_angle
        self.front_vehicle = None

    def update(self, debug):
        global distance_threshold
        self.transform = self.actor.get_transform()
        fwd_vector = self.transform.get_forward_vector()
        #self.throttle =self. actor.get_control().throttle
        #self.brake = self.actor.get_control().brake
        self.steer = self.actor.get_control().steer
        self.steer_angle = self.steer * self.max_steering_angle
        self.centroid = self.transform.location + carla.Location(z=1)
        self.bumper = self.centroid + (self.actor.bounding_box.extent.x * fwd_vector)
        self.rear = self.centroid - (self.actor.bounding_box.extent.x * fwd_vector)
        _yaw = self.transform.rotation.yaw
        self.yaw = _yaw if _yaw > 0 else _yaw + 360
        self.lateral = round((getDistance(self.centroid, CENTER) - R_SMALL) / (LANE_COUNT * LANE_WIDTH), 3)
        tangent = get_tangent(CENTER, self.bumper)
        self.direction = self.bumper + (10 * fwd_vector)
        angle_tangent_direction = find_angle(tangent, self.direction, self.bumper)
        self.heading = -(round(self.steer_angle + angle_tangent_direction, 2))
        self.in_poly_out = True if P(self.centroid.x, self.centroid.y).within(poly_IN) else False
        speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
        self.speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
        self.speed_km = round(3.6 * self.speed, 2)
        self.front_vehicle = None
        self.coming_vehicle = None
        self.yield_to_veh = None
        try:
            #region "Calculate Steer"
            center_axle_x, center_axle_y = self.bumper.x, self.bumper.y
            self.index_back = get_nearest_index(self.list_wps, self.index_back, self.bumper)
            if self.index_back == (len(self.list_wps) - 1):self.index_front = 0
            else:                                          self.index_front = (self.index_back+1)
            back_x, back_y = self.list_wps[self.index_back].transform.location.x, self.list_wps[self.index_back].transform.location.y
            front_x, front_y = self.list_wps[self.index_front].transform.location.x, self.list_wps[self.index_front].transform.location.y
            yaw_path = np.arctan2(front_y - back_y, front_x - back_x)
            yaw_diff = yaw_path - math.radians(self.yaw)
            if yaw_diff > np.pi:   yaw_diff -= 2 * np.pi
            if yaw_diff < - np.pi: yaw_diff += 2 * np.pi
            speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
            v = math.sqrt(speed_x ** 2 + speed_y ** 2)
            crosstrack_error = DistancePointLine(center_axle_x, center_axle_y, back_x, back_y, front_x, front_y)
            yaw_cross_track = np.arctan2(center_axle_y - front_y, center_axle_x - front_x)
            yaw_path2ct = yaw_path - yaw_cross_track
            if yaw_path2ct > np.pi:   yaw_path2ct -= 2 * np.pi
            if yaw_path2ct < - np.pi: yaw_path2ct += 2 * np.pi
            if yaw_path2ct > 0:
                crosstrack_error = abs(crosstrack_error)
            else:
                crosstrack_error = - abs(crosstrack_error)
            yaw_diff_crosstrack = np.arctan(k_e * crosstrack_error / (k_v + v))
            steer_expect = yaw_diff + yaw_diff_crosstrack
            if steer_expect > np.pi:   steer_expect -= 2 * np.pi
            if steer_expect < - np.pi: steer_expect += 2 * np.pi
            steer_expect = min(max_steering_angle_rad, steer_expect)
            steer_expect = max(-max_steering_angle_rad, steer_expect)
            steer_expect = max(-max_steering_angle_rad, steer_expect)
            steer_normalized = steer_expect / max_steering_angle_rad
            #endregion

            self.steer = steer_normalized
            add = self.transform.rotation.pitch / 60
            self.throttle = 0.4 + (add if add > 0 else (add * 1.25))
            self.brake = 0
            self.setControl()
        except:
            traceback.print_exc()
    def setControl(self):
        control = self.actor.get_control()
        control.steer = self.steer
        control.throttle = self.throttle
        control.brake = self.brake
        #if control.brake > 0.1:print(f"{self._id} is brake: {round(control.brake,2)} and Throttle: {round(control.throttle,2)}")
        self.actor.apply_control(control)
    def setPath(self,world, wp = None):
        if wp == None:
            _, wp = get_nearest_wp(self.centroid, wps)
        self.list_wps = []
        LEN = 1000 if self.role == "ego" else LEN_WP
        while len(self.list_wps) < LEN:
            #world.debug.draw_string(wp.transform.location, str(len(self.list_wps)), False, COLOR_CARLA_YELLOW, 300)
            myWaypoint = MyWaypoint(wp)
            self.list_wps.append(myWaypoint)
            nexts = wp.next(NEXT_INTERVAL)
            if self.role == "other":
                if len(nexts) == 1:
                    wp = nexts[0]
                else:
                    for n in nexts:
                        nn = n.next(20)[0]
                        p = P(nn.transform.location.x, nn.transform.location.y)
                        if p.within(poly):
                            wp = n
                            break
            else:
                wp = nexts[0]

def start_sequence(world, blueprints):
    global vehicles
    other_bp = blueprints.find('vehicle.tesla.model3')
    other_bp.set_attribute('color', '0,255,0')
    actor_other = world.spawn_actor(other_bp, random.choice(spawn_points))
    wp_other = random.choice(other_sps)
    actor_other.set_transform(wp_other.transform)
    actor_other.set_autopilot(AUTO_PILOT)
    veh_other = Vehicle(actor_other, "other")
    veh_other.setPath(world,wp=wp_other)
    vehicles.append(veh_other)
    for i in range(NB_VEH-1):
        dwell = random.choice(list(range(7,27)))
        wp_other_new = None
        previous = wp_other.previous(dwell)
        while not wp_other_new:
            if len(previous) == 1:
                wp_other_new = previous[0]
            else:
                d_min = 1000
                d_max = 0
                for prev in previous:
                    d = getDistance(CCC, prev.transform.location)
                    if -85.5 < prev.transform.location.y < -75.5:
                        if d > d_max:
                            d_max = d
                            wp_other_new = prev
                    else:
                        if d < d_min:
                            d_min = d
                            wp_other_new = prev

        actor_other = world.spawn_actor(other_bp, random.choice(spawn_points))
        actor_other.set_transform(wp_other_new.transform)
        actor_other.set_autopilot(AUTO_PILOT)
        veh_other = Vehicle(actor_other, "other")
        veh_other.setPath(world,wp=wp_other_new)
        vehicles.append(veh_other)
        wp_other = wp_other_new

other_sps = [],[]
spawn_points = []

poly = Polygon([(82, -133.5,8.2),(80,-2,8.2),(152,-2,8.2),(153,-132,8.2)])
def draw_polygones(debug):
    pts = poly.boundary.coords
    for i in range(len(pts)-1):
        p1 = carla.Location(pts[i][0], pts[i][1], pts[i][2])
        p2 = carla.Location(pts[i+1][0], pts[i+1][1], pts[i][2])
        debug.draw_line(p1, p2, thickness=0.5, color=COLOR_CARLA_BLUE, life_time=100)


def main():
    global timestamp, initial_transform, timeStep
    global wps, vehicles,other_sps, spawn_points
    global ego_id, ego_actor,veh_ego
    loop = True

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    client = carla.Client('127.0.0.1',2000)
    client.set_timeout(10)
    traffic_manager = client.get_trafficmanager(9000)
    traffic_manager.set_synchronous_mode(False)
    world = client.load_world("Town03")
    # weather = world.get_weather()
    # weather.sun_altitude_angle = 90
    # weather.cloudyness = 15
    # weather.precipitation = 10
    # # weather.fog_density = 0
    # # weather.fog_distance = 10
    # world.set_weather(weather)
    # print(world.get_weather())

    time.sleep(3)
    #world = client.get_world()
    debug = world.debug
    try:
        map = world.get_map()
        #wps = map.generate_waypoints(1)
        wps = map.generate_waypoints(0.5)
        other_sps = [x for x in wps
                     if 82 < x.transform.location.x < 85
                     and -110 < x.transform.location.y < -90]
        spectator = world.get_spectator()
        spectator.set_transform(carla.Transform(carla.Location(40, -35, 100),carla.Rotation(pitch=-90)))


        blueprints = world.get_blueprint_library()
        spawn_points = get_spawnpoints(world)
        random.shuffle(spawn_points)
        start_sequence(world, blueprints)
        #draw_polygones(debug)
        # for v in vehicles:
        #     traffic_manager.distance_to_leading_vehicle(v.actor, 0.8)
        #     traffic_manager.vehicle_percentage_speed_difference(v.actor, -20)
        #     c = carla.Color(255, 0, 0) if v.role =="ego" else carla.Color(0, 255, 0)
        #     for p in v.list_wps: world.debug.draw_string(p.transform.location, "*", False, c, 50)
    except: traceback.print_exc()

    while loop:
        # transform_spectator = world.get_spectator().get_transform()
        # location = transform_spectator.location
        # print(location)
        #spectator.set_transform(carla.Transform())
        for veh in vehicles:
            try:
                veh.update(debug)
            except Exception:
                    traceback.print_exc()
        #world.tick()
        world.wait_for_tick()

if __name__ == '__main__':
    main()
