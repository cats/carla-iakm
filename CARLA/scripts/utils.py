import glob
import atexit
import traceback
from threading import Thread
#import schedule
#import pygame
import os
import sys
import time
import json
import csv
import time
import pickle
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import random
import math
import argparse
import logging
import subprocess
from termcolor import colored
from dataclasses import dataclass
# from shapely.geometry import Point
# from shapely.geometry.polygon import Polygone
import matplotlib.pyplot as plt
from dotenv import load_dotenv
try:
    sys.path.append(glob.glob('../../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:pass
import carla

GREY = '128,128,128'
RED ='255,0,0'
GREEN = '0,255,0'
YELLOW = '255,255,0'
BLUE = '0,0,255'
MAGENTA = '255,0,255'
CYAN = '0,255,255'
WHITE = '255,255,255'
path = sys.path[0] + '/../../ra_town03.env'
#path = sys.path[0] + '/../../ra_xodr.env'
b= load_dotenv(dotenv_path=path)
IM_WIDTH = float(os.getenv('IM_WIDTH'))
IM_HEIGHT = float(os.getenv('IM_HEIGHT'))
SLICE_NUMBER = int(os.getenv('SLICE_NUMBER'))
LANE_COUNT = int(os.getenv('LANE_COUNT'))
EXIT_COUNT = int(os.getenv('EXIT_COUNT'))
CIRCLE_COUNT = LANE_COUNT + 1
LANE_WIDTH = float(os.getenv('LANE_WIDTH'))
R_SMALL = float(os.getenv('R_SMALL'))
R_BIG = R_SMALL + (LANE_COUNT * LANE_WIDTH)
D_BIG = 2 * R_BIG
X_CENTER = float(os.getenv('X_CENTER'))
Y_CENTER = float(os.getenv('Y_CENTER'))
#X_CENTER = -1.3
#Y_CENTER = 1.08
SLICE_COUNT = int(os.getenv('SLICE_COUNT'))
SLICE_ANGLE = 360/SLICE_COUNT
COLOR_CARLA_GREEN = carla.Color(0, 200, 0)
COLOR_CARLA_BLUE = carla.Color(0, 0, 200)
COLOR_CARLA_YELLOW = carla.Color(200, 200, 0)
COLOR_CARLA_WHITE = carla.Color(200, 200, 200)
COLOR_CARLA_BLACK = carla.Color(0, 0, 0)
COLOR_CARLA_RED = carla.Color(200, 0, 0)
COLOR_CARLA_PINK = carla.Color(255,192,203)

TOP_exit_X, TOP_exit_Y = float(os.getenv('TOP_exit_X')), float(os.getenv('TOP_exit_Y'))
RIGHT_exit_X, RIGHT_exit_Y = float(os.getenv('RIGHT_exit_X')), float(os.getenv('RIGHT_exit_Y'))
BOTTOM_exit_X, BOTTOM_exit_Y = float(os.getenv('BOTTOM_exit_X')), float(os.getenv('BOTTOM_exit_Y'))
LEFT_exit_X, LEFT_exit_Y = float(os.getenv('LEFT_exit_X')), float(os.getenv('LEFT_exit_Y'))

TOP_entry_X, TOP_entry_Y =20.306316, -10.241057
RIGHT_entry_X, RIGHT_entry_Y = 14.902605, 17.888777
BOTTOM_entry_X, BOTTOM_entry_Y = -19.968517, 13.354440
LEFT_entry_X, LEFT_entry_Y = -15.191403, -17.738312

# TOP_entry_X, TOP_entry_Y = float(os.getenv('TOP_entry_X')), float(os.getenv('TOP_entry_Y'))
# RIGHT_entry_X, RIGHT_entry_Y = float(os.getenv('RIGHT_entry_X')), float(os.getenv('RIGHT_entry_Y'))
# BOTTOM_entry_X, BOTTOM_entry_Y = float(os.getenv('BOTTOM_entry_X')), float(os.getenv('BOTTOM_entry_Y'))
# LEFT_entry_X, LEFT_entry_Y = float(os.getenv('LEFT_entry_X')), float(os.getenv('LEFT_entry_Y'))





CENTER = carla.Location(x=X_CENTER, y=Y_CENTER, z=0.5)
colors = [GREEN,RED,YELLOW,GREY,MAGENTA,CYAN,WHITE,BLUE]
roles = ['GREEN','RED','YELLOW','GREY','MAGENTA','CYAN','WHITE','BLUE']
print_colors = ['green','red','yellow','grey','magenta','cyan','white','blue']

def get_world(client,path):
    with open(path) as od_file:
        try:
            data = od_file.read()
        except OSError:
            print('file could not be readed.')
            sys.exit()
    #print('load opendrive map %r.' % os.path.basename(path))
    vertex_distance = 2.0  # in meters
    max_road_length = 500.0 # in meters
    wall_height = 1.0      # in meters
    extra_width = 0.6      # in meters
    world = client.generate_opendrive_world(data, carla.OpendriveGenerationParameters(vertex_distance=vertex_distance,max_road_length=max_road_length,wall_height=wall_height,additional_width=extra_width,smooth_junctions=True,enable_mesh_visibility=True))
    return world
def getDistance(p1,p2):
    return round(math.sqrt(((p1.x - p2.x) ** 2) + ((p1.y - p2.y) ** 2)), 2)
def getAngleBetween2p(p1, p2):
    xDiff = p2.x - p1.x
    yDiff = p2.y - p1.y
    diff = math.atan2(yDiff, xDiff)
    diff = diff if diff > 0 else diff + 360
    return diff

def vehicle_in_roundabout(location):
    dx = abs(location.x - CENTER.x)
    dy = abs(location.y - CENTER.y)
    if (dx**2 + dy**2) > (R_BIG) **2:
        return False
    return True
def abs_angle(pt_tangent,pt_direction,center):
    direction = (math.degrees(math.atan2(pt_direction.y - center.y, pt_direction.x - center.x))+ 360) % 360
    tangent = (math.degrees(math.atan2(pt_tangent.y - center.y, pt_tangent.x - center.x))+ 360) % 360
    angle = abs(direction - tangent)
    angle = angle if angle < 180 else 360 - angle
    return angle
def find_angle(pt_tangent,pt_direction,center):
    direction = (math.degrees(math.atan2(pt_direction.y - center.y, pt_direction.x - center.x))+ 360) % 360
    tangent = (math.degrees(math.atan2(pt_tangent.y - center.y, pt_tangent.x - center.x))+ 360) % 360
    angle = direction - tangent
    angle = angle if abs(angle) < 180 else angle + 360
    return angle
def angle_directions(ego_transform,wp_transform):
    ego_location = ego_transform.location
    forward_vector_wp = wp_transform.get_forward_vector()
    forward_vector_ego = ego_transform.get_forward_vector()
    direction_wp = wp_transform.location + (2 * forward_vector_wp)
    direction_ego = ego_transform.location + (2 * forward_vector_ego)
    angle_wp = (math.degrees(math.atan2(direction_wp.y - ego_location.y, direction_wp.x - ego_location.x))+ 360) % 360
    angle_ego = (math.degrees(math.atan2(direction_ego.y - ego_location.y, direction_ego.x - ego_location.x))+ 360) % 360
    angle = angle_wp - angle_ego
    if angle > 90: angle = (angle_wp - 360) - angle_ego
    if angle < -90: angle = (angle_wp + 360) - angle_ego
    return angle
def draw_front_vehicles(debug, ego, other, egoToSpot_distance, otherToSpot_distance, _color):
    debug.draw_line(ego.bumper, other.bumper, color=_color, life_time=0.05)
    #debug.draw_string(ego.centroid, str(egoToSpot_distance), False, carla.Color(255, 100, 0), 0.05)
    #debug.draw_string(other.centroid, str(round(otherToSpot_distance)), False, carla.Color(255, 100, 0), 0.05)
def get_tangent(point1, point2, debug=None, draw=False):
    x1, y1 = point1.x, point1.y
    x2, y2 = point2.x, point2.y
    alp1, alp2, alp3 = 45, 90, 45
    u = x2 - x1
    v = y2 - y1
    a3 = math.sqrt(u ** 2 + v ** 2)
    a2 = a3 * math.sin(alp2 * math.pi / 180) / math.sin(alp3 * math.pi / 180);
    RHS1 = x1 * u + y1 * v + a2 * a3 * math.cos(alp1 * math.pi / 180);
    RHS2 = y2 * u - x2 * v - a2 * a3 * math.sin(alp1 * math.pi / 180);
    x3, y3 = (1 / a3 ** 2) * (u * RHS1 - v * RHS2), (1 / a3 ** 2) * (v * RHS1 + u * RHS2)
    arrow_location = carla.Location(x=x3, y=y3, z=0.5)
    if draw: debug.draw_line(point2, arrow_location, color=carla.Color(255, 255, 0), life_time=0.05)
    return arrow_location
def abs_angle(pt_tangent, pt_direction, center):
    direction = (math.degrees(math.atan2(pt_direction.y - center.y, pt_direction.x - center.x)) + 360) % 360
    tangent = (math.degrees(math.atan2(pt_tangent.y - center.y, pt_tangent.x - center.x)) + 360) % 360
    angle = abs(direction - tangent)
    angle = angle if angle < 180 else 360 - angle
    return angle
def get_polygones(debug, draw=False):
    circles = []
    for c in range(CIRCLE_COUNT):
        circle = []
        countd = 1
        radius = R_SMALL + (c * LANE_WIDTH)
        for i in range(SLICE_COUNT):
            angle = SLICE_ANGLE * countd * (math.pi / 180)
            y = radius * math.cos(angle)
            x = radius * math.sin(angle)
            p = carla.Location(x + X_CENTER, y + Y_CENTER)
            circle.append(p)
            countd += 1
        circles.append(circle)

    polygones = []
    for c in range(CIRCLE_COUNT - 1):
        circle1 = circles[c]
        circle2 = circles[c + 1]
        polygons_cirlce = []
        for i in range(SLICE_COUNT - 1):
            p1 = circle1[i]
            p2 = circle2[i]
            p3 = circle2[i + 1]
            p4 = circle1[i + 1]
            polygon = Polygon([(p1.x, p1.y), (p2.x, p2.y), (p3.x, p3.y), (p4.x, p4.y)])
            polygons_cirlce.append(polygon)
            draw_polygon(polygon, debug, 0, carla.Color(255, 0, 0))
            if i == SLICE_COUNT - 2:
                p1 = circle1[i + 1]
                p2 = circle2[i + 1]
                p3 = circle2[0]
                p4 = circle1[0]
                polygon = Polygon([(p1.x, p1.y), (p2.x, p2.y), (p3.x, p3.y), (p4.x, p4.y)])
                polygons_cirlce.append(polygon)
                draw_polygon(polygon, debug, 0, carla.Color(255, 0, 0))
        polygones.append(polygons_cirlce)
    return polygones
def draw_polygon(polygon, debug, lifetime=0.05, color=carla.Color(0, 0, 255)):
    points = []
    for x, y in polygon.exterior.coords:
        p = carla.Location(x, y)
        points.append(p)
    for i in range(len(points) - 1):
        debug.draw_line(points[i], points[i + 1], color=color, life_time=lifetime)
        debug.draw_line(points[i], points[i + 1], color=color, life_time=lifetime)
def get_spawnpoints(world, replacing=False):
    transforms = world.get_map().get_spawn_points()
    filtered_transforms = []
    filtered_transforms = []
    for t in transforms:
        d = getDistance(CENTER, t.location)
        new_yaw = (t.rotation.yaw + 360) % 360
        new_x = t.location.x
        new_y = t.location.y
        map_north = 170 < new_yaw < 190 and new_x > (R_BIG + 1.5)
        map_west = 80 < new_yaw < 100 and new_y < -(R_BIG + 1.5)
        map_south = (350 < new_yaw or new_yaw < 10) and new_x < - (R_BIG + 1.5)
        map_east = 260 < new_yaw < 280 and new_y > (R_BIG + 1.5)
        if (d > R_BIG) and abs(new_x) < 60 and abs(new_y) < 60:
            if (map_north or map_west or map_south or map_east):
                filtered_transforms.append(t)
            # else:
            #    filtered_transforms.append(t)
    return filtered_transforms

def get_ego_transform(world, replacing = False):
    wps = world.get_map().get_spawn_points()  # generate_waypoints(2)
    filtered_transforms = []
    for t in wps:
        d = getDistance(CENTER, t.location)
        direction = t.location + (10 * t.get_forward_vector())
        angle = find_angle(direction, CENTER, t.location)
        if (R_BIG) < d < (R_BIG + 30) and t.location.x > 0 and t.location.y > 0 and abs(angle) < 45:
            filtered_transforms.append(t)
    return filtered_transforms[0]

def get_inbound_spawnpoint(world):
    wps =  world.get_map().get_spawn_points()
    #wps =  world.get_map().generate_waypoints(3)
    filtered_transforms = []
    counter = 0
    for wp in wps:
        d = getDistance(CENTER, wp.location)
        if R_BIG >= d >= (R_SMALL + LANE_WIDTH):
            filtered_transforms.append(wp)
    return filtered_transforms

def get_around_spawnpoint(world,L, radius = 50):
    wps =  world.get_map().get_spawn_points()
    filtered_transforms = []
    for wp in wps:
        d = getDistance(L, wp.location)
        if  radius -30 < d < radius:
            filtered_transforms.append(wp)
    return filtered_transforms

def get_right_spawnpoint(world, replacing = False):
    wps =  world.get_map().get_spawn_points()#generate_waypoints(2)
    filtered_transforms = []
    for t in wps:
        d = getDistance(CENTER, t.location)
        direction = t.location + (10 * t.get_forward_vector())
        angle = find_angle(direction, CENTER, t.location)
        if (R_BIG + 10) < d < (R_BIG + 20) and t.location.x > 0 and t.location.y > 0 and abs(angle) < 45:
            filtered_transforms.append(t)
    return filtered_transforms[0]
def get_top_spawnpoint(world, replacing = False):
    wps =  world.get_map().get_spawn_points()#generate_waypoints(2)
    filtered_transforms = []
    for t in wps:
        d = getDistance(CENTER, t.location)
        direction = t.location + (10 * t.get_forward_vector())
        angle = find_angle(direction, CENTER, t.location)
        if (R_BIG + 10) < d < (R_BIG + 20) and t.location.x > 0 and t.location.y < 0 and abs(angle) < 45:
            filtered_transforms.append(t)
    return filtered_transforms[0]
def get_left_spawnpoint(world, replacing = False):
    wps =  world.get_map().get_spawn_points()#generate_waypoints(2)
    filtered_transforms = []
    for t in wps:
        d = getDistance(CENTER, t.location)
        direction = t.location + (10 * t.get_forward_vector())
        angle = find_angle(direction, CENTER, t.location)
        if (R_BIG + 10) < d < (R_BIG + 20) and t.location.x < 0 and t.location.y < 0 and abs(angle) < 45:
            filtered_transforms.append(t)
    return filtered_transforms[0]
def get_bottom_spawnpoint(world, replacing = False):
    wps =  world.get_map().get_spawn_points()#generate_waypoints(2)
    filtered_transforms = []
    for t in wps:
        d = getDistance(CENTER, t.location)
        direction = t.location + (10 * t.get_forward_vector())
        angle = find_angle(direction, CENTER, t.location)
        if (R_BIG + 10) < d < (R_BIG + 50) and t.location.x < 0 and t.location.y > 0:
            filtered_transforms.append(t)
    return filtered_transforms[0]

def get_others_spawnpoints(world, ego_transform):
    transforms = world.get_map().get_spawn_points()
    filtered_transforms = []
    for t in transforms:
        d = getDistance(CENTER, t.location)
        if d > R_SMALL + LANE_WIDTH and  d < R_BIG and t.location.y < 0 :
            filtered_transforms.append(t)
    random.shuffle(filtered_transforms)
    return filtered_transforms
def get_nearest_wp(location, wps):
    old_d= 100
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            wp = wps[i]
            index = i
    if old_d > 2:
        return None, None
    else:
        return index, wp

def find_angle_degree(CENTER, START, END):
    delta_start_x = START.x - CENTER.x
    delta_start_y = START.y - CENTER.y
    angle_point_degrees = math.degrees(math.atan2(delta_start_y, delta_start_x))

    delta_spot_x = END.x - CENTER.x
    delta_spot_y = END.y - CENTER.y
    delta_spot_degrees = math.degrees(math.atan2(delta_spot_y, delta_spot_x))

    angle_start_end = (360 + (angle_point_degrees - delta_spot_degrees)) % 360
    return angle_start_end

def find_arc_length(angle_degree, R):
    angle_radian = math.radians(angle_degree)
    arc_length = angle_radian * R
    return arc_length

def original(x):
    x = np.array(x)
    x = x.reshape(-1, 1)  # reshape array so that it works with sklearn
    return x


class Point:
    x: int
    y:  int
    def __init__(self,x,y):
        self.x = x
        self.y = y

@dataclass
class Exit:
    index: int
    name: str
    location: carla.Location

@dataclass
class Entry:
    index: int
    name: str
    location: carla.Location

class Arc(object):
    angle: float
    destination: str
    back: Exit = None
    front: Exit = None
    def __init__(self, name, exit_back, exit_front, destination):
        self.name = name
        self.back = exit_back
        self.front = exit_front
        self.destination = destination
        back_x, back_y = self.back.location.x, self.back.location.y
        front_x, front_y = self.front.location.x, self.front.location.y
        #self.total_distance = round(math.sqrt(((back_x - front_x) ** 2) + ((back_y - front_y) ** 2)), 2)
        self.angle_end = (math.degrees(math.atan2(back_y - CENTER.y, back_x - CENTER.x)) + 360) % 360
        self.angle_start = (math.degrees(math.atan2(front_y - CENTER.y, front_x - CENTER.x)) + 360) % 360
        self.angle =(self.angle_end - self.angle_start) if self.angle_start < self.angle_end else (360 - (self.angle_start - self.angle_end))
        #self.angle = 360 - self.angle
        # print(f"zone:{self.angle}")
        self.total_distance = math.radians(self.angle) * R_BIG

    def contains(self, p, debug):
        in_roundabout = getDistance(p, CENTER) < R_BIG  # max([self.back_radius,self.front_radius])
        p_to_start = abs_angle(self.front.location, p, CENTER)
        p_to_end = abs_angle(self.back.location, p, CENTER)
        b = max(p_to_start, p_to_end) < self.angle
        return b and in_roundabout


class MyWaypoint(dict):
    radius: float
    v_max_turn : float
    circle_center = Point
    wp: carla.Waypoint
    transform: carla.Transform
    def __init__(self, wp):
        dict.__init__(self, wp=wp)
        self.wp = wp
        self.transform = wp.transform
        self.radius = float('inf')
        self.v_max_turn = 40    # km/h

@dataclass
class RotationLocation:
    roll: int
    yaw: int
    pitch: int
    x: int
    y: int
    z: int



##################################################
############ Specific for Training  ##############
##################################################
def normalize(x):
    from sklearn import preprocessing as pre
    x = np.array(x)
    x = x.reshape(-1, 1)#reshape array so that it works with sklearn
    x_norm = pre.MinMaxScaler().fit_transform(x)#normalize all values to be between 0 and 1
    return x_norm
def original(x):
    x = np.array(x)
    x = x.reshape(-1, 1)  # reshape array so that it works with sklearn
    return x
def update(X,y, c):
    indices = []
    for i in range(1,len(y)):
        prev_thr, curr_thr = y[i-1][0], y[i][0]
        if (prev_thr == 0 and  curr_thr > 0) or (curr_thr > 0 and prev_thr == 0):
            a = np.arange(i - c, i + c).tolist()
            indices.extend(a)
    print(indices)
    X = np.delete(X, indices, axis=0)
    y = np.delete(y, indices, axis=0)
    return X, y



