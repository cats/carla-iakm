import os
import re
import sys
import json
import openai
import traceback
import pymongo
import datetime
from gridfs import GridFS
from bson import ObjectId
import requests
import torch
from tools import utility
from dataclasses import dataclass
from dotenv import load_dotenv, find_dotenv
openai.api_key = "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"
HOST_GRAPH = "localhost"  # os.environ['HOST_GRAPH']
load_dotenv()
PORT_GRAPH = os.environ['PORT_GRAPH']
ID_GRAPH   = os.environ['ID_GRAPH']
URL_GRAPH  = f"http://{HOST_GRAPH}:{PORT_GRAPH}"
GRAPH_INDEX_NAME = os.environ['GRAPH_INDEX_NAME']
GRAPH_SEARCH_TYPE = os.environ['GRAPH_SEARCH_TYPE']
BERT_SIMILARITY_MODEL = "stsb-roberta-base"

#region "singleton MongoDB instance"
class Mongo:
    class __Mongo:
        def __init__(self):
            self.client = None
    instance = None
    def __new__(cls, host,port):
        if not Mongo.instance:
            Mongo.instance = Mongo.__Mongo()
        Mongo.instance.client = pymongo.MongoClient(f'mongodb://{host}:{port}/')
        return Mongo.instance 
#endregion 

@dataclass
class ModelResponse:
    instance_map: str
    instance_model: str
    model_db_id: str
    model_db_host: str
    model_db_port: int
    samples: int
    model: bytes

#endregion

#region "repository settings"
def graph_restart():
    URL_RESTART = f"http://{HOST_GRAPH}:{PORT_GRAPH}/rest/repositories/{ID_GRAPH}/restart?location="
    r = requests.post(URL_RESTART)
    print(r.status_code)
    print(r.content.decode())
def index_create():
    URL_SIMILARITY = f"http://{HOST_GRAPH}:{PORT_GRAPH}/rest/similarity"
    print(URL_SIMILARITY, flush=True)
    list_indexes = json.load(open('indexes.json'))
    for index in list_indexes:        
        headers = {"Content-Type": "application/json","Accept": "application/json"}
        data ={
        "name": index["name"],
        "options": index["options"],
        "selectQuery":"\n".join(index["selectQuery"]) ,
        "infer": True,
        "sameAs": True,
        "type": "text",
        "analyzer": "org.apache.lucene.analysis.en.EnglishAnalyzer",
        "searchQuery":"\n".join(index["searchQuery"])
        }
        r = requests.post(URL_SIMILARITY, data=json.dumps(data), headers=headers)
        print(r.status_code)
        print(r.content.decode())      
def index_update():
    URL_SIMILARITY = f"http://{HOST_GRAPH}:{PORT_GRAPH}/rest/similarity"
    list_indexes = json.load(open('indexes.json'))
    for index in list_indexes: 
        headers = {"Content-Type": "application/json","Accept": "application/json"}
        data ={
        "name": index["name"],
        "options": index["options"],
        "selectQuery":"\n".join(index["selectQuery"]),
        "searchQuery":"\n".join(index["searchQuery"]),
        "stopList": None,
        "infer": True,
        "sameAs": True,
        "type": "text",
        "analogicalQuery": None
        }
        r = requests.put(URL_SIMILARITY, data=json.dumps(data), headers=headers)
        print(r.status_code)
        print(r.content.decode())
#endregion

#region "Post model" 
# { "status":200,
#   "message-rdf":"OK",
#   "message-db":"OK",
#   "info" :{"model_db_type":"", "model_db_host":"","model_db_port":"","model_db_id":"" }
# }
def setModel(context, model, db_info, isMerged=False):
    response = {} 
    try: # Save model-AI in MnogoDB
        db_client = Mongo(db_info["model_db_host"],db_info["model_db_port"]).client
        mydb = db_client['data']
        model_db_id =  GridFS(mydb).put(model)
        db_info["model_db_id"] = str(model_db_id)
        response["status"] = 200
        response["message-db"] = f"OK, model-AI saved in {db_info['model_db_type']}"
    except Exception:
        print(sys.exc_info(), flush=True)
        response["status"] = 404
        response["message-db"] = f"Error! saving Failure in {db_info['model_db_type']}"
        response["message-rdf"] = "No message !!"
        return response
    if not isMerged:
        try: # Save model-info in GraphDB
            res_saveRDF = _saveGraphInfo(context, db_info)
            if res_saveRDF == "OK":
                response["status"] = 200
                response["message-rdf"] = "OK, model-Info saved in GraphDB!"
                response["info"] = db_info
            else:
                response["status"] = 404
                response["message-rdf"] = "Error, cannot save data in Graph!"
        except Exception:
            print(sys.exc_info(), flush=True)
            response["status"] = 404
            response["message-rdf"] = "Error, Graph saving Failure!"
        return response
    else:
        try: # Save merging info in GraphDB
            res_saveRDF = _saveGraphMergedInfo(context, db_info)
            if res_saveRDF == "OK":
                response["status"] = 200
                response["message-rdf"] = "OK, model-Info saved in GraphDB!"
                response["info"] = db_info
            else:
                response["status"] = 404
                response["message-rdf"] = "Error, cannot save data in Graph!"
        except Exception:
            print(sys.exc_info(), flush=True)
            response["status"] = 404
            response["message-rdf"] = "Error, Graph saving Failure!"
        
        return response

def _saveGraphInfo(context, db_info):
    try:
        urlQuery =f"{URL_GRAPH}/repositories/{ID_GRAPH}/statements" 
        query = """PREFIX : <http://www.example.org/IAKM.owl#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                INSERT DATA{ %s   %s }"""
        
        print(json.dumps(context, indent=1))
        if "entity" in context:
            uid = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
            instance_class = context['entity']
            del context["entity"]
            if "name" in context:
                ctx_instance_name  =  context['name'] 
                del context["name"]
            else: 
                ctx_instance_name = f"{instance_class}_{str(uid)}" 
            ##########################################################################
            def format_item(item):
                k,v = item[0], item[1]
                if getType(v) == str: return f":{k} '{v}';"
                else:                 return f":{k}  {v};"
           
            trplts_map = (f":{ctx_instance_name} a owl:NamedIndividual . "
                          f":{ctx_instance_name} a :{instance_class}; ")
            l = list(map(format_item, context.items()))
            l[-1]=l[-1].replace(";", " . ")
            trplts_map = trplts_map + " ".join(l)
            ##########################################################################
            def format_model(ctx):
                return f":{ctx[0]} '{ctx[1]}'; "
            
            ctx_model = f"model_{ctx_instance_name}"
            trplts_model = (f":{ctx_model}         a              owl:NamedIndividual . "
                            f":{ctx_model}         a              :Models . "
                            f":{ctx_instance_name} :hasModel      :{ctx_model} . "
                            f":{ctx_model}         :hasMapContext :{ctx_instance_name} ; ")             
            l = list(map(format_model, db_info.items()))
            l[-1]=l[-1].replace(";", " .")
            trplts_model = trplts_model + " ".join(l)
   
            query = query % (trplts_map, trplts_model)
            params = { "update":query, "infer":True,"sameAs":True }
            print(urlQuery, flush=True)
            print(params, flush=True)
            headers = {"Accept": "application/sparql-results+json"}
            res = requests.post(urlQuery, params=params, headers=headers)
            print(res.content.decode(), flush=True)
            if res.status_code == 204:
                return "OK"
            else:
                return res.content.decode()
        else:
            return "Error, Context must has [Entity] and [Name] attributes"
    except:
        print(traceback.format_exc(), flush=True)
        return f"Error, {sys.exc_info()}"

def _saveGraphMergedInfo(context, db_info):
    uid = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    try:
        urlQuery =f"{URL_GRAPH}/repositories/{ID_GRAPH}/statements" 
        query = """PREFIX : <http://www.example.org/IAKM.owl#> 
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                INSERT DATA{ %s }"""
            
        def format_model(ctx):
            return f":{ctx[0]} '{ctx[1]}'; "

        trplts_model = f":model_optimized_{uid} a owl:NamedIndividual ;  a :Models ; :hasOrigin <{db_info['IRI_model']}>; "
        del db_info['IRI_model']
        l = list(map(format_model, db_info.items()))
        l[-1]=l[-1].replace(";", " .")
        trplts_model = trplts_model + " ".join(l)

        query = query % ( trplts_model)
        params = {
            "update":query,
            "infer":True,
            "sameAs":True
        }
        print(urlQuery, flush=True)
        print(params, flush=True)
        headers = {"Accept": "application/sparql-results+json"}
        res = requests.post(urlQuery, params=params, headers=headers)
        print(res.content.decode(), flush=True)
        if res.status_code == 204:
            return "OK"
        else:
            return res.content.decode()
    except:
        print(sys.exc_info(), flush=True)
        return f"Error, {sys.exc_info()}"
#endregion

#region "Get model" 
# {
#     "model_db_type": "MongoDB",
#     "model_db_host": "192.168.10.130",
#     "model_db_port": "27017",
#     "model_db_id"  : "642584cf3a8baae7d2290937",
#     "IRI_model"    : "http://www.example.org/IAKM.owl#ai_saint_claud"
# }
def getModel_similarity(context):
    modelInfo, model = None, None
    def _similarity_search(context: dict):
        list = []
        URL_SEARCH_SIMILARITY = f"http://{HOST_GRAPH}:{PORT_GRAPH}/repositories/{ID_GRAPH}"
        list_indexes = json.load(open('indexes.json'))
        for index in list_indexes:
            if index["name"] == GRAPH_INDEX_NAME:
                search_term = contextToSearchterm(context, index["indexedWithKey"])
                print(f"Search Term: {search_term}", flush=True)
                params = {
                    "query": "\n".join(index["searchQuery"]),
                    "$index": f"<http://www.ontotext.com/graphdb/similarity/instance/{index['name']}>",
                    "$query": f"\"{search_term}\"",
                    "$searchType": "<http://www.ontotext.com/graphdb/similarity/searchTerm>",
                    "$resultType": "<http://www.ontotext.com/graphdb/similarity/documentResult>",
                    "$parameters": f"\"-searchtype {GRAPH_SEARCH_TYPE}\"",
                }
                headers = {"Accept": "application/sparql-results+json"}
                res = requests.get(URL_SEARCH_SIMILARITY, params=params, headers=headers)
                result = json.loads(res.content.decode())
                for r in result["results"]["bindings"]:
                    if "model" in r:
                        item = {}
                        item["documentID"] = r["documentID"]["value"]
                        item["model"] = r["model"]["value"]
                        item["proximity"] = r["score"]["value"]
                        list.append(item)
                break
        print(json.dumps(list, indent=1))
        return list
    list_models_scores = _similarity_search(context)

    if len(list_models_scores) > 0:
        IRI_model = list_models_scores[0]["model"] # take the first IRI of model
        def _getModelInfo(IRI_model):
            endpoint = f"repositories/{ID_GRAPH}"
            params = {
                "query": """ PREFIX : <http://www.example.org/IAKM.owl#> 
                         SELECT (strafter(str(?prop),"#") as ?prop_key) ?prop_val WHERE {
                                <""" + IRI_model + """> ?prop ?prop_val .
                                FILTER(IsLiteral(?prop_val))
                         }"""
            }
            headers = {"Accept": "application/sparql-results+json"}
            res = requests.get(f"{URL_GRAPH}/{endpoint}", params=params, headers=headers)
            result = json.loads(res.content.decode())
            model_props = {}
            for r in result["results"]["bindings"]:
                model_props[r["prop_key"]["value"]] = r["prop_val"]["value"]
            return model_props
        modelInfo = _getModelInfo(IRI_model)
        modelInfo["IRI_model"] = IRI_model
        if "model_db_type" in modelInfo:
            if modelInfo["model_db_type"] == "MongoDB":
                db_host     = modelInfo["model_db_host"] if "model_db_host" in modelInfo else None
                db_port     = modelInfo["model_db_port"] if "model_db_port" in modelInfo else None
                db_model_id = modelInfo["model_db_id"] if "model_db_id" in modelInfo else None
                try:
                    db_client = Mongo(db_host,db_port).client
                    model = GridFS(db_client['data']).get(ObjectId(db_model_id)).read() 
                    print(f"Size of model : {sys.getsizeof(model)} bytes", flush=True)
                except Exception: pass
    return modelInfo, model

def getModel_lucene(search_term):
    model = None
    def _lucene_search_modelInfo(search_text: str):
        URL_SEARCH_SIMILARITY = f"http://{HOST_GRAPH}:{PORT_GRAPH}/repositories/{ID_GRAPH}"
        sparql_query = """
            PREFIX luc:       <http://www.ontotext.com/connectors/lucene#>
            PREFIX luc-index: <http://www.ontotext.com/connectors/lucene/instance#>
            PREFIX rdf:       <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs:      <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX iakm:      <http://www.example.org/IAKM.owl#>
            SELECT (strafter(str(?prop),"#") as ?k) ?v ?score 
            WHERE {
                    ?model ?prop ?v .
                    FILTER(IsLiteral(?v))
                    { 
                        SELECT ?entity ?model ?score
                        {
                        ?search a luc-index:starwars_fts2;
                            luc:query "%s" ;
                            luc:entities ?entity .
                        ?entity iakm:hasModel ?model .
                        ?entity luc:score     ?score .

                        } ORDER BY DESC(?score) LIMIT 1 
                    }   
                }
            """ % (search_text)

        params = {"query": sparql_query, "infer": True, "sameAs": True, "limit": 1001, "offset": 0}
        headers = {"Accept": "application/sparql-results+json"}
        res = requests.post(URL_SEARCH_SIMILARITY, data=params, headers=headers)
        result = json.loads(res.content.decode())
        model_props = {}
        for r in result["results"]["bindings"]:
            model_props["score"] = r["score"]["value"]
            model_props[r["k"]["value"]] = r["v"]["value"]

        return model_props

    dict_model_props = _lucene_search_modelInfo(search_term)
    if "model_db_type" in dict_model_props:
        if dict_model_props["model_db_type"] == "MongoDB":
            db_host     = dict_model_props["model_db_host"] if "model_db_host" in dict_model_props else None
            db_port     = dict_model_props["model_db_port"] if "model_db_port" in dict_model_props else None
            db_model_id = dict_model_props["model_db_id"]   if "model_db_id"   in dict_model_props else None
            try:
                mongo_client = Mongo(db_host,db_port).client
                print("Connected to DB !", flush=True)
                list = GridFS(mongo_client['data']).find()
                print(list)
                model = GridFS(mongo_client['data']).find_one({'_id':ObjectId(db_model_id)}).read()
                print(f"Type of model : {type(model)}", flush=True)
                if model is None:
                    print(".......... Model NOT found in DB ...........")
                else:
                    print(f"Size of model : {sys.getsizeof(model)} bytes", flush=True)
            except Exception:
                print(traceback.format_exc(), flush=True)
    return dict_model_props, model


  
def _lucene_search(search_text: str):
    list = []
    URL_SEARCH_SIMILARITY = f"http://{HOST_GRAPH}:{PORT_GRAPH}/repositories/{ID_GRAPH}" 
    sparql_query ='''
        PREFIX luc:       <http://www.ontotext.com/connectors/lucene#>
        PREFIX luc-index: <http://www.ontotext.com/connectors/lucene/instance#>
        PREFIX rdf:       <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs:      <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX iakm:      <http://www.example.org/IAKM.owl#>
        SELECT  ?entity ?model ?score{
                ?search a            luc-index:starwars_fts2 ;
                        luc:query    "%s" ;
                        luc:entities ?entity .
                ?entity iakm:hasModel ?model .
                ?entity luc:score    ?score .
                }
        ''' % (search_text)
    params  = { "query" : sparql_query, "infer":True, "sameAs":True, "limit": 1001, "offset": 0}
    headers = { "Accept": "application/sparql-results+json"}

    res = requests.post(URL_SEARCH_SIMILARITY, data=params, headers=headers)
    result = json.loads(res.content.decode())

    for r in result["results"]["bindings"]:
        item = {}
        if "model" in r:
            item["entity"] = r["entity"]["value"]
            item["model"]  = r["model"]["value"]
            item["score"]  = r["score"]["value"]
            list.append(item)
    print(json.dumps(list, indent=1))   
    return list 


#endregion

def save_model_mongo(model, db_info):
    response = {}
    try:  # Save model-AI in MongoDB
        db_client = Mongo(db_info["model_db_host"], db_info["model_db_port"]).client
        mydb = db_client['data']
        model_db_id = GridFS(mydb).put(model)
        db_info["model_db_id"] = str(model_db_id)
        print(db_info)
    except:
        traceback.print_exc()
    return db_info

def load_model_mongo(model_id, db_info):
    model = None
    try:  # Save model-AI in MongoDB
        db_client = Mongo(db_info["model_db_host"], db_info["model_db_port"]).client
        model = GridFS(db_client['data']).find_one({'_id': ObjectId(model_id)}).read()
    except:
        traceback.print_exc()
    return model


#region "Entrypoints"
def setGraphModel(map_context, model, db_info):
    #region "Save model data in MongoDB"
    response = {}
    try:  # Save model-AI in MongoDB
        db_client = Mongo(db_info["model_db_host"], db_info["model_db_port"]).client
        mydb = db_client['data']
        model_db_id = GridFS(mydb).put(model)
        db_info["model_db_id"] = str(model_db_id)
        response["status"] = 200
        response["message-db"] = f"OK, model-AI saved in {db_info['model_db_type']}"
        print(response)
        print(db_info)
    except Exception:
        print(sys.exc_info(), flush=True)
        response["status"] = 404
        response["message-db"] = f"Error! saving Failure in {db_info['model_db_type']}"
        response["message-rdf"] = "No message !!"
        return response
    #endregion

    #region "Context Processing"
    if not utility.validateJson(map_context, "set"):
        print("ERR: JSON object does not match the Context Schema !")
        return
    map_context = utility.formulateContext(map_context)
    entity = map_context["entity"]
    query_entity_value = map_context['entity']
    leaves = getGraph_RoadPartLeaves()  # ['Lane', 'Intersection', 'Roundabout', 'Turn']
    # Find Entity similarity ...
    filtered_leaves = []
    if not entity in leaves:
        res_dbpedia = get_dbpedia_similarity(query_entity_value)
        for item_dbpedia in res_dbpedia:
            for leaf in leaves:
                if leaf.lower() in item_dbpedia["label"].lower():
                    filtered_leaves.append((leaf, item_dbpedia["score"]))

        if len(filtered_leaves) == 1:
            print("1 exact result in DBpedia")
            print(filtered_leaves)
            map_context["entity"] = filtered_leaves[0][0]
            print(f"Similarity by DBpedia: f{filtered_leaves}")
        else:
            if len(filtered_leaves) == 0:
                print("0 result in DBpedia")
                pass
            else:
                print("2 or more results in DBpedia")
                leaves = [x[0] for x in filtered_leaves]

            res_BERT = getBERT_similarity(BERT_SIMILARITY_MODEL, query_entity_value, leaves)
            road_element = max(res_BERT, key=lambda ev: ev['score'])
            print(f"Similarity by Google BERT: {res_BERT}")
            map_context["entity"] = road_element["label"]
    else: print("Exact Match !")
    print(json.dumps(map_context, indent=1))
    #endregion

    #region "SPARQL"
    try:
        urlQuery = f"{URL_GRAPH}/repositories/{ID_GRAPH}/statements"
        query = """ PREFIX : <http://www.example.org/IAKM.owl#>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX owl: <http://www.w3.org/2002/07/owl#>
                    INSERT DATA{ %s %s %s }"""

        if "entity" in map_context:
            road_part = map_context['entity']
            uid = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
            individualName = f"{entity.replace(' ', '_')}_{str(uid)}"
            trplts_map = (f":{individualName} a owl:NamedIndividual;   a :{road_part};  rdfs:comment '{entity}'; ")
            trplts_attributes = convertAttributesToTriplets(map_context["attributes"])
            trplts_map = trplts_map + " ".join(trplts_attributes)
            axioms = getAxioms(individualName, map_context["attributes"])

            def format_model(ctx):
                return f":{ctx[0]} '{ctx[1]}'; "
            ctx_model = f"model_{individualName}"
            trplts_model = (f":{ctx_model}         a              owl:NamedIndividual . "
                            f":{ctx_model}         a              :Models . "
                            f":{individualName} :hasModel      :{ctx_model} . "
                            f":{ctx_model}         :hasMapContext :{individualName} ; ")
            l = list(map(format_model, db_info.items()))
            l[-1] = l[-1].replace(";", " .")
            trplts_model = trplts_model + " ".join(l)

            query = query % (trplts_map, axioms, trplts_model)
            params = {
                "update": query,
                "infer": True,
                "sameAs": True
            }
            print(urlQuery, flush=True)
            print(params, flush=True)
            headers = {"Accept": "application/sparql-results+json"}
            res = requests.post(urlQuery, params=params, headers=headers)
            print(res.content.decode(), flush=True)
            if res.status_code == 204:
                return "OK"
            else:
                return res.content.decode()
        else:
            return "Error, Context must has [Entity] and [Name] attributes"
    except:
        print(traceback.format_exc(), flush=True)
    return f"Error, {sys.exc_info()}"

def getGraphModel_simple(map_context):
    model = None
    #region "Context Processing"
    if not utility. validateJson(map_context, "get"):
        print("ERR: Not valid json object for CONTEXT")
        return
    map_context = utility.formulateContext(map_context)
    entity = map_context["entity"]
    query_entity_value = map_context['entity']
    query_mapContext_properties = map_context['attributes']
    leaves = getGraph_RoadPartLeaves()  # ['Lane', 'intersection', 'roundabout', 'turn']
    if len(leaves) == 0: return  None
    # Find Entity similarity ...
    filtered_leaves = []
    if not entity in leaves:
        res_dbpedia = get_dbpedia_similarity(query_entity_value)
        for item_dbpedia in res_dbpedia:
            for leaf in leaves:
                if leaf.lower() in item_dbpedia["label"].lower():
                    same_entity = [x for x in filtered_leaves if leaf == x]
                    if len(same_entity) == 0:
                        filtered_leaves.append(leaf)

        print(filtered_leaves)
        if len(filtered_leaves) == 1:
            print("1 exact response by DBpedia")
            map_context["entity"] = filtered_leaves[0]
            print(f"Similarity by DBpedia: f{filtered_leaves}")
        else:

            if len(filtered_leaves) == 0:
                print("0 response by DBpedia")
                pass
            else:
                print("2 or more results by DBpedia")
                leaves = filtered_leaves

            prompt = f"in one word, which one is the closest to '{query_entity_value}': {' or '.join(leaves)}"
            print(prompt)
            completion = openai.ChatCompletion.create(model="gpt-3.5-turbo",
                                                      messages=[{"role": "user", "content": prompt}])
            gpt_response = (completion['choices'][0]['message']['content']).replace('.', '').capitalize()
            print(f"Similarity response by GPT: {gpt_response}")
            map_context["entity"] = gpt_response
    else: print("Exact Match !")
    print(json.dumps(map_context, indent=1))
    #endregion

    print(json.dumps(map_context,indent=1))

    #region "SPARQL Request"
    index = 0
    response_graph, query_attributes = [], []
    for attribute in map_context["attributes"]:
        key = attribute["name"]
        value = ('"' + str(attribute["value"]) + '"') if type(attribute["value"]) == str else str(attribute["value"])
        query_attributes.append(f"{key}:{value}")
    query_lucene = " ".join(query_attributes)
    try:
        query = \
            """ PREFIX luc:       <http://www.ontotext.com/connectors/lucene#>
                PREFIX luc-index: <http://www.ontotext.com/connectors/lucene/instance#>
                PREFIX rdf:       <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs:      <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX iakm:     <http://www.example.org/IAKM.owl#>
                PREFIX extfn:     <http://www.ontotext.com/js#>
                PREFIX owl:       <http://www.w3.org/2002/07/owl#>
                SELECT ?entity ?score ?model ?model_db_id 
                FROM <http://www.ontotext.com/explicit> WHERE{ 
                    ?search a luc-index:starwars_fts2; 
                              luc:query ?query; 
                              luc:entities ?entity .
                    ?entity luc:score ?score . 
                    ?entity iakm:hasModel ?model .
                    ?model iakm:model_db_type ?model_db_type .
                    ?model iakm:model_db_host ?model_db_host .
                    ?model iakm:model_db_port ?model_db_port .
                    ?model iakm:model_db_id ?model_db_id .
                    {
                        SELECT ?entity ?query 
                        WHERE{
                                 { ?entity rdf:type ?x . iakm:%s rdfs:subClassOf+ ?x .  } 
                            union{ ?entity rdf:type iakm:%s.}
                            BIND('%s' as ?query)
                            }}
                    }
                   ORDER BY DESC(?score) LIMIT 10 """ % (map_context["entity"],map_context["entity"], query_lucene)

        endpoint = f"repositories/{ID_GRAPH}"
        print(query)
        params = {"query": query}
        headers = {"Accept": "application/sparql-results+json"}
        res = requests.get(f"{URL_GRAPH}/{endpoint}", params=params, headers=headers)
        if res.status_code == 200:
            content = json.loads(res.content.decode())
            list_data = content["results"]["bindings"]
            for i in range(len(list_data)):
                entity_name  = list_data[i]["entity"]["value"]
                entity_model = list_data[i]["model"]["value"]
                entity_model_db_id = list_data[i]["model_db_id"]["value"]
                entity_score = list_data[i]["score"]["value"]
                response_graph.append({
                    "name": entity_name,
                    "model": entity_model,
                    "model_db_id": entity_model_db_id,
                    "score": entity_score})
    except:
        print(traceback.format_exc(), flush=True)
        return f"Error, {sys.exc_info()}"
    #endregion


    #region "Model Data Request"
    if len(response_graph) > 0:
        mongo_model_id = response_graph[0]["model_db_id"]
        try:
            mongo_client = Mongo("127.0.0.1", 27017).client
            #list = GridFS(mongo_client['data']).find()
            model = GridFS(mongo_client['data']).find_one({'_id': ObjectId(mongo_model_id)}).read()
            if model is None:
                print(".......... Model NOT found in MongoDB ...........")
            else:
                print(f"Type of model : {type(model)}", flush=True)
                print(f"Size of model : {sys.getsizeof(model)} bytes", flush=True)
        except Exception:
            print(traceback.format_exc(), flush=True)
    else: print(f"No model-info in Graph", flush=True)
    #endregion

    return model
def getGraphModel_complex(map_context):
    model = None

    # region "Context Processing"
    if not utility.validateJson(map_context, "get"):
        print("ERR: Not valid json object for CONTEXT")
        return
    map_context = utility.formulateContext(map_context)
    entity = map_context["entity"]
    query_entity_value = map_context['entity']
    query_mapContext_properties = map_context['attributes']
    leaves = getGraph_RoadPartLeaves()  # ['Lane', 'intersection', 'roundabout', 'turn']
    # Find Entity similarity ...
    filtered_leaves = []
    if not entity in leaves:
        res_dbpedia = get_dbpedia_similarity(query_entity_value)
        for item_dbpedia in res_dbpedia:
            for leaf in leaves:
                if leaf.lower() in item_dbpedia["label"].lower():
                    same_entity = [x for x in filtered_leaves if leaf == x]
                    if len(same_entity) == 0:
                        filtered_leaves.append(leaf)

        print(filtered_leaves)
        if len(filtered_leaves) == 1:
            print("1 exact response by DBpedia")
            map_context["entity"] = filtered_leaves[0]
            print(f"Similarity by DBpedia: f{filtered_leaves}")
        else:

            if len(filtered_leaves) == 0:
                print("0 response by DBpedia")
                pass
            else:
                print("2 or more results by DBpedia")
                leaves = filtered_leaves

            prompt = f"in one word, which one is the closest to '{query_entity_value}': {' or '.join(leaves)}"
            print(prompt)
            completion = openai.ChatCompletion.create(model="gpt-3.5-turbo",
                                                      messages=[{"role": "user", "content": prompt}])
            gpt_response = (completion['choices'][0]['message']['content']).replace('.', '').capitalize()
            print(f"Similarity response by GPT: {gpt_response}")
            map_context["entity"] = gpt_response
    else:
        print("Exact Match !")
    print(json.dumps(map_context, indent=1))
    # endregion

    # region "SPARQL"
    index = 0
    response_graph, query_attributes = [], []
    for attribute in map_context["attributes"]:
        index += 1
        query_attribute = """
        BIND("{key}" AS ?att_key_{idx})
        BIND( {value} AS ?att_val_{idx})
            OPTIONAL{{
                ?entity ?p{idx} ?value{idx} .
                FILTER(isLiteral(?value{idx}))
                BIND(STRAFTER(STR(?p{idx}),"#") as ?key{idx} )
                BIND(extfn:matchKey(?key{idx}, ?att_key_{idx}) as ?match{idx} )
                FILTER(?match{idx})
                OPTIONAL{{ ?axiom{idx}_approx owl:annotatedSource ?entity;owl:annotatedProperty ?p{idx};owl:annotatedTarget ?value{idx}; iakm:approx ?approx{idx} . }}
                OPTIONAL{{ ?axiom{idx}_weight owl:annotatedSource ?entity;owl:annotatedProperty ?p{idx};owl:annotatedTarget ?value{idx}; iakm:weight ?weight{idx} . }}
                BIND(IF(?match{idx} && !BOUND(?approx{idx}), CONCAT(?query," ",?key{idx},":",STR(?att_val_{idx}),IF(BOUND(?weight{idx}),CONCAT("^",STR(?weight{idx})),"")), ?query) AS ?query) 
            }}
        FILTER(IF( BOUND(?approx{idx}), ABS(?value{idx}-?att_val_{idx})<=?approx{idx},  true))
        """
        query_attributes.append(query_attribute.format(key=attribute["name"],
                                                       value=('"' + str(attribute["value"]) + '"') if type(
                                                           attribute["value"]) == str else str(attribute["value"]),
                                                       idx=str(index)))
    query_attribute = """\n""".join(query_attributes)
    try:
        query = \
            """ PREFIX luc:       <http://www.ontotext.com/connectors/lucene#>
                PREFIX luc-index: <http://www.ontotext.com/connectors/lucene/instance#>
                PREFIX rdf:       <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs:      <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX iakm:     <http://www.example.org/IAKM.owl#>
                PREFIX extfn:     <http://www.ontotext.com/js#>
                PREFIX owl:       <http://www.w3.org/2002/07/owl#>
                SELECT ?entity ?score ?query ?model ?model_db_id {  
                    ?search a luc-index:starwars_fts2; luc:query ?query; luc:entities ?entity .
                    ?entity luc:score ?score . 
                    ?entity iakm:hasModel ?model .
                    ?model iakm:model_db_type ?model_db_type .
                    ?model iakm:model_db_host ?model_db_host .
                    ?model iakm:model_db_port ?model_db_port .
                    ?model iakm:model_db_id ?model_db_id .


                    {
                        SELECT ?entity ?query {
                        ?entity rdf:type iakm:%s.   
                    BIND(" " AS ?query)
                    %s
                        }  
                    }
                } ORDER BY DESC(?score) LIMIT 5 """ % (map_context["entity"], query_attribute)

        endpoint = f"repositories/{ID_GRAPH}"
        print(query)
        params = {"query": query}
        headers = {"Accept": "application/sparql-results+json"}
        res = requests.get(f"{URL_GRAPH}/{endpoint}", params=params, headers=headers)
        if res.status_code == 200:
            content = json.loads(res.content.decode())
            list_data = content["results"]["bindings"]
            print("++++++++++++++++++++++++++++++++++++++++")
            print(len(list_data))
            # print(json.dumps(list_data, indent=1))
            print("++++++++++++++++++++++++++++++++++++++++")
            for i in range(len(list_data)):
                entity_name = list_data[i]["entity"]["value"]
                entity_model = list_data[i]["model"]["value"]
                entity_model_db_id = list_data[i]["model_db_id"]["value"]
                entity_score = list_data[i]["score"]["value"]
                response_graph.append({
                    "name": entity_name,
                    "model": entity_model,
                    "model_db_id": entity_model_db_id,
                    "score": entity_score})
    except:
        print(traceback.format_exc(), flush=True)
        return f"Error, {sys.exc_info()}"
    # endregion

    # region "DB"
    if len(response_graph) > 0:
        mongo_model_id = response_graph[0]["model_db_id"]
        try:
            mongo_client = Mongo("127.0.0.1", 27017).client
            # list = GridFS(mongo_client['data']).find()
            model = GridFS(mongo_client['data']).find_one({'_id': ObjectId(mongo_model_id)}).read()
            if model is None:
                print(".......... Model NOT found in MongoDB ...........")
            else:
                print(f"Type of model : {type(model)}", flush=True)
                print(f"Size of model : {sys.getsizeof(model)} bytes", flush=True)
        except Exception:
            print(traceback.format_exc(), flush=True)
    else:
        print(f"No model-info in Graph", flush=True)
    # endregion

    return model
#endregion

#region "Local Functions"
def getGraph_RoadPartLeaves():
    leaves = []
    try:
        endpoint = f"repositories/{ID_GRAPH}"
        #     MINUS {
        #     ?leaf rdfs:subClassOf :RoadPart.
        #     ?z rdfs:subClassOf ?leaf . }
        params = {
            "query": """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                        PREFIX : <http://www.example.org/IAKM.owl#>
                        PREFIX owl: <http://www.w3.org/2002/07/owl#>
                        SELECT (strafter(str(?leaf),"#") as ?node) ?x FROM <http://www.ontotext.com/explicit>
                        WHERE {
                            ?leaf rdfs:subClassOf+ :Road .
                        }"""
        }
        headers = {"Accept": "application/sparql-results+json"}
        res = requests.get(f"{URL_GRAPH}/{endpoint}", params=params, headers=headers)
        result = json.loads(res.content.decode())
        for r in result["results"]["bindings"]:
            leaves.append(r["node"]["value"])
    except:
        print(sys.exc_info())
    print(leaves)
    return leaves

def getBERT_similarity(modelname, query, corpus):
    from sentence_transformers import SentenceTransformer, util
    embedder = SentenceTransformer(modelname)

    corpus_embeddings = embedder.encode(corpus, convert_to_tensor=True)

    # Find the closest 5 sentences of the corpus for each query sentence based on cosine similarity
    top_k = min(5, len(corpus))
    query_embedding = embedder.encode(query, convert_to_tensor=True)

    # We use cosine-similarity and torch.topk to find the highest 5 scores
    cos_scores = util.cos_sim(query_embedding, corpus_embeddings)[0]
    top_results = torch.topk(cos_scores, k=top_k)

    results = []

    for score, idx in zip(top_results[0], top_results[1]):
        results.append(
            {"label": corpus[idx],
             "score": float("{:.2f}".format(score))
             })
    return results

def get_dbpedia_similarity(query):
    request_dbpedia = f"https://lookup.dbpedia.org/api/search?format=JSON&MaxHits=10&QueryString={query}"
    r = requests.get(request_dbpedia)
    result = json.loads(r.content.decode())
    total_score = sum(map(lambda x: float(x['score'][0]), result['docs']))
    res = []
    for element in result["docs"]:
        label, score = element["label"][0], element["score"][0]
        res.append(
            {"label": re.sub('<[^<]+?>', '', label),
             "score": float("{:.2f}".format(float(score) / total_score))
             })
    return res

def convertAttributesToTriplets(attributes: list) -> list:
    def format(att):
        predicate, object = att["name"], att["value"]
        if type(object) == str:
            return f":{predicate} '{object}'; "
        else:
            return f":{predicate}  {object} ; "

    list_triplets = list(map(format, attributes))
    list_triplets[-1] = list_triplets[-1].replace(";", " . ")
    return list_triplets

def getAxioms(individualName, attributes: list) -> str:
    def format(att):
        axiom = ""
        name = att["name"]
        value = att["value"]
        value = f"'{value}'" if type(value) == str else value
        for k_axiom, v_axiom in att.items():
            if k_axiom not in ["name", "value"]:
                axiom = f"""
                [ rdf:type owl:Axiom ;owl:annotatedSource   :{individualName} ;
                                      owl:annotatedProperty :{name};
                                      owl:annotatedTarget    {value} ;
                                         :{k_axiom}          {v_axiom} 
                ] ."""
        return axiom

    return " ".join(list(map(format, attributes)))

# example search to find {round + intersection} : Roundabout nbLane:2 nbExit:4 nbEntry:4 width:31
def contextToSearchterm(context, indexedWithKey:True):
    searchterm =""
    if indexedWithKey:
        list_terms = []
        if "entity" in context:
            list_terms.append(context["entity"])
            del context["entity"]
            for key,value in context.items():
                #list_terms.append(f"{key}:{value}")
                list_terms.append(f"{value}")
            searchterm = " AND ".join(list_terms)# Roundabout nbLane:2 nbExit:4 nbEntry:4 width:38
    else:
        searchterm = " AND ".join(str(e) for e in list(context.values()))  # Roundabout 2 4 4 38
    return searchterm

def getType(value):
    if value.isdigit():
        return int
    elif value.replace('.', '', 1).isdigit() and value.count('.') < 2:
        return float
    else:
        return str

#endregion




def test_getModel():
    try:
        mongo_client = Mongo("127.0.0.1", 27017).client
        print("Connected to DB !", flush=True)
        list = GridFS(mongo_client['data']).find()
        print(list)
        model = GridFS(mongo_client['data']).find_one({'_id': ObjectId("651fd9805c0d9004202c70c9")}).read()
        print(f"Type of model : {type(model)}", flush=True)
        if model is None:
            print(".......... Model NOT found in DB ...........")
        else:
            print(f"Size of model : {sys.getsizeof(model)} bytes", flush=True)
    except Exception:
        print(traceback.format_exc(), flush=True)


