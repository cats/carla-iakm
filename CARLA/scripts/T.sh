#!/usr/bin/env bash
max=10000
for (( i=1; i <= $max; ++i ))
do
    python3.7 ego_T.py
    sleep 1
done