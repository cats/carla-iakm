import sys
import traceback
import json, requests, pickle, joblib
import time, argparse
import pandas as pd
import numpy as np
from io import BytesIO
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression  # You can choose other regression models as well
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing as pre
from sklearn.metrics import mean_squared_error,mean_absolute_error, r2_score
from dataclasses import dataclass
from db import dbGateway
from tools import *

IAKM_HOST, IAKM_PORT="127.0.0.1", 8004
EDGE_ID="eurecom-5gmec"
AGENT_ID="oulu-agent"
AGENT_TYPE="vehicle"
TRAINABILITY="yes"
FILE_MODEL_SIEMENS = "./workpieceStorage_marker_model_small.hdf5"
FILE_MODEL_T_JUNCTION = "./model_T_RFC_50.sav"

class IAKMRequest(object):
    #region "class variables"
    dataSource: object
    URL: str
    usage:str
    method:str
    context:json
    model_response:object
    instance_map:str
    instance_model:str
    model_db_id:str
    model_db_host:str
    model_db_port:int
    samples:int
    model: bytes = None
    response: str
    #endregion
    def __init__(self, context, dataSource=None):
        self.URL = f"http://{IAKM_HOST}:{IAKM_PORT}"
        self.response = None
        self.model_response =None
        self.instance_map = None
        self.instance_model =None
        self.model_db_id =None
        self.model_db_host =None
        self.model_db_port = None
        self.samples = None
        self.dataSource = dataSource
        self.context = {
            "method": "",
            "model_usage": "",
            "agent_type": AGENT_TYPE,
            "trainability": TRAINABILITY,
            "agent_id": AGENT_ID,
            "edge_id": EDGE_ID,
            "context": context
        }
        self.initialize()

    def initialize(self):
        if self.dataSource is not None:
            if isinstance(self.dataSource,str):
                self.model = readModel_fromFile(self.dataSource)
            else:
                bytes_container = BytesIO()
                joblib.dump(self.dataSource, bytes_container)
                bytes_container.seek(0)
                self.model = bytes_container.read()

    def dictToTopic(self,dict, t=""):
        keys = list(dict.keys())
        values = list(dict.values())
        for i in range(len(keys)):
            if type(values[i]) == str:
                if t == "":
                    t = keys[i] + "=" + str(values[i])
                else:
                    t = t + "/" + keys[i] + "=" + str(values[i])
            else:
                t = t + "/" + keys[i]
                t = self.dictToTopic(values[i], t)
        return t.replace('/context/', '/')

    def get_model(self):
        self.context["method"] = "sub-model"
        self.context["model_usage"] = "use"
        params = {"topic": self.dictToTopic(self.context)}

        try:
            response = requests.get(f"{self.URL}/model", params=params)
            self.response = {"code": response.status_code, "content": ""}
            if response.status_code == 200 and not isinstance(response.content, str):
                print("-> Reading AI model from IAKM !")
                self.model_response = pickle.loads(response.content)
                # ModelResponse(instance_map='http://www.example.org/IAKM.owl#Junction_20231018105313964169',
                #               instance_model='http://www.example.org/IAKM.owl#model_Junction_20231018105313964169',
                #               model_db_id='652fb956a24b2c53f60aff5b',
                #               model_db_host='192.168.10.134',
                #               model_db_port=27017, model=b'')
                self.instance_map  = self.model_response.instance_map.replace('http://www.example.org/IAKM.owl#','')
                self.instance_model= self.model_response.instance_model.replace('http://www.example.org/IAKM.owl#','')
                self.model_db_id   = self.model_response.model_db_id
                self.model_db_host = self.model_response.model_db_host
                self.model_db_port = self.model_response.model_db_port
                self.samples       = self.model_response.samples
                model              = self.model_response.model
                #print(f"(GET) Samples:{self.samples}")
                self.model = joblib.load(BytesIO(model))
                #self.model = joblib.load(BytesIO(self.model))
                self.response["content"] = "Model OK"
            else:
                print("-> No model in IAKM !")
                self.model = RandomForestClassifier(random_state=42)
                print("-> New <RandomForestModel> was created ")
                self.response["content"] = response.content
        except requests.exceptions.Timeout:
            self.response = {"code": "", "content": ""}
            print("Request time-out: No model in IAKM !")
            self.model = RandomForestClassifier(random_state=42)
            print("-> New <RandomForestModel> was created ")
        return self.model

    def push_model(self):
        self.context["method"] = "pub-model"
        self.context["model_usage"] = "push"
        params = {"topic": self.dictToTopic(self.context)}
        try:
            bytes_container = BytesIO()
            joblib.dump(self.model, bytes_container)
            bytes_container.seek(0)
            model_db = bytes_container.read()

            #print(f"(PUSH)  Samples:{self.samples}")
            self.model_response = (
                dbGateway.ModelResponse(self.instance_map,
                                        self.instance_model,
                                        self.model_db_id,
                                        self.model_db_host,
                                        self.model_db_port,
                                        self.samples,
                                        model_db))
            pickled_data = pickle.dumps(self.model_response)
            req = requests.post(f"{self.URL}/model", data=pickled_data, params=params)
            self.response = {"code": req.status_code, "content": req.content.decode()}
            print(self.response)
        except:
            traceback.print_exc()

def readModel_fromFile(file_model) -> bytes:
    try:
        model = pickle.load(open(file_model, 'rb'))
        pickled = pickle.dumps(model)
        print(f"✓✓✓ Model Pickled; Size : {sys.getsizeof(pickled)} bytes")
        return pickled
    except:
        traceback.print_exc()
        return None
def readModel(model) -> bytes:
    try:
        pickled = pickle.dumps(model)
        print(f"✓✓✓ Model Pickled; Size : {sys.getsizeof(pickled)} bytes")
        return pickled
    except:
        traceback.print_exc()
        return None


context = {
            "context_map": {
                    "entity": "roundabout",
            },
            "context_model": {
                "ml_type": "decision",
                "input": "3",
                "feature": "1"
            }
        }

print(json.dumps(context, indent=4))
# def dictToTopic(dict, t=""):
#         keys = list(dict.keys())
#         values = list(dict.values())
#         for i in range(len(keys)):
#             if type(values[i]) == str:
#                 if t == "":
#                     t = keys[i] + "=" + str(values[i])
#                 else:
#                     t = t + "/" + keys[i] + "=" + str(values[i])
#             else:
#                 t = t + "/" + keys[i]
#                 t = dictToTopic(values[i], t)
#         return t.replace('/context/', '/')
#
# print(dictToTopic(context))
