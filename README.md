# IAKM-CARLA


IAKM is an AIaaS platform, AIaaS (AI as a Service) with CARLA driving simulator typically involves integrating AI services into the CARLA simulation environment for various purposes such as autonomous driving algorithm testing, training machine learning models, or experimenting with AI-based control systems. In our usecase, we are emphisizing on the AI models exchange between AI actors (Producer/Consumer).


## Intro

IAKM-CARLA architecture consists of 3 mmain components:

- <strong>IAKM-server:</strong> represents the core microservices for knowledge management, data-access management and data storage. 
- <strong>IAKM-agent: </strong> acts as intermediaries that decouple the logic of an autonomous entity from the specific details of the actors it controls. 
- <strong>IAKM-actor: </strong> represents the client of IAKM agent, in other term we call it "AI actors", the entity that makes in use the AI models, it could play as AI producer, consumer or trainer.


<div align="center">
![CARLA Producer/Consumer](images/IAKM-CARLA.mp4)
</div>


## Contents

*   [Deployement requirements](#deployement-requirements)
*   [Getting started](#getting-started)
    *   [IAKM Server](#iakm-server)
    *   [IAKM Agent](#iakm-agent)
    *   [CARLA Simulation](#carla-simulation)
*   [Used Technologies](#used-technologies)
*   [Contribute](#contribute)
*   [License](#license)

## Deployement requirements

*   [x] [Docker](https://docs.docker.com/engine/) Engine/compose
*   [x] [CARLA](https://carla.readthedocs.io/en/0.9.14/), we use CARLA version 0.9.14
*   [x] [Python](https://www.python.org/downloads/release/python-370/)


## Getting Started

Clone the main branch of IAKM-API repository to your local machine:

```
git clone https://gitlab.eurecom.fr/cats/carla-iakm.git
```

The deployment is in threefold: first deploy IAKM server for knowledge management and data storage; second deploy IAKM agent for AI actors requests; Finally run CARLA simlulator for simulating AI actors as producer or consumer.


### 1. IAKM Server

 For server deployement, just run the following shell script:

```
./server-build-run.sh 
```
this script creates 4 docker containers inside docker network:
*   <strong>MQTT</strong> broker, for message queuing between IAKM server and the agents using publish/subscribe messaging pattern.
*   <strong>KM</strong> (Knowledge Management); the entrypoint microservice for the message of IAKM actors for GET/POST/Train of AI models.
*   <strong>MongoDB</strong> NoSQL instance for the storage of AI metadata in form of RDF graph
*   <strong>GraphDB</strong> RDF graph instance for the storage of AI models in documents; the script builds also the IAKM ontology in order to store AI contexts, the image below show the class hierarchy of IAKM ontology

<div align="center" width="150px">
<img src='images/class0hierarchy.png' width='400'>
</div>

### 2. IAKM Agent

 For agent deployement, just run the following shell script:

```
./agent-build-run.sh 
```

### 3. CARLA Simulation

### 3.1 CARLA server
 Download [CARLA](https://carla.readthedocs.io/en/0.9.14/), In order to connect to CARLA server, in our experiment all CARLA scripts are using python v3.7 in Linux environement with egg file "carla-0.9.14-py3.7-linux-x86_64.egg". With aim to includes some interesting road network features, we use the built-in Town03 as CARLA map.To load CARLA server in town03, run this commands:

```
./CarlaUE4.sh 
python3.7 scripts/config.py -m Town03
```

### 3.2 CARLA life traffic 
For testing the interoperability between IAKM and CARLA ego vehicles, we spawn certain number of vehicles in autopilot mode bringing life to the traffic scene in the CARLA simulator. To do, we run 2 scripts to bring life surrounding T-junction (intersection) and R-junction (roundabout)

```
cd CARLA/scripts
python3.7 cars_inbound_T.py 
python3.7 cars_inbound_R.py 
```

### 3.3 CARLA vehicle as Producer(vehicle-A)
```
python3.7 ego_R.py 
```
Assuming Vehicle-A has already a machine learning model of type RandomForest classifier (see drawing below), and approaching to Roundabout, before to enter and while it is in control-box, at every CARLA tick it collects the 4 features as model input and tags them with a control label that represents the real Control values; Throttle & Break. Here, the "Steering" value is out of scoope as it is managed by Bicycle model. As soon as the vehicle-A (AI producer) enters the roundabout it makes a training for the AI model and push it to IAKM server in attaching its context description in the form of RDF graph.

<div align="center" width="150px">
<img src='images/AI-model.png' width='300'>
</div>

### 3.4 CARLA vehicle as Consumer(vehicle-B)
```
python3.7 ego_T.py 
```
 Assuming Vehicle-B, on other point of time, is approaching to a T-junction (yield/not-signaled) without having the intelligence to navigate through such complex situation, it sends a get AI model request to IKAM server in attaching its context description. The Server does tyhe following: receives the get request; uses the context desccription to make similarity search; finds the best scored AI model using SPARQL/Lucene capabilities, anf finally reply by the AI model for control prediction that might help Vehicle-B to cross the junction. 

## Used technologies

- <strong>Flask RESTful:</strong>to build API that represents the interface between IAKM agent and its IAKM actors
- <strong>MongoDB/GridFS:</strong> For data storage of AI models 
- <strong>GraphDB:</strong> For data storage of AI metadata 
- <strong>MQTT:</strong> for pub/sub pattern
- <strong>Docker:</strong> helps for scalibility, portability and networking
  

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
This work is released under Apache 2.0 license. See the [license file](LICENSE.txt) for more details. Contribution and integrations are appreciated.


## References
Our citation details will be finalized upon paper acceptance ...

[def]: #carla-simulation