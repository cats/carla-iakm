// Declare variables for mongodb operations
var express = require('express');
var https = require('https');

//mongo database
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://mongo:27017';

// Geoserver routes
var app = express();
var appRouter = function(app) {

// GET models - example: agent/ego1/model1
app.get("/agent/:vehicleId/:modelname", function(req, res) {
    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
	    var all_models_req = {}; // empty JSON object
	    all_models_req["models"] = []; // empty array, which you can push() values into
	    if (err) {
	        console.log('Unable to connect to the MongoDB for data dump. Error:', err);
	    } else {
	        console.log('Connection established to MongoDB for data dump');
	        var db = client.db("data");
            var collection = db.collection('models');
	        collection.find().toArray(function(err,items){
	            items.forEach(function(item){
	                if (item.name == req.params.modelname)
	                if(item!=null)
	                {
	                    all_models_req["models"].push(item);
	                }
	            });
	        client.close();
	        console.log('All Requested Data: ', JSON.stringify(all_models_req));
	        res.status(200);
    		res.setHeader('Content-Type', 'application/json');
	        res.send(JSON.stringify(all_models_req));
	        });
	    }
    });
});

}
module.exports = appRouter;