/*
* Copyright 2019-2022 EURECOM
* Author - Nadar Ali (ali.nadar@eurecom.fr)
* Date created - 11/10/2021
*/
const express = require("express");
const app = express();
const server = require('http').createServer(app)
const router = express.Router();

// Constants
const AGENT_HOST = process.env.AGENT_HOST
const PORT = process.env.AGENT_PORT;
const HOST = '0.0.0.0';
const BROKER_HOST = process.env.BROKER_HOST;
const BROKER_PORT = process.env.BROKER_PORT;
const req_timeinterval = 10
const req_timeout = req_timeinterval * 12000

var fileupload = require("express-fileupload");
var fs = require('fs');
var path = require('path')
var mqtt = require("mqtt");
var sizeof = require('object-sizeof')
var request = require('request');
var bodyParser = require("body-parser");
var morgan = require('morgan');


global.received={};
global.AI_micorservices=[];
// var KEY = fs.readFileSync(path.join(__dirname, '/certs/server.key'))
// var CERT = fs.readFileSync(path.join(__dirname, '/certs/server.crt'))
// var TRUSTED_CA_LIST = fs.readFileSync(path.join(__dirname, '/certs/ca.crt'))
// const MQTT_KEEPALIVE = 60
// private key and certificate for https
// var options = {	key: fs.readFileSync('/home/geoserver/ssl_cert/5gcroco_eurecom_fr.key', 'ascii'),	cert: fs.readFileSync('/home/geoserver/ssl_cert/5gcroco_eurecom_fr.crt', 'ascii') }
// MQTT option with TLS
// var mqtt_options = {
//   username:"nadar",
//   password:"Ali@1234",
//   port: BROKER_PORT,
//   host: BROKER_HOST,
//   properties:{messageExpiryInterval:600},
//   key: KEY,
//   cert: CERT,
//   rejectUnauthorized: false,
//   ca: TRUSTED_CA_LIST
// }
//var mqttclient = mqtt.connect('mqtts://'+BROKER_HOST , mqtt_options);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'))
app.use(morgan('dev'));// use morgan to log requests to the console
app.use(fileupload());

function dictToTopic(dict)
{
  keys = Object.keys(dict);
  values = Object.values(dict);
  array=[]
  for (i=0; i< keys.length; i++)
  {
    array.push(keys[i] + "=" + values[i]);
  }
  topic = array.join('/');
  return topic
}
function getValueFromTopic(topic, key){
    var array= topic.split("/")
    for (i=0; i< array.length; i++)
    {
        var arr= array[i].split("=")
        if (arr[0] == key) return arr[1]
    }
    return null;
}

const mqtt_options = { port: BROKER_PORT, host: BROKER_HOST, clientId: AGENT_HOST};
var mqttclient = mqtt.connect(mqtt_options);
mqttclient.on("connect",function(     ){console.log("Connected to Mosquitto broker! ")});
mqttclient.on("error"  ,function(error){console.log("Can't connect to Mosquitto !!! " + error);});

mqttclient.on('message',function(topic, message, packet){
	console.log("Receiving Published message with topic: "+ topic); 
    topic_method = getValueFromTopic(topic, "method")
    topic_usage = getValueFromTopic(topic, "model_usage")
	if (topic_method == "sub-available" && topic_usage == "available")
	{
        var ai_id = getValueFromTopic(topic,"agent_id")
        var filtered_micorservices = global.AI_micorservices.filter(item => item.ai_id == ai_id)
        if (filtered_micorservices.length > 0){
            var ai_host = filtered_micorservices[0]['ai_host']
            var ai_port = filtered_micorservices[0]['ai_port']
            // EDGE assigns training model to Agent
            request.post({
            url:'http://'+ ai_host +':'+ ai_port +'/train?topic='+topic,
            body: message,
            encoding: null},
            function(error, response, body){
                if (error == null)console.log("Model-Train has been assigned ! ");
                else              console.log(error);
            });
        }
    }
     else
     {
       global.received[topic] = message; 
       console.log("===========================")
       console.log(Object.keys(global.received))
       //method=sub-available/model_usage=available/agent_type=vehicle/trainability=yes/
       //agent_id=oulu-agent-available/edge_id=eurecom-5gmec/entity=forest/lighting=good/model_type=crosshair_marker_detection
     } 
  });


// Push AI model
router.post("/model", function (req,res){
    params = req.query
    topic_pub_push = dictToTopic(params)
    console.log("Agent Pushs an AI model with topic :" + topic_pub_push)
    const msgs = []
    
    req.on('data', (chunk) => {
        if(chunk)msgs.push(chunk)
    })
    req.on('end', () => {
        const buffer_model = Buffer.concat(msgs)
        console.log("Model Received {size: " + sizeof(buffer_model) + " bytes}")
        mqttclient.publish(topic_pub_push, buffer_model)
        res.status(200).end('End Push-OK!');
    })
});

// Get AI model
router.get("/model", async function(req, res) {
    params = req.query
    topic_sub = dictToTopic(params)
    console.log("Subscribing to: "+ topic_sub);
    mqttclient.subscribe(topic_sub);
    const interval = setInterval(() =>
    {
        if (global.received[topic_sub])
        {
            clearInterval(interval);
            model = global.received[topic_sub]
            delete global.received[topic_sub]
            mqttclient.unsubscribe(topic_sub);
            return res.status(200).end(model, 'binary');
        }
        if (count_interval > req_timeout)
            {
                clearInterval(interval);
                mqttclient.unsubscribe(topic_sub);
                return res.status(200).end('');
            }
        count_interval += req_timeinterval


    }, req_timeinterval);
});

// Announce Agent Avaialability to train 
router.post("/availability", async function(req, res) {
    var params = req.query
    global.AI_micorservices.push({
      'ai_id':params["agent_id"],
      'ai_host':params["_ai_host"],
      'ai_port':params["_ai_port"],
    })
    delete params["_ai_host"]
    delete params["_ai_port"]
    var topic_available = dictToTopic(params)
    console.log("Subscribing to: " + topic_available)
    mqttclient.subscribe(topic_available);
    return res.status(202).json({"message":"Availability announcement has been sent to Geoserver!"})
    });

router.delete("/availability", async function(req, res) {
    params = req.query
    agent_id = params["agent_id"]
    return res.status(202).json({"message":"Availability cancellation has been sent to Geoserver"})
    });
    
// GET a model and Train it and ask for Merging 
router.get("/train", async function(req, res) {
    var params = req.query
    topic_sub = dictToTopic(params)
    console.log("Subscribing to: "+ topic_sub);
    mqttclient.subscribe(topic_sub);
    var count_interval = 0
    const interval = setInterval(() =>
    {
        if (global.received[topic_sub])
        {
            clearInterval(interval);
            var model = global.received[topic_sub]
            delete global.received[topic_sub]
            mqttclient.unsubscribe(topic_sub);
            return res.status(200).end(model, 'binary');
        }
        if (count_interval > req_timeout)
        {
            clearInterval(interval);
            mqttclient.unsubscribe(topic_sub);
            return res.status(200).end('');
        }
        count_interval += req_timeinterval
    }, req_timeinterval);

});
   
router.post("/mergedModel", async function (req,res){
    params = req.query
    topic_pub_model = dictToTopic(params)
    params["method"]= "pub-merge"
    topic_pub_merge =  dictToTopic(params)
    console.log("---> Subscribing to:" + topic_pub_merge)
    mqttclient.subscribe(topic_pub_merge);

    var data = req.data
    req.on('data', (data) => {
        // publish trained model "pub-model"
        console.log("Publishing to:" + topic_pub_model)
        mqttclient.publish(topic_pub_model, data)
        //interval_1 = setInterval(() =>
        setInterval(() =>{
                     if (global.received[topic_pub_merge])
                      {
                         //clearInterval(interval_1);
                         merged_model = global.received[topic_pub_merge]
                         delete global.received[topic_pub_merge]
                         return res.status(200).end(merged_model, 'binary');
                     }
        }, 10);
	});
});



app.use('/', router);
server.listen(PORT, () => { console.log(`Running on http://${HOST}:${PORT}`);});


